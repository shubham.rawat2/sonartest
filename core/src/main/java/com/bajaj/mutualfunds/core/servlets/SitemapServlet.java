package com.bajaj.mutualfunds.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Iterator;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.bajaj.mutualfunds.core.constants.BFDLConstants;
import com.bajaj.mutualfunds.core.services.StaticSitemapService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "= BFL Sitemap",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.methods=" + HttpConstants.METHOD_POST,
        "sling.servlet.resourceTypes=mutualfunds/components/page", "sling.servlet.extensions=xml" })
public class SitemapServlet extends SlingAllMethodsServlet {

    @Reference
    private StaticSitemapService staticSitemapService;

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        try {
            ValueMap valueMap = request.getResource().getChild("root/responsivegrid/sitemapcomp").getValueMap();
            String isV1Sitemap = valueMap.containsKey("enablevone") ? valueMap.get("enablevone").toString() : "false";
            String isSitemap = isV1Sitemap.equalsIgnoreCase("true") ? valueMap.get("issitemapvone").toString()
                    : valueMap.get("issitemap").toString();
            StringBuffer xmlStringBuffer = new StringBuffer();
            String requestDomain = request.getRequestURL().toString().replace(request.getRequestURI(), "");
//			String requestDomain = "https://www.bajajfinservmarkets.in/mutualfunds";
            Calendar calendar = Calendar.getInstance();
            String month = (calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1)
                    : "" + (calendar.get(Calendar.MONTH) + 1);
            String day = calendar.get(Calendar.DATE) < 10 ? "0" + calendar.get(Calendar.DATE)
                    : String.valueOf(calendar.get(Calendar.DATE));
            String date = calendar.get(Calendar.YEAR) + "-" + month + "-"
                    + day;
            if (isSitemap.equalsIgnoreCase("true")) {
                xmlStringBuffer.append("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
            } else {
                xmlStringBuffer.append("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
            }
            if (isV1Sitemap.equalsIgnoreCase("true")) {
                Iterator<Resource> itr = request.getResource().getChild("root/responsivegrid/sitemapcomp/sitemaplist")
                        .getChildren().iterator();
                while (itr.hasNext()) {
                    ValueMap valueMap1 = itr.next().getValueMap();
                    String siteMapData = valueMap1.get("sitemapurlsvone").toString();
                    String[] siteMapDataArray = siteMapData.split("\r?\n");
                    String changeFreq = valueMap1.get("frequency").toString().toLowerCase();
                    String priority = valueMap1.containsKey("priority") ? valueMap1.get("priority").toString() : "0.7";
                    if (valueMap1.get("prorityconfig").toString().equalsIgnoreCase("true")) {
                        JsonArray sitemapPageData = staticSitemapService.getSitemapDetails(siteMapDataArray,
                                requestDomain);
                        for (JsonElement eachData : sitemapPageData) {
                            JsonObject pageData = eachData.getAsJsonObject();
                            String lastMod = pageData.get("lastMod").isJsonNull()
                                    ? date
                                    : pageData.get("lastMod").getAsString();
                            xmlStringBuffer.append(
                                    this.getSitemapString(isSitemap, pageData.get("pageUrl").getAsString(),
                                            lastMod, changeFreq, pageData.get("priority").getAsString()));
                        }
                    } else {
                        for (String eachDataString : siteMapDataArray) {
                            String lastMod = changeFreq.equalsIgnoreCase("daily") ? date : date;
                            xmlStringBuffer.append(this.getSitemapString(isSitemap, eachDataString, lastMod,
                                    changeFreq, priority));
                        }
                    }
                }
            } else {
                String siteMapData = valueMap.get("sitemapurls").toString();
                String lastModified = valueMap.containsKey("lastmodified")
                        ? valueMap.get("lastmodified").toString().equalsIgnoreCase("true") ? date : ""
                        : "";
                String changeFrequency = valueMap.containsKey("changefrequency")
                        ? valueMap.get("changefrequency").toString().equalsIgnoreCase("true") ? "daily" : ""
                        : "";
                String priority = valueMap.containsKey("priority")
                        ? valueMap.get("priority").toString().equalsIgnoreCase("true") ? "0.9" : ""
                        : "";
                String[] siteMapDataArray = siteMapData.split("\r?\n");
                for (String eachDataString : siteMapDataArray) {
                    xmlStringBuffer
                            .append(this.getSitemapString(isSitemap, eachDataString, lastModified, changeFrequency,
                                    priority));
                }
            }
            if (isSitemap.equalsIgnoreCase("true")) {
                xmlStringBuffer.append("</sitemapindex>");
            } else {
                xmlStringBuffer.append("</urlset>");
            }
            PrintWriter out = response.getWriter();
            response.setContentType("application/xml");
            out.println(xmlStringBuffer.toString());
        } catch (Exception e) {
            StringBuffer xmlStringBuffer = new StringBuffer();
            xmlStringBuffer.append("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
            xmlStringBuffer.append("</urlset>");
            PrintWriter out = response.getWriter();
            response.setContentType("application/xml");
            out.println(xmlStringBuffer.toString());
        }
    }

    public String getSitemapString(String isSitemap, String pageUrl, String lastMod,
            String frequency, String priority) {
        String sitemapStr = "";
        if (isSitemap.equalsIgnoreCase("true")) {
            sitemapStr += "<sitemap> <loc>" + pageUrl + "</loc>";

            sitemapStr += lastMod.equalsIgnoreCase("") ? "" : "<lastmod>" + lastMod + "</lastmod>";
            sitemapStr += frequency.equalsIgnoreCase("") ? "" : "<changefreq>" + frequency + "</changefreq>";
            if (pageUrl.equalsIgnoreCase(BFDLConstants.DOMAIN_PATH + "/")
                    || pageUrl.equalsIgnoreCase(BFDLConstants.DOMAIN_PATH)) {
                sitemapStr += priority.equalsIgnoreCase("") ? "" : "<priority>1</priority>";
            } else {
                sitemapStr += priority.equalsIgnoreCase("") ? "" : "<priority>" + priority + "</priority>";
            }

            sitemapStr += "</sitemap>";
        } else {
            sitemapStr += "<url> <loc>" + pageUrl + "</loc>";

            sitemapStr += lastMod.equalsIgnoreCase("") ? "" : "<lastmod>" + lastMod + "</lastmod>";
            sitemapStr += frequency.equalsIgnoreCase("") ? "" : "<changefreq>" + frequency + "</changefreq>";
            if (pageUrl.equalsIgnoreCase(BFDLConstants.DOMAIN_PATH + "/")
                    || pageUrl.equalsIgnoreCase(BFDLConstants.DOMAIN_PATH)) {
                sitemapStr += priority.equalsIgnoreCase("") ? "" : "<priority>1</priority>";
            } else {
                sitemapStr += priority.equalsIgnoreCase("") ? "" : "<priority>" + priority + "</priority>";
            }

            sitemapStr += "</url>";
        }
        return sitemapStr;
    }

}
