package com.bajaj.mutualfunds.core.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bajaj.mutualfunds.core.constants.BFDLConstants;
import com.bajaj.mutualfunds.core.services.BFDLConfigService;
import com.bajaj.mutualfunds.core.services.PageGenerationService;
import com.bajaj.mutualfunds.core.services.ReplicationService;
import com.bajaj.mutualfunds.core.services.ResourceHelper;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

@Component(service = PageGenerationService.class, immediate = true)
public class PageGenerationServiceImpl implements PageGenerationService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceHelper helper;

    @Reference
    BFDLConfigService bflConfigService;

    @Reference
    ReplicationService replicationService;
    List<String> pagesPath;
    int pagesCount;
    final DecimalFormat decfor = new DecimalFormat("0.00");
    JsonArray fundsData;
    static List<Map<String, JsonElement>> fundList;

    @Override
    public JsonArray getFundData() {
        try {

            ClassLoader classLoader = this.getClass().getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("fundsfeed.json");
            try {
                String fundData = null;
                fundData = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()
                        .collect(Collectors.joining(""));
                return new JsonParser().parse(fundData).getAsJsonArray();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

			// S3Object headerOverrideObject = null;
			//
			// AmazonS3 s3client = AmazonS3ClientBuilder.standard()
			// .withRegion(Regions.valueOf(bflConfigService.getRegion())).build();
			//
			// ResponseHeaderOverrides headerOverrides = new
			// ResponseHeaderOverrides().withCacheControl("No-cache")
			// .withContentDisposition("attachment; filename=example.txt");
			//
			// GetObjectRequest getObjectRequestHeaderOverride = new
			// GetObjectRequest(bflConfigService.getBucketName(),
			// this.getFileName()).withResponseHeaders(headerOverrides);
			//
			// headerOverrideObject = s3client.getObject(getObjectRequestHeaderOverride);
			//
			// return new
			// JsonParser().parse(getStreamAsString(headerOverrideObject.getObjectContent())
			// ).getAsJsonArray();

		} catch (Exception e) {
			logger.info("getFundData Error :: {}", e.getMessage());
		}
        return null;
    }

    private String getStreamAsString(InputStream input) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    public String getFileName() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return "funds_feed_" + new SimpleDateFormat("dd_MM_yyyy").format(calendar.getTime()) + ".json";
    }

    public JsonArray updateFundListData(JsonArray fundsData) {
        try {
            JsonArray fundArr = new JsonArray();
            for (JsonElement element : fundsData) {
                try {
                    JsonObject fundObj = element.getAsJsonObject();
                    JsonObject funds = new JsonObject();
                    funds.addProperty(BFDLConstants.AMC_LOGO, fundObj.get(BFDLConstants.AMC_LOGO).getAsString());
                    funds.addProperty(BFDLConstants.AMC_FUNDSCHEME_ID, fundObj.get(BFDLConstants.AMC_FUNDSCHEME_ID).getAsString());
                    funds.addProperty(BFDLConstants.BSE_SCHEME_NAME, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
                    if (!fundObj.get(BFDLConstants.FUND_RETURN_CATEGORY).getAsJsonArray().isJsonNull()
                            && fundObj.get(BFDLConstants.FUND_RETURN_CATEGORY).getAsJsonArray().size() > 0) {
                        funds.addProperty(BFDLConstants.RET_3YEAR, fundObj.get(BFDLConstants.FUND_RETURN_CATEGORY).getAsJsonArray().get(0)
                                .getAsJsonObject().get(BFDLConstants.RET_3YEAR).getAsString());
                    } else {
                        funds.addProperty(BFDLConstants.RET_3YEAR, "-");
                    }
                    funds.addProperty(BFDLConstants.FUND_TYPE, fundObj.get(BFDLConstants.FUND_TYPE).getAsString());
                    funds.addProperty(BFDLConstants.FUND_SUB_TYPE, fundObj.get(BFDLConstants.FUND_SUB_TYPE).getAsString());
                    funds.addProperty(BFDLConstants.RISK_FACTOR, fundObj.get(BFDLConstants.RISK_FACTOR).getAsString());
                    fundArr.add(funds);
                } catch (Exception e) {
                    logger.error("updateFundListData Itr Error : {}", e.getMessage());
                }
            }
            return fundArr;
        } catch (Exception e) {
            logger.error("updateFundListData Error :: {}", e.getMessage());
        }
        return null;
    }

	@Override
	public boolean generateFundPages() {
		this.pagesPath = new ArrayList<>();
		this.pagesCount = 0;
		this.fundsData = this.getFundData();
		JsonArray fundsData = updateAMCLogoURL(this.fundsData);
		fundList = new ArrayList<>();
		fundList = new Gson().fromJson(fundsData, new TypeToken<List<Map<String,
		 JsonElement>>>() {
		 }.getType());
		if (Objects.nonNull(fundsData)) {
			ResourceResolver resolver = null;
			Session session = null;
			try {
				resolver = helper.getResourceResolver();
				session = resolver.adaptTo(Session.class);
				PageManager pageManager = resolver.adaptTo(PageManager.class);
				JcrUtils.getOrCreateByPath(bflConfigService.getRootPath(), BFDLConstants.PAGE_TYPE, session);
				for (JsonElement jsonElement : fundsData) {
					try {
						JsonObject fundData = jsonElement.getAsJsonObject();
						boolean status = createFundPage(fundData, session, pageManager);
						if (status) {
							pagesCount++;
						}
					} catch (Exception e) {
						logger.error("generateFundPages Loop Error :: {}", e.getMessage());
					}
				}
				session.save();

                /*
                 * replicationService.replicatePageContent(this.pagesPath.toArray(new
                 * String[0]));
                 */
            } catch (Exception e) {
                logger.error("generateFundPages Error :: {}", e.getMessage());
            } finally {
                if (session.isLive())
                    session.logout();
                if (resolver.isLive())
                    resolver.close();
            }
        }
        return false;
    }

    public String getFundPageName(String fundName) {
        return fundName.trim().replaceAll("[^a-zA-Z0-9]", "-");
    }

	public boolean createFundPage(JsonObject fundData, Session session, PageManager pageManager) {
		try {
			String categoryPath = bflConfigService.getRootPath() + "/"
					+ fundData.get("fund_type").getAsString().toLowerCase().trim();
			if (!session.itemExists(categoryPath)) {
				JcrUtils.getOrCreateByPath(categoryPath, BFDLConstants.PAGE_TYPE, session);
				session.save();
			}
			String fundName = fundData.get("bse_scheme_name").getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
					.replaceAll("\\-+", "-");
			String fundPath = categoryPath + "/" + fundName;
			if (!session.itemExists(fundPath)) {
				pageManager.create(categoryPath, fundName,
						BFDLConstants.getTemplatePath(fundData.get("fund_type").getAsString().toLowerCase().trim()),
						fundData.get("bse_scheme_name").getAsString());
			}
			Node contentNode = session.getNode(fundPath + "/jcr:content");
			contentNode.setProperty("pagePath", fundPath);
			try {
				contentNode.setProperty("similar_funds",
						this.getSimilarFunds(fundData.get("amcfundscheme_id").getAsInt(),
								fundData.get("fund_type").getAsString(), fundData.get("fund_sub_type").getAsString(),
								fundData.get("risk_factor").getAsString()).toString());
				contentNode.setProperty("others_plan",this.getOthersPlanFunds(fundData.get("amcfundscheme_id").getAsInt(),fundData.get("bse_amc_code").getAsString()).toString());
				contentNode.setProperty("peer_comparison_1_year",
						getPeerCompareList(fundData.get("fund_type").getAsString(), "returns_1_yr",
								fundData.get("amcfundscheme_id").getAsInt()).toString());
				contentNode.setProperty("peer_comparison_3_year",
						getPeerCompareList(fundData.get("fund_type").getAsString(), "returns_3_yr",
								fundData.get("amcfundscheme_id").getAsInt()).toString());
				contentNode.setProperty("peer_comparison_5_year",
						getPeerCompareList(fundData.get("fund_type").getAsString(), "returns_5_yr",
								fundData.get("amcfundscheme_id").getAsInt()).toString());
				contentNode.setProperty("peer_comparison_10_year",
						getPeerCompareList(fundData.get("fund_type").getAsString(), "returns_10_yr",
								fundData.get("amcfundscheme_id").getAsInt()).toString());
				contentNode.setProperty("company_holdings", fundData.get("company_holdings").toString());
				contentNode.setProperty("sectors_holding", fundData.get("sectors_holding").toString());
				contentNode.setProperty("risk_factor", fundData.get("risk_factor").getAsString());
				try {
					JsonArray fundManagers = new JsonArray();
					JsonArray fundManager = fundData.get("fund_manager").getAsJsonArray();
					for (int i = 0; i < fundManager.size(); i++) {
						JsonObject fundManagerData = new JsonObject();
						fundManagerData.addProperty("fund_manager", fundManager.get(i).getAsString());
						for (Entry<String, JsonElement> entry : fundData.get("fund_manager_details").getAsJsonArray()
								.get(i).getAsJsonObject().entrySet()) {
							fundManagerData.addProperty(entry.getKey(), entry.getValue().getAsString());
						}
						fundManagerData.addProperty("since",
								fundData.get("fund_manager_since").getAsJsonArray().get(i).getAsString().split("-")[0]);
						fundManagers.add(fundManagerData);
					}
					contentNode.setProperty("fund_manager_data", fundManagers.toString());
				} catch (Exception e) {
					logger.error("Fund Manager Details Error :: {}", e.getMessage());
				}
				try {
					if (!fundData.get("fund_return_category").isJsonNull()) {
						for (Map.Entry<String, JsonElement> ele : fundData.get("fund_return_category").getAsJsonArray()
								.get(0).getAsJsonObject().entrySet()) {
							if (BFDLConstants.getPdpNumKeys().contains(ele.getKey())) {
								contentNode.setProperty(ele.getKey(),
										ele.getValue().isJsonNull() ? "" : decfor.format(ele.getValue().getAsDouble()));
								continue;
							}
							contentNode.setProperty(ele.getKey(), ele.getValue().getAsString());
						}
					}
				} catch (Exception e) {
					logger.error("fund_return_category Error : {}", e.getMessage());
				}
				try {
					if (!fundData.get("stats_details").isJsonNull()) {
						for (Map.Entry<String, JsonElement> ele : fundData.get("stats_details").getAsJsonArray().get(0)
								.getAsJsonObject().entrySet()) {
							if (BFDLConstants.getPdpNumKeys().contains(ele.getKey())) {
								contentNode.setProperty(ele.getKey(),
										ele.getValue().isJsonNull() ? "" : decfor.format(ele.getValue().getAsDouble()));
								continue;
							}
							contentNode.setProperty(ele.getKey(), ele.getValue().getAsString());
						}
					}
				} catch (Exception e) {
					logger.error("stats_details Error ::{}", e.getMessage());
				}
			} catch (Exception e) {
				logger.error("createFundPage Error :: {} :: {}", fundName, e.getMessage());
			}
			for (Map.Entry<String, JsonElement> entry : fundData.entrySet()) {
				try {
					if (BFDLConstants.getPdpNumKeys().contains(entry.getKey())) {
						contentNode.setProperty(entry.getKey(),
								entry.getValue().isJsonNull() ? "" : decfor.format(entry.getValue().getAsDouble()));
						continue;
					}
					if ((entry.getValue().isJsonArray() || entry.getValue().isJsonObject())) {
						contentNode.setProperty(entry.getKey(),
								entry.getValue().isJsonNull() ? "" : entry.getValue().toString());
						continue;
					}
					if (BFDLConstants.getPdpAmountKeys().contains(entry.getKey())) {
                        contentNode.setProperty(entry.getKey(),
                                entry.getValue().isJsonNull() ? "" : entry.getValue().getAsString().replaceAll("\\.\\d+$", ""));
                    }
                    else {
                        contentNode.setProperty(entry.getKey(),
                                entry.getValue().isJsonNull() ? "" : entry.getValue().getAsString());
                    }
				} catch (Exception e) {
					logger.info("createFundPage Set Property Error :: {}", e.getMessage());
				}
			}
			pagesPath.add(fundPath);
			session.save();
			return true;
		} catch (Exception e) {
			logger.error("createFundPage :: {}", e.getMessage());
			logger.error("createFundPage Fund Data :: {}", fundData);
		}
		return false;
	}

	public List<Map<String, JsonElement>> getPeerCompareList(String type, String sortedBy, int amcId) {
		try {
			List<Map<String, JsonElement>> filteredList = fundList.stream().filter(
					obj -> !obj.get("fund_type").isJsonNull()
							&& obj.get("fund_type").getAsString().equalsIgnoreCase(type)
							&& !obj.get(sortedBy).isJsonNull())
					.collect(Collectors.toList());

            Collections.sort(filteredList, new Comparator<Map<String, JsonElement>>() {
                @Override
                public int compare(Map<String, JsonElement> o1, Map<String, JsonElement> o2) {
                    return o2.get(sortedBy).getAsBigDecimal().compareTo(o1.get(sortedBy).getAsBigDecimal());
                }
            });

            List<Map<String, JsonElement>> peerCompareData = new ArrayList<>();

            for (Map<String, JsonElement> map : filteredList) {
                if (map.get(BFDLConstants.AMC_FUNDSCHEME_ID).getAsInt() != amcId) {
                    peerCompareData.add(map);
                }
                if (peerCompareData.size() == 3) {
                    break;
                }
            }
            return peerCompareData;
        } catch (Exception e) {
            logger.error("getPeerCompareList Error :: {}", e.getMessage());
        }
        return Collections.emptyList();
    }

	public JsonArray getOthersPlanFunds(int amcId, String amcCode) {
		try {
			JsonArray othersPlanArray = new JsonArray();
			for (JsonElement ele : fundsData) {
				JsonObject fundObj = ele.getAsJsonObject();
				if (fundObj.get(BFDLConstants.AMC_FUNDSCHEME_ID).getAsInt() != amcId
						&& fundObj.get(BFDLConstants.BSE_AMC_CODE).getAsString().equalsIgnoreCase(amcCode)) {
					othersPlanArray.add(fundObj);
				}
				if (othersPlanArray.size() == 4) {
					break;
				}
			}
			return othersPlanArray;
		} catch (Exception e) {
			logger.error("getOthersPlanFunds Error :: {}", e.getMessage());
		}
		return null;
	}

    public JsonArray getSimilarFunds(int amcId, String type, String subType, String riskFactor) {
        try {
            JsonArray similarArray = new JsonArray();
            for (JsonElement ele : fundsData) {
                JsonObject fundObj = ele.getAsJsonObject();
                if (fundObj.get(BFDLConstants.AMC_FUNDSCHEME_ID).getAsInt() != amcId
                        && fundObj.get(BFDLConstants.FUND_TYPE).getAsString().equalsIgnoreCase(type)
                        && fundObj.get(BFDLConstants.FUND_SUB_TYPE).getAsString().equalsIgnoreCase(subType)
                        && fundObj.get(BFDLConstants.RISK_FACTOR).getAsString().equalsIgnoreCase(riskFactor)) {
                    similarArray.add(fundObj);
                }
                if (similarArray.size() == 3) {
                    break;
                }
            }
            return similarArray;
        } catch (Exception e) {
            logger.error("getSimilarFunds Error :: {}", e.getMessage());
        }
        return null;
    }

	public JsonArray updateAMCLogoURL(JsonArray fundsData) {
		Iterator<JsonElement> iterator = fundsData.iterator();
		String imageDomainName=bflConfigService.getImageDomainName();
		while (iterator.hasNext()){
			JsonObject fundData = iterator.next().getAsJsonObject();
			if (fundData.has(BFDLConstants.AMC_LOGO)) {
				String amc_logo = imageDomainName + fundData.get(BFDLConstants.AMC_LOGO).getAsString();
				fundData.addProperty(BFDLConstants.AMC_LOGO, amc_logo);
			}
			setRiskFactors(fundData);
		}
		return fundsData ;
	}
	public void setRiskFactors(JsonObject fundData) {
		fundData.addProperty("risk_factors", fundData.get(BFDLConstants.RISK_FACTOR).isJsonNull() ? "" : BFDLConstants.getRiskFactor(fundData.get(BFDLConstants.RISK_FACTOR).getAsString()));
	}
}
