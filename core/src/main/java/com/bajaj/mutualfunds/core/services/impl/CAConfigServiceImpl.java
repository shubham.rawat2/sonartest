package com.bajaj.mutualfunds.core.services.impl;

import java.util.Collection;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.osgi.service.component.annotations.Component;

import com.bajaj.mutualfunds.core.caconfig.CaConfigGlobalVariablesDefinition;
import com.bajaj.mutualfunds.core.services.CAConfigService;

@Component(service = CAConfigService.class, immediate = true)
public class CAConfigServiceImpl implements CAConfigService {
    @Override
    public Collection getConfigDatas(SlingHttpServletRequest request, Resource resource) {
        ConfigurationBuilder configurationBuilder = resource.adaptTo(ConfigurationBuilder.class);

        Collection sampleCollections = configurationBuilder.asCollection(CaConfigGlobalVariablesDefinition.class);
        return sampleCollections;
    }

}
