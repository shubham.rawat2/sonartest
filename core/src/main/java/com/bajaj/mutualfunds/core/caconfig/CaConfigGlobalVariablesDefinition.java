package com.bajaj.mutualfunds.core.caconfig;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * CA Configuration for Global Variables Definitions for content pages
 */
@Configuration(label = "BFDL MF Global Variables Definitions - Configuration", description = "BFDL MF Global Variables Definitions - Configuration", collection = true)
public @interface CaConfigGlobalVariablesDefinition {

    /**
     * @return variable name
     */
    @Property(label = "Variable Name")
    String variableName() default "";

    /**
     * @return variable value
     */
    @Property(label = "Variable Value")
    String variableValue() default "";
}