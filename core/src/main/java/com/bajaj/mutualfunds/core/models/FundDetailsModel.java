package com.bajaj.mutualfunds.core.models;

import com.bajaj.mutualfunds.core.constants.BFDLConstants;
import com.bajaj.mutualfunds.core.services.BFDLConfigService;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.osgi.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.DecimalFormat;
import java.util.*;

@Model(adaptables = { SlingHttpServletRequest.class,
		Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FundDetailsModel {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@OSGiService
	BFDLConfigService bfdlConfigService;
	@Inject
	public String fundManagerData;
	@Inject
	public String companyHoldingData;
	@Inject
	public String sectorHoldingData;
	@Inject
	public String peerComparison1YearData;
	@Inject
	public String peerComparison3YearData;
	@Inject
	public String peerComparison5YearData;
	@Inject
	public String peerComparison10YearData;
	@Inject
	public String similarFundData;
	@Inject
	public String otherPlansFundData;
	@Inject
	public String topInvestorData;
	List<Map<String, String>> fundManagerList;
	List<Map<String, String>> similarFundList;
	List<Map<String, String>> otherPlansFundList;
	List<Map<String, String>> companyHoldingList;
	List<Map<String, String>> sectorHoldingList;
	List<Map<String, String>> peerComparison1YearList;
	List<Map<String, String>> peerComparison3YearList;
	List<Map<String, String>> peerComparison5YearList;
	List<Map<String, String>> peerComparison10YearList;
	List<Map<String, String>> topInvestorList;

	public List<Map<String, String>> getSectorHoldingList() {
		return sectorHoldingList;
	}

	public List<Map<String, String>> getSimilarFundList() {
		return similarFundList;
	}

	public List<Map<String, String>> getOtherPlansFundList() {
		return otherPlansFundList;
	}

	public List<Map<String, String>> getCompanyHoldingList() {
		return companyHoldingList;
	}

	public List<Map<String, String>> getFundManagerList() {
		return fundManagerList;
	}

	public List<Map<String, String>> getTopInvestorList() {
		return topInvestorList;
	}

	public List<Map<String, String>> getPeerComparison1YearList() {
		return peerComparison1YearList;
	}

	public List<Map<String, String>> getPeerComparison3YearList() {
		return peerComparison3YearList;
	}

	public List<Map<String, String>> getPeerComparison5YearList() {
		return peerComparison5YearList;
	}

	public List<Map<String, String>> getPeerComparison10YearList() {
		return peerComparison10YearList;
	}

	final DecimalFormat decfor = new DecimalFormat("0.00");

	@PostConstruct
	public void init() {
		if (Objects.nonNull(this.fundManagerData)) {
			fundManagerList = new ArrayList<>();
			fundManagerList = new Gson().fromJson(this.fundManagerData, new TypeToken<List<Map<String, String>>>() {
			}.getType());
		}
		if (Objects.nonNull(this.companyHoldingData)) {
			companyHoldingList = new ArrayList<>();
			JsonArray companyHoldingArr = new JsonParser().parse(this.companyHoldingData).getAsJsonArray();
			for (JsonElement jsonElement : companyHoldingArr) {
				JsonArray assetHoldingArr = jsonElement.getAsJsonObject().get(BFDLConstants.ASSET_HOLDING).getAsJsonArray();
				for (JsonElement assetEle : assetHoldingArr) {
					Map<String, String> companyHoldingMap = new HashMap<>();
					JsonObject companyObj = assetEle.getAsJsonObject();
					companyHoldingMap.put(BFDLConstants.NAME_PROP, companyObj.get(BFDLConstants.NAME_PROP).getAsString());
					companyHoldingMap.put(BFDLConstants.PERCENTAGE, companyObj.get(BFDLConstants.PERCENTAGE).getAsString());
					companyHoldingList.add(companyHoldingMap);
				}
			}
		}
		if (Objects.nonNull(this.sectorHoldingData)) {
			sectorHoldingList = new ArrayList<>();
			JsonArray sectorHoldingArr = new JsonParser().parse(this.sectorHoldingData).getAsJsonArray();
			for (JsonElement jsonElement : sectorHoldingArr) {
				JsonObject assetHoldingObj = jsonElement.getAsJsonObject();
				Map<String, String> sectorHoldingMap = new HashMap<>();
				sectorHoldingMap.put(BFDLConstants.NAME_PROP, assetHoldingObj.get(BFDLConstants.NAME_PROP).getAsString());
				sectorHoldingMap.put(BFDLConstants.PERCENTAGE, assetHoldingObj.get(BFDLConstants.PERCENTAGE).getAsString());
				sectorHoldingList.add(sectorHoldingMap);
			}
		}
		if (Objects.nonNull(this.peerComparison1YearData)) {
			peerComparison1YearList = new ArrayList<>();
			JsonArray peerComaprisonArray = new JsonParser().parse(this.peerComparison1YearData).getAsJsonArray();
			for (JsonElement jsonElement : peerComaprisonArray) {
				JsonObject fundObj = jsonElement.getAsJsonObject();
				Map<String, String> peerData = new HashMap<>();
				peerData.put(BFDLConstants.AMC_LOGO,fundObj.get(BFDLConstants.AMC_LOGO).getAsString());
				peerData.put(BFDLConstants.BSE_SCHEME_NAME, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerData.put(BFDLConstants.VR_RATING,
						fundObj.get(BFDLConstants.VR_RATING).isJsonNull() ? "" : fundObj.get(BFDLConstants.VR_RATING).getAsString());
				peerData.put(BFDLConstants.RETURNS, fundObj.get(BFDLConstants.RETURNS_1_YEAR).isJsonNull() ? ""
						: decfor.format(fundObj.get(BFDLConstants.RETURNS_1_YEAR).getAsDouble()));
				String pagePath = bfdlConfigService.getRootPath() + "/"  + fundObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
								.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				peerData.put("pagePath", pagePath + ".html");
				peerData.put(BFDLConstants.NAME_PROP, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerComparison1YearList.add(peerData);
			}
		}
		if (Objects.nonNull(this.peerComparison3YearData)) {
			peerComparison3YearList = new ArrayList<>();
			JsonArray peerComaprisonArray = new JsonParser().parse(this.peerComparison3YearData).getAsJsonArray();
			for (JsonElement jsonElement : peerComaprisonArray) {
				JsonObject fundObj = jsonElement.getAsJsonObject();
				Map<String, String> peerData = new HashMap<>();
				peerData.put(BFDLConstants.AMC_LOGO,fundObj.get(BFDLConstants.AMC_LOGO).getAsString());
				peerData.put(BFDLConstants.BSE_SCHEME_NAME, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerData.put(BFDLConstants.VR_RATING,
						fundObj.get(BFDLConstants.VR_RATING).isJsonNull() ? "" : fundObj.get(BFDLConstants.VR_RATING).getAsString());
				peerData.put(BFDLConstants.RETURNS, fundObj.get(BFDLConstants.RETURNS_3_YEAR).isJsonNull() ? ""
						: decfor.format(fundObj.get(BFDLConstants.RETURNS_3_YEAR).getAsDouble()));
				String pagePath = bfdlConfigService.getRootPath() + "/"  + fundObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
								.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				peerData.put("pagePath", pagePath + ".html");
				peerData.put(BFDLConstants.NAME_PROP, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerComparison3YearList.add(peerData);
			}
		}
		if (Objects.nonNull(this.peerComparison5YearData)) {
			peerComparison5YearList = new ArrayList<>();
			JsonArray peerComaprisonArray = new JsonParser().parse(this.peerComparison5YearData).getAsJsonArray();
			for (JsonElement jsonElement : peerComaprisonArray) {
				JsonObject fundObj = jsonElement.getAsJsonObject();
				Map<String, String> peerData = new HashMap<>();
				peerData.put(BFDLConstants.AMC_LOGO,fundObj.get(BFDLConstants.AMC_LOGO).getAsString());
				peerData.put(BFDLConstants.BSE_SCHEME_NAME, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerData.put(BFDLConstants.VR_RATING,
						fundObj.get(BFDLConstants.VR_RATING).isJsonNull() ? "" : fundObj.get(BFDLConstants.VR_RATING).getAsString());
				peerData.put(BFDLConstants.RETURNS, fundObj.get(BFDLConstants.RETURNS_5_YEAR).isJsonNull() ? ""
						: decfor.format(fundObj.get(BFDLConstants.RETURNS_5_YEAR).getAsDouble()));
				String pagePath = bfdlConfigService.getRootPath() + "/"  + fundObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
								.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				peerData.put("pagePath", pagePath + ".html");
				peerData.put(BFDLConstants.NAME_PROP, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerComparison5YearList.add(peerData);
			}
		}
		if (Objects.nonNull(this.peerComparison10YearData)) {
			peerComparison10YearList = new ArrayList<>();
			JsonArray peerComaprisonArray = new JsonParser().parse(this.peerComparison10YearData).getAsJsonArray();
			for (JsonElement jsonElement : peerComaprisonArray) {
				JsonObject fundObj = jsonElement.getAsJsonObject();
				Map<String, String> peerData = new HashMap<>();
				peerData.put(BFDLConstants.AMC_LOGO,fundObj.get(BFDLConstants.AMC_LOGO).getAsString());
				peerData.put(BFDLConstants.BSE_SCHEME_NAME, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerData.put(BFDLConstants.VR_RATING,
						fundObj.get(BFDLConstants.VR_RATING).isJsonNull() ? "" : fundObj.get(BFDLConstants.VR_RATING).getAsString());
				peerData.put(BFDLConstants.RETURNS, fundObj.get(BFDLConstants.RETURNS_10_YEAR).isJsonNull() ? ""
						: decfor.format(fundObj.get(BFDLConstants.RETURNS_10_YEAR).getAsDouble()));
				String pagePath = bfdlConfigService.getRootPath() + "/" + fundObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
								.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				peerData.put("pagePath", pagePath + ".html");
				peerData.put(BFDLConstants.NAME_PROP, fundObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString());
				peerComparison10YearList.add(peerData);
			}
		}
		if (Objects.nonNull(this.similarFundData)) {
			similarFundList = new ArrayList<>();
			JsonArray similarArr = new JsonParser().parse(this.similarFundData).getAsJsonArray();
			for (JsonElement jsonElement : similarArr) {
				JsonObject similarObj = jsonElement.getAsJsonObject();
				Map<String, String> fundMap = new HashMap<>();
				fundMap.put(BFDLConstants.AMC_LOGO,similarObj.get(BFDLConstants.AMC_LOGO).getAsString());
				fundMap.put(BFDLConstants.FUND_TYPE, similarObj.get(BFDLConstants.FUND_TYPE).getAsString());
				fundMap.put(BFDLConstants.RETURNS_5_YEAR, similarObj.get(BFDLConstants.RETURNS_5_YEAR).isJsonNull() ? ""
						: decfor.format(similarObj.get(BFDLConstants.RETURNS_5_YEAR).getAsDouble()));
				fundMap.put(BFDLConstants.NAME_PROP, similarObj.get(BFDLConstants.NAME_PROP).getAsString());
				fundMap.put(BFDLConstants.FUND_SUB_TYPE, similarObj.get(BFDLConstants.FUND_SUB_TYPE).getAsString());
				fundMap.put(BFDLConstants.VR_RATING, similarObj.get(BFDLConstants.VR_RATING).isJsonNull() ? ""
						: similarObj.get(BFDLConstants.VR_RATING).getAsString());
				fundMap.put(BFDLConstants.RISK_FACTOR, similarObj.get(BFDLConstants.RISK_FACTOR).getAsString());
				fundMap.put(BFDLConstants.BSE_MINIMUM_PURCHASE_AMOUNT, similarObj.get(BFDLConstants.BSE_MINIMUM_PURCHASE_AMOUNT).getAsString());
				String pagePath = bfdlConfigService.getRootPath() + "/" + similarObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ similarObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
								.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				fundMap.put("pagePath", pagePath + ".html");
				similarFundList.add(fundMap);
			}
		}

		if (Objects.nonNull(this.otherPlansFundData)) {
			otherPlansFundList = new ArrayList<>();
			JsonArray otherPlanArr = new JsonParser().parse(this.otherPlansFundData).getAsJsonArray();
			for (JsonElement jsonElement : otherPlanArr) {
				JsonObject otherPlanObj = jsonElement.getAsJsonObject();
				Map<String, String> fundMap = new HashMap<>();
				fundMap.put(BFDLConstants.AMC_LOGO,otherPlanObj.get(BFDLConstants.AMC_LOGO).getAsString());
				fundMap.put(BFDLConstants.FUND_TYPE, otherPlanObj.get(BFDLConstants.FUND_TYPE).getAsString());
				fundMap.put(BFDLConstants.RETURNS_5_YEAR, otherPlanObj.get(BFDLConstants.RETURNS_5_YEAR).isJsonNull() ? ""
						: decfor.format(otherPlanObj.get(BFDLConstants.RETURNS_5_YEAR).getAsDouble()));
				fundMap.put(BFDLConstants.NAME_PROP, otherPlanObj.get(BFDLConstants.NAME_PROP).getAsString());
				fundMap.put(BFDLConstants.FUND_SUB_TYPE, otherPlanObj.get(BFDLConstants.FUND_SUB_TYPE).getAsString());
				fundMap.put(BFDLConstants.VR_RATING, otherPlanObj.get(BFDLConstants.VR_RATING).isJsonNull() ? ""
						: otherPlanObj.get(BFDLConstants.VR_RATING).getAsString());
				fundMap.put(BFDLConstants.RISK_FACTOR, otherPlanObj.get("risk_factors").getAsString());
				fundMap.put(BFDLConstants.BSE_MINIMUM_PURCHASE_AMOUNT, otherPlanObj.get(BFDLConstants.BSE_MINIMUM_PURCHASE_AMOUNT).getAsString());
				String pagePath = bfdlConfigService.getRootPath() + "/" + otherPlanObj.get(BFDLConstants.FUND_TYPE).getAsString().trim().toLowerCase() + "/"
						+ otherPlanObj.get(BFDLConstants.BSE_SCHEME_NAME).getAsString().trim().toLowerCase().replaceAll("\\s+", "-")
						.replaceAll("[^a-zA-Z0-9]", "-").replaceAll("\\-+", "-");
				fundMap.put("pagePath", pagePath + ".html");
				otherPlansFundList.add(fundMap);
			}
		}
		if (Objects.nonNull(this.topInvestorData)) {
			topInvestorList = new ArrayList<>();
			JsonArray topInvestorsData = new JsonParser().parse(this.topInvestorData).getAsJsonArray();
			for (JsonElement jsonElement : topInvestorsData) {
				JsonObject fund_category = jsonElement.getAsJsonObject();
				Map<String, String> hashMap = new HashMap<>();
				hashMap.put("count_1day", fund_category.get("count_1day").getAsString());
				hashMap.put("count_10year", fund_category.get("count_10year").getAsString());
				hashMap.put("count_1month", fund_category.get("count_1month").getAsString());
				topInvestorList.add(hashMap);
			}
		}
	}
}