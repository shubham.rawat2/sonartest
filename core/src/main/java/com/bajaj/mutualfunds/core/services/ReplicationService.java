package com.bajaj.mutualfunds.core.services;

public interface ReplicationService {

	boolean replicatePageContent(String[] path);

	void activatePageAssets(String[] path);
}
