package com.bajaj.mutualfunds.core.services;

public interface BFDLConfigService {
	String getRegion();

	String getBucketName();
	
	String getRootPath();

	String getImageDomainName();
}
