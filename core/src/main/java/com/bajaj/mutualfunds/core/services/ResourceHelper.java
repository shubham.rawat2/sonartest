package com.bajaj.mutualfunds.core.services;

import javax.jcr.LoginException;

import org.apache.sling.api.resource.ResourceResolver;

public interface ResourceHelper {
	ResourceResolver getResourceResolver() throws LoginException;
}
