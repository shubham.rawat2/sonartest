package com.bajaj.mutualfunds.core.services.impl;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bajaj.mutualfunds.core.services.ReplicationService;
import com.bajaj.mutualfunds.core.services.ResourceHelper;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Component(service = ReplicationService.class, immediate = true)
public class ReplicationServiceImpl implements ReplicationService {

	@Reference
	ResourceHelper resHelper;

	@Reference
	QueryBuilder builder;

	@Reference
	private Replicator replicator;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	ResourceResolver resolver;
	Session session;

	public boolean replicatePageContent(String[] path) {
		try {
			resolver = resHelper.getResourceResolver();
			session = resolver.adaptTo(Session.class);
			replicateContent(path);
			return true;
		} catch (Exception e) {
			logger.error("Error in Replicate Page Content " + e.getMessage());
		} finally {
			if (Objects.nonNull(resolver))
				resolver.close();

			if (Objects.nonNull(session))
				session.logout();
		}
		return false;
	}

	public void activatePageAssets(String[] path) {
		try {
			resolver = resHelper.getResourceResolver();
			session = resolver.adaptTo(Session.class);
			for (int i = 0; i < path.length; i++) {
				Set<String> pageAssetPaths = getPageAssetsPaths(resolver, path[i]);
				if (pageAssetPaths == null) {
					return;
				}
				replicateContent(pageAssetPaths.toArray(new String[0]));
			}

		} catch (Exception e) {
			logger.error("Error in Activate Page Assets " + e.getMessage());
		} finally {
			if (Objects.nonNull(resolver))
				resolver.close();

			if (Objects.nonNull(session))
				session.logout();
		}

	}

	private Set<String> getPageAssetsPaths(ResourceResolver resolver, String pagePath) {

		try {
			PageManager pageManager = resolver.adaptTo(PageManager.class);

			Page page = pageManager.getPage(pagePath);

			if (page == null) {
				return new LinkedHashSet<>();
			}

			Resource resource = page.getContentResource();
			AssetReferenceSearch assetReferenceSearch = new AssetReferenceSearch(resource.adaptTo(Node.class),
					DamConstants.MOUNTPOINT_ASSETS, resolver);
			Map<String, Asset> assetMap = assetReferenceSearch.search();

			return assetMap.keySet();
		} catch (Exception e) {
			logger.error("Error in getPageAssetsPaths " + e.getMessage());
		}
		return Collections.emptySet();
	}

	private void replicateContent(String[] path) {
		try {
			resolver = resHelper.getResourceResolver();
			session = resolver.adaptTo(Session.class);
			ReplicationOptions options = new ReplicationOptions();
			options.setSuppressVersions(true);
			options.setSynchronous(true);
			options.setSuppressStatusUpdate(false);
			replicator.replicate(session, ReplicationActionType.ACTIVATE, path, options);
			logger.info("Replication Status");
			for (int i = 0; i < path.length; i++) {
				logger.info("Replication Status :: path = {0} :: {1}", path[i],
						replicator.getReplicationStatus(session, path[i]));
			}
			session.save();
		} catch (ReplicationException e) {
			logger.error("Content Replication Error ::" + e.getMessage(), e);
			e.printStackTrace();
		} catch (LoginException e) {
			logger.error("Replicate Content ERROR ::" + e.getMessage());
		} catch (RepositoryException e) {
			logger.error("Repository Exception ERROR ::" + e.getMessage());
		} finally {
			if (Objects.nonNull(resolver))
				resolver.close();

			if (Objects.nonNull(session))
				session.logout();
		}
	}
}