package com.bajaj.mutualfunds.core.services;

import java.util.Collection;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

public interface CAConfigService {
    public Collection getConfigDatas(SlingHttpServletRequest request, Resource resource);
}
