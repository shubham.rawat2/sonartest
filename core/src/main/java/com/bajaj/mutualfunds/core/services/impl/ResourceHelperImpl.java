package com.bajaj.mutualfunds.core.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bajaj.mutualfunds.core.services.ResourceHelper;

@Component(service = ResourceHelper.class, immediate = true)
public class ResourceHelperImpl implements ResourceHelper {
	@Reference
	ResourceResolverFactory resResolverFactory;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public ResourceResolver getResourceResolver() {
		try {
			Map<String, Object> map = new HashMap<>();
			map.put(ResourceResolverFactory.SUBSERVICE, "subServiceName");
			return resResolverFactory.getServiceResourceResolver(map);
		} catch (LoginException e) {
			logger.error("getResourceResolver ::: {}", e.getMessage());
		}
		return null;
	}
}
