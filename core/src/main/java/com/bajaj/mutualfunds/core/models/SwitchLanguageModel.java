package com.bajaj.mutualfunds.core.models;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class SwitchLanguageModel {

	public static Map<String, String> LANGUAGE_MAP = new HashMap<>();
	public static Map<String, String> FOOTER_MAP = new HashMap<>();

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Inject
	String pagePath;

	@Inject
	@Default(booleanValues = true)
	boolean isHeader;
	@Inject
	Resource resource;

	String tempPath;
	boolean isTemplateFound=false;

	@PostConstruct
	public static void constructLanguageMap() {
		LANGUAGE_MAP.put("en", "/conf/bajajfinserv/settings/wcm/templates/home-page/structure/jcr:content/root/l0_navigation");
		LANGUAGE_MAP.put("hi", "/conf/bajajfinserv/settings/wcm/templates/hindi-page-template/structure/jcr:content/root/l0_navigation");
	}

	@PostConstruct
	public static void constructFooterMap() {
		FOOTER_MAP.put("en", "/conf/bajajfinserv/settings/wcm/templates/home-page/structure/jcr:content/root/footer");
		FOOTER_MAP.put("hi", "/conf/bajajfinserv/settings/wcm/templates/hindi-page-template/structure/jcr:content/root/footer");
	}

	@PostConstruct
	public void init() {
		SwitchLanguageModel.constructFooterMap();
		SwitchLanguageModel.constructLanguageMap();
		this.isTemplateFound = false;
		try {
			if(Objects.nonNull(this.pagePath)) {
				if(this.isHeader){
					for (Map.Entry<String, String> entry : SwitchLanguageModel.LANGUAGE_MAP.entrySet()) {
						if(pagePath.contains("/"+entry.getKey()+"/")) {
							this.isTemplateFound = true;
							this.tempPath=entry.getValue(); }
					}
					if(!this.isTemplateFound){
						this.tempPath = SwitchLanguageModel.LANGUAGE_MAP.get("en");
					}
				}else{
					for (Map.Entry<String, String> entry : SwitchLanguageModel.FOOTER_MAP.entrySet()) {
						if(pagePath.contains("/"+entry.getKey()+"/")) {
							this.isTemplateFound = true;
							this.tempPath=entry.getValue();
						}
					}
					if(!this.isTemplateFound){
						this.tempPath = SwitchLanguageModel.FOOTER_MAP.get("en");
					}
				}
			}
		} catch (Exception e) {
			logger.error("SwitchLanguageModel Error ::{} " , e.getMessage());
		}
	}

	public String getTempPath() {
		return tempPath;
	}
}