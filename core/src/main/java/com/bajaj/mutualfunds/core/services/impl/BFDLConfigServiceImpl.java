package com.bajaj.mutualfunds.core.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.service.metatype.annotations.Option;

import com.bajaj.mutualfunds.core.constants.BFDLConstants;
import com.bajaj.mutualfunds.core.services.BFDLConfigService;

@Designate(ocd = BFDLConfigServiceImpl.Config.class)
@Component(service = BFDLConfigService.class, immediate = true)
public class BFDLConfigServiceImpl implements BFDLConfigService {

	protected String bucketName = null;
	protected String region = null;
	protected String pdpTemplatePath = null;
	protected String rootPageName = null;
	protected String rootPagePath = null;
	protected String imageDomainName = null;

	@ObjectClassDefinition(name = "BFDL Mutual Fund Site Configuration", description = "BFDL Mutual Fund Site Configuration")
	public @interface Config {

		@AttributeDefinition(name = "MF Root Page Name", description = "MF Root Page Name", required = true, type = AttributeType.STRING)
		public String getRootPageName() default "mutualfunds";

		@AttributeDefinition(name = "MF Root Path", description = "MF Root Path", required = true, type = AttributeType.STRING)
		public String getRootPath() default "/content/mutualfunds/in/en";

		@AttributeDefinition(name = "Region", description = "Region", options = {
				@Option(label = "AP_SOUTHEAST_1", value = "AP_SOUTHEAST_1"),
				@Option(label = "AP_SOUTH_1", value = "AP_SOUTH_1") })
		public String getRegions();

		@AttributeDefinition(name = "Bucket Name", description = "Bucket Name", required = true, type = AttributeType.STRING)
		public String getBucketName() default "bfl-npqa-invmf-application-bucket/media/FUNDS_FEED";

		@AttributeDefinition(name = "Image Domain Name",description ="Image Domain Name (ends with '/')", required = true, type = AttributeType.STRING)
		public String getImageDomainName() default "https://www.bajajfinservmarkets.in/mf/static/";
	}

	@Activate
	@Modified
	protected void activate(Config config) {
		BFDLConstants.constructNumkeys();
		BFDLConstants.constructFundTypeTempMap();
		BFDLConstants.constructRiskFactorMap();
		BFDLConstants.constructAmountKeys();
		this.bucketName = config.getBucketName();
		this.region = config.getRegions();
		this.rootPageName = config.getRootPageName();
		this.rootPagePath = config.getRootPath();
		this.imageDomainName = config.getImageDomainName();
	}

	@Override
	public String getRegion() {
		return this.region;
	}

	@Override
	public String getBucketName() {
		return this.bucketName;
	}

	@Override
	public String getRootPath() {
		return this.rootPagePath + "/" + this.rootPageName;
	}

	@Override
	public String getImageDomainName() {
		return this.imageDomainName;
	}
}