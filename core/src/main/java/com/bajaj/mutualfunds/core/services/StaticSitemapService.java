package com.bajaj.mutualfunds.core.services;

import com.google.gson.JsonArray;

public interface StaticSitemapService {
    public JsonArray getSitemapDetails(String[] pages, String requestDomain);
}
