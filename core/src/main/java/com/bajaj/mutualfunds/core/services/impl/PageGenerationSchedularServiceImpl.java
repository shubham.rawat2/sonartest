	package com.bajaj.mutualfunds.core.services.impl;

import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bajaj.mutualfunds.core.services.PageGenerationSchedularService;
import com.bajaj.mutualfunds.core.services.PageGenerationService;


@Component(service = PageGenerationSchedularServiceImpl.class, immediate = true)
@Designate(ocd = PageGenerationSchedularService.class)
public class PageGenerationSchedularServiceImpl implements Runnable {
	@Reference
	private Scheduler scheduler;

	@Reference
	PageGenerationService generationService;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	private int schedulerId;

	@Activate
	@Modified
	protected void activate(PageGenerationSchedularService config) {
		schedulerId = config.schdulerName().hashCode();
		if (config.enabled()) {
			addScheduler(config);
		}
	}

	@Deactivate
	protected void deactivate(PageGenerationSchedularService config) {
		removeScheduler();
	}

	private void removeScheduler() {
		logger.info("Removing scheduler: {}", schedulerId);
		scheduler.unschedule(String.valueOf(schedulerId));
	}

	private void addScheduler(PageGenerationSchedularService config) {
		removeScheduler();
		if (config.enabled()) {
			ScheduleOptions scheduleOptions = scheduler.EXPR(config.cronExpression());
			scheduleOptions.name(config.schdulerName());
			scheduleOptions.canRunConcurrently(false);
			scheduler.schedule(this, scheduleOptions);
			logger.info("Scheduler added");
		} else {
			logger.info("Scheduler is disabled");
		}
	}

	@Override
	public void run() {
		logger.info("BFDL Mutual Fund - Page Generator Schedular is now running....");
		generationService.generateFundPages();
		logger.info("BFDL Mutual Fund - Page Generator Schedular is end....");
	}
}
