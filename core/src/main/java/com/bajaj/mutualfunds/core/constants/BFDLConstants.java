package com.bajaj.mutualfunds.core.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

public class BFDLConstants {

    private BFDLConstants() {
    }

	protected static Map<String, String> fundTypeTempMap = new HashMap<>();
	public static Map<String, String> riskFactorMap = new HashMap<>();

    public static final String PAGE_TYPE = "cq:Page";
    public static final String FUND_TYPE = "fund_type";
    public static final String AMC_FUNDSCHEME_ID = "amcfundscheme_id";
    public static final String FUND_RETURN_CATEGORY = "fund_return_category";
    public static final String STATS_DETAILS = "stats_details";
    public static final String PERCENTAGE = "percentage";
    public static final String BSE_SCHEME_NAME = "bse_scheme_name";
    public static final String VR_RATING = "vr_rating";
    public static final String RETURNS = "return";
    public static final String RETURNS_5_YEAR = "returns_5_yr";
    public static final String RETURNS_1_YEAR = "returns_1_yr";
    public static final String RETURNS_3_YEAR = "returns_3_yr";
    public static final String RETURNS_10_YEAR = "returns_10_yr";
    public static final String FUND_MANAGER_DATA = "fund_manager_data";
    public static final String COMPANY_HOLDINGS = "company_holdings";
    public static final String ASSET_HOLDING = "asset_holding";
    public static final String SECTOR_HOLDING = "sectors_holding";
    public static final String PEER_COMPARISON_1_YEAR = "peer_comparison_1_year";
    public static final String PEER_COMPARISON_3_YEAR = "peer_comparison_3_year";
    public static final String PEER_COMPARISON_5_YEAR = "peer_comparison_5_year";
    public static final String PEER_COMPARISON_10_YEAR = "peer_comparison_10_year";
    public static final String NAME_PROP = "name";
    public static final String SIMILAR_FUNDS = "similar_funds";
    public static final String FUND_SUB_TYPE = "fund_sub_type";
    public static final String RISK_FACTOR = "risk_factor";
    public static final String BSE_MINIMUM_PURCHASE_AMOUNT = "bse_minimum_purchase_amount";
    public static final String AMC_LOGO = "amc_logo";
    public static final String RET_3YEAR = "ret_3year";
    public static final String BSE_AMC_CODE = "bse_amc_code";

    protected static List<String> pdpAmountKeys = new ArrayList<>();
    protected static List<String> pdpNumKeys = new ArrayList<>();

	public static final String DOMAIN_PATH = "https://www.bajajfinservmarkets.in/mutualfunds";
	public static final String PAGE_PATH = "/content/mutualfunds/in/en";

    @PostConstruct
    public static void constructFundTypeTempMap() {
        fundTypeTempMap.put("debt", "/conf/mutualfunds/settings/wcm/templates/debt");
        fundTypeTempMap.put("equity", "/conf/mutualfunds/settings/wcm/templates/equity");
        fundTypeTempMap.put("hybrid", "/conf/mutualfunds/settings/wcm/templates/hybrid");
        fundTypeTempMap.put("commodities", "/conf/mutualfunds/settings/wcm/templates/fund-pdp-page");
    }

    @PostConstruct
    public static void constructNumkeys() {
        getPdpNumKeys().add("returns_1_mth");
        getPdpNumKeys().add("returns_1_mth_regular");
        getPdpNumKeys().add("returns_1_week");
        getPdpNumKeys().add(RETURNS_10_YEAR);
        getPdpNumKeys().add(RETURNS_1_YEAR);
        getPdpNumKeys().add("returns_1_yr_regular");
        getPdpNumKeys().add("returns_2_yr");
        getPdpNumKeys().add("returns_2_yr_regular");
        getPdpNumKeys().add("returns_3_mth");
        getPdpNumKeys().add("returns_3_mth_regular");
        getPdpNumKeys().add(RETURNS_3_YEAR);
        getPdpNumKeys().add("returns_3_yr_regular");
        getPdpNumKeys().add("returns_4_yr");
        getPdpNumKeys().add(RETURNS_5_YEAR);
        getPdpNumKeys().add("returns_5_yr_regular");
        getPdpNumKeys().add("returns_6_mth");
        getPdpNumKeys().add("returns_6_mth_regular");
        getPdpNumKeys().add("returns_7_yr");
        getPdpNumKeys().add("returns_9_mth");
        getPdpNumKeys().add("returns_from_launch");
        getPdpNumKeys().add("returns_ytd");
        getPdpNumKeys().add("ret_10year");
        getPdpNumKeys().add("ret_1month");
        getPdpNumKeys().add("ret_1year");
        getPdpNumKeys().add("ret_2year");
        getPdpNumKeys().add("ret_3month");
        getPdpNumKeys().add(RET_3YEAR);
        getPdpNumKeys().add("ret_4year");
        getPdpNumKeys().add("ret_5year");
        getPdpNumKeys().add("ret_6month");
        getPdpNumKeys().add("latest_nav");
        getPdpNumKeys().add("stats_alpha");
        getPdpNumKeys().add("stats_beta");
        getPdpNumKeys().add("stats_information_ratio");
        getPdpNumKeys().add("stats_mean");
        getPdpNumKeys().add("stats_rsquared");
        getPdpNumKeys().add("stats_sharpe_ratio");
        getPdpNumKeys().add("stats_sortino_ratio");
        getPdpNumKeys().add("stats_standard_deviation");
        getPdpNumKeys().add("assets_under_management");
    }

	@PostConstruct
	public static void constructRiskFactorMap() {
		riskFactorMap.put("VHG", "Very High");
		riskFactorMap.put("HGH", "High");
		riskFactorMap.put("MDH", "Moderate High");
		riskFactorMap.put("MOD", "Moderate");
		riskFactorMap.put("MLW", "Moderate Low");
		riskFactorMap.put("LOW", "Low");
	}

	@PostConstruct
	public static void constructAmountKeys() {
		getPdpAmountKeys().add("bse_sip_minimum_installment_amount_monthly");
		getPdpAmountKeys().add("bse_minimum_purchase_amount");
		getPdpAmountKeys().add("bse_additional_purchase_amount_multiple");
	}

	public static List<String> getPdpNumKeys() {
		return pdpNumKeys;
	}

	public static String getTemplatePath(String fundType) {
		return fundTypeTempMap.get(fundType);
	}

	public static String getRiskFactor(String riskFactor) {
		return riskFactorMap.get(riskFactor);
	}

	public static List<String> getPdpAmountKeys() {
		return pdpAmountKeys;
	 }
}
