package com.bajaj.mutualfunds.core.services;

import com.google.gson.JsonArray;

public interface PageGenerationService {

	JsonArray getFundData();

	boolean generateFundPages();

}
