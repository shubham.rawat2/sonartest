package com.bajaj.mutualfunds.core.models;

import java.util.Map;

public class ConfigModel {

    private Map<String, String> configMap;
    private static ConfigModel configModel = null;
    private ConfigModel() {
    }
    public static ConfigModel getInstance() {
        if(configModel==null){
            configModel = new ConfigModel();
        }
        return configModel;
    }
    public void updateConfigMap(Map<String, String> patch) {
        patch.forEach(configMap::put);
    }
    public void setConfigMap(Map<String, String> configMap) {
        this.configMap = configMap;
    }
    public Map<String, String> getConfigMap(){
        return configMap;
    }
    public static String get(String key) {
        return getInstance().getConfigMap().get(key);
    }
}