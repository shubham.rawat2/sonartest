package com.bajaj.mutualfunds.core.services;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "BFDL Mutual Fund - Page Generator Schedular Configuration", description = "BFDL Mutual Fund - Page Generator Schedular Configuration")
public @interface PageGenerationSchedularService {
	@AttributeDefinition(name = "Scheduler name", description = "Name of the scheduler", type = AttributeType.STRING)
	public String schdulerName() default "Mutual Fund Page Generator Schedular Configuration";

	@AttributeDefinition(name = "Enabled", description = "True, if scheduler service is enabled", type = AttributeType.BOOLEAN)
	public boolean enabled() default false;

	@AttributeDefinition(name = "Cron Expression", description = "Cron expression used by the scheduler", type = AttributeType.STRING)
	public String cronExpression() default "0 0 0 ? * * *";

	@AttributeDefinition(name = "Custom Parameter", description = "Custom parameter to be displayed by the scheduler", type = AttributeType.STRING)
	public String customParameter() default "Page Generator Schedular";
}
