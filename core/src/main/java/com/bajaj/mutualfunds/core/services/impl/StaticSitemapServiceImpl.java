package com.bajaj.mutualfunds.core.services.impl;

import com.bajaj.mutualfunds.core.services.ResourceHelper;
import com.bajaj.mutualfunds.core.services.StaticSitemapService;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Session;
import java.text.SimpleDateFormat;
import java.util.Objects;

import com.bajaj.mutualfunds.core.constants.BFDLConstants;

@Component(service = StaticSitemapService.class, immediate = true)
public class StaticSitemapServiceImpl implements StaticSitemapService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceHelper resourceHelper;

    @Override
    public JsonArray getSitemapDetails(String[] pages, String requestDomain) {
        ResourceResolver resolver = null;
        Session session = null;
        try {
            resolver = resourceHelper.getResourceResolver();
            session = resolver.adaptTo(Session.class);
            PageManager pageManager = resolver.adaptTo(PageManager.class);
            JsonArray sitemapArr = new JsonArray();
            SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd");
            String basePath= BFDLConstants.PAGE_PATH;
            for (String pageUrl : pages) {
                JsonObject pageData = new JsonObject();
                try {
                    String pagePath = null;
                    if (pageUrl.contains("/sitemap.xml")) {
                        if (pageUrl.contains(requestDomain)) {
                            pagePath = pageUrl.replace(requestDomain, basePath);
                        }
                        pagePath = pagePath.replace("/sitemap.xml", "");
                    } else if (pageUrl.contains(".html")) {
                        if (pageUrl.contains(requestDomain)) {
                            pagePath = pageUrl.replace(requestDomain, basePath);
                        }
                        pagePath = pagePath.replace(".html", "");
                    } else if (pageUrl.equalsIgnoreCase(requestDomain)) {
                        pagePath = BFDLConstants.PAGE_PATH;
                    }
                    if (Objects.nonNull(pagePath)) {
                        Page sitemapPage = pageManager.getPage(pagePath);
                        pageData.addProperty("pageUrl", pageUrl);
                        pageData.addProperty("lastMod", sfd.format(sitemapPage.getLastModified().getTime()));
                        pageData.addProperty("priority",
                                sitemapPage.getProperties().containsKey("priority")
                                        ? sitemapPage.getProperties().get("priority").toString()
                                        : "0.7");
                    } else {
                        pageData.addProperty("pageUrl", pageUrl);
                        pageData.add("lastMod", null);
                        pageData.addProperty("priority", pageUrl.contains("/emi-store") ? "0.9" : "0.7");
                    }
                    sitemapArr.add(pageData);
                } catch (Exception e) {
                    logger.info("getSitemapDetails iterator ::" + e.getMessage());
                    pageData.addProperty("pageUrl", pageUrl);
                    pageData.add("lastMod", null);
                    pageData.addProperty("priority", pageUrl.contains("/emi-store") ? "0.9" : "0.7");
                    sitemapArr.add(pageData);
                }
            }
            return sitemapArr;
        } catch (Exception e) {
            logger.error("getSitemapDetails ::: " + e.getMessage());
        } finally {
            if (session.isLive())
                session.logout();
            if (resolver.isLive())
                resolver.close();
        }
        return null;
    }
}

