package com.bajaj.mutualfunds.core.services.impl;

import java.util.Collection;
import java.util.Iterator;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.bajaj.mutualfunds.core.caconfig.CaConfigGlobalVariablesDefinition;
import com.bajaj.mutualfunds.core.services.CAConfigService;
import com.bajaj.mutualfunds.core.services.UrlRewriterService;

@Component(service = UrlRewriterService.class, immediate = true)
public class UrlRewriterServiceImpl implements UrlRewriterService {

    @Reference
    CAConfigService configService;

    @Override
    public String rewriteUrl(String responseBody, Resource resource, SlingHttpServletRequest request) {
        String rewroteContent = responseBody;
        Collection configDatas = configService.getConfigDatas(request, resource);

        if (responseBody.contains("%$$")) {
            Iterator sampleCoigIterator = configDatas.iterator();
            while (sampleCoigIterator.hasNext()) {
                CaConfigGlobalVariablesDefinition configData = (CaConfigGlobalVariablesDefinition) sampleCoigIterator
                        .next();

                if (responseBody.contains("%$$" + configData.variableName() + "$$%")) {
                    responseBody = responseBody.replace("%$$" + configData.variableName() + "$$%",
                            configData.variableValue());

                }
            }

            return responseBody;

        }
        return rewroteContent;
    }
}
