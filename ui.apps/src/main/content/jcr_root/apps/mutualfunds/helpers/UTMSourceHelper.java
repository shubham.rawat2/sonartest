package apps.mutualfunds.helpers;

import java.util.Arrays;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import com.adobe.cq.sightly.WCMUsePojo;

public class UTMSourceHelper extends WCMUsePojo{
    private boolean hideHeader = false; 
	private List<String> selectors ;
	private String qid="";
    @Override
	public void activate() throws Exception {
       SlingHttpServletRequest request=getRequest();
        if (request != null) {
		 selectors = Arrays.asList(request.getRequestPathInfo().getSelectors()); 
         hideHeader = selectors.contains("is-mobile");
         for(int i=0;i<selectors.size();i++) {
        	 if(selectors.get(i)!="is-mobile") {
        		 qid=selectors.get(i);
        	 }
         }
        }
		
	}
	
	   public boolean getHideHeader() 
	    { 
	        return hideHeader; 
	        }
	   public String getQuoteId() 
	    { 
	        return qid; 
	        } 

}
