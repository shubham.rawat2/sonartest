package apps.mutualfunds.helpers;

import java.util.Date;

import com.adobe.cq.sightly.WCMUsePojo;

public class MetaTag extends WCMUsePojo{
	private String metaValue="default";
	@Override
	public void activate() throws Exception {
		this.metaValue = get("metaValue",String.class);
	}
	public String getMetaValue() {
		return "<meta " +this.metaValue+ " />";
	}
}