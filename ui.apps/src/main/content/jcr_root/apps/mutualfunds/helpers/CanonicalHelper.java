package apps.mutualfunds.helpers;


import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;

import com.adobe.cq.sightly.WCMUsePojo;

public class CanonicalHelper extends WCMUsePojo{
   public String param; 
   public String value;
@Override
	 public void activate() throws Exception {
	
	        Node currentNode = getResource().adaptTo(Node.class);
	        param = get("propertyName",String.class);
	        value = get("propertyValue",String.class);
	        if(param!="" && value!="") {
		        currentNode.setProperty(param,"https://www.bajajfinservmarkets.in" + value);
		        Session session= getResource().getResourceResolver().adaptTo(Session.class);
		        session.save(); 
	        }
	        
	 }
	public boolean checkCanonicalUrl() throws Exception {
		return findProperty(getResource(),"canonicalurl");
		

	}
	public boolean findProperty(Resource node,String property) throws Exception {
		if(node.getValueMap().containsKey(property)) {
			return true;
		}else {
			return findProperty(node.getParent(),property);
			}
		}
	}


