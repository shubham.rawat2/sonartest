$(function () {
    $('.categories-blog__arrow-left').addClass('arrow-disable');
    $('.categories-blog__scr-tabs li').click(function () {
        let position = $(this).position();
        scroll = $('.categories-blog__scr-tabs').scrollLeft();

        $(this).is(':first-child') ?
            $('.categories-blog__arrow-left').addClass('arrow-disable') :
            $('.categories-blog__arrow-left').removeClass('arrow-disable');

        $(this).is(':last-child') ?
            $('.categories-blog__arrow-right').addClass('arrow-disable') :
            $('.categories-blog__arrow-right').removeClass('arrow-disable');

        $('.categories-blog__scr-tabs').animate({
            'scrollLeft': scroll + position.left - 80
        }, 200);

        $('.categories-blog__scr-tabs li').removeClass('active-scr-tab');
        $(this).addClass('active-scr-tab');
    });
    categories_blog_arrow = (type) => {
        let item_scroll = $('.categories-blog__scr-tabs-item.active-scr-tab');
        if (type == "pre") {
            item_scroll.prev('li').trigger('click');
        }
        else {
            item_scroll.next('li').trigger('click');
        }
    };
});