function renderFilterListFn(objData,eleId){
    var htmlStr='';
    Object.values(objData).forEach(function(ele){
        htmlStr += '<li data-sortfilter="'+ele.value+'" data-sortfilterval="'+ele.name+'">'+ele.name+'</li>';
        console.table(ele.name,ele.value)
     })
     $(eleId).html('<ul>'+htmlStr+'</ul>')
     
}

function renderRatingList(objData,eleId){
    var htmlStr='';
    Object.values(objData).forEach(function(ele,ind){
        htmlStr += '<div class="mf-fund-filterby-stars-row">'+
        '                                    <div class="mf-fund-filter__checkbox">'+
        '                                        <input type="checkbox" id="rating_'+(ind+1)+'" data-sortFilter='+ele.value+'>'+
        '                                        <label for="rating_'+(ind+1)+'"></label>'+
        '                                    </div>'+
        '                                    <div class="mf-peer-rating-col rating-center">'+
        '                                        <div class="mf-peer-rating-pr">'+
        '                                            <div class="cmpc-cd-ratings">'+    
        '                                                <div class="bs-rating rating-'+(ind+1)+' clear-fix">'+
        '                                                    <span class="star"><i class="icon icon-rating-star"></i></span>'+
        '                                                    <span class="star"><i class="icon icon-rating-star"></i></span>'+
        '                                                    <span class="star"><i class="icon icon-rating-star"></i></span>'+
        '                                                    <span class="star"><i class="icon icon-rating-star"></i></span>'+
        '                                                    <span class="star"><i class="icon icon-rating-star"></i></span>'+
        '                                                </div>'+
        '                                            </div>'+
        '                                            <div class="mf-peer-rating-text">'+
        '                                                <span>('+(ind+1)+' Star)</span>'+
        '                                            </div>'+
        '                                        </div>'+
        '                                    </div>'+
        '                                </div>';
        console.table(ele.name,ele.value)
     });
     $(eleId).html(htmlStr)
}

function renderSubCatList(objData,eleId){
    var htmlStr='';
    objData.forEach(function(ele,ind){
        htmlStr += '<li>'+
        '<div class="mf-fund-filter__checkbox">'+
        '<input type="checkbox" id="'+ele.name+'" data-sortSubCatFilter="'+ele.value+'" '+ (ele.isChecked == true ? 'checked' : '')+'>'+
        '<label for="'+ele.name+'">'+ele.name+'</label>'+
        '</div>'+
        '</li>';
     });
     $(eleId).html('<ul>'+htmlStr+'</ul>')
     subTypeChangeEvent();
}