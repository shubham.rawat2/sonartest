document.addEventListener('DOMContentLoaded', function () {
    var riskoMeterValue = document.querySelector('.riskometer').getAttribute('data-risko-value');
    var arrowTransformProperty = '';

    switch (riskoMeterValue) {
        case 'LOW':
            arrowTransformProperty = 'rotate(4deg)';
            break;
        case 'MLW':
            arrowTransformProperty = 'rotate(33deg)';
            break;
        case 'MOD':
            arrowTransformProperty = 'rotate(67deg)';
            break;
        case 'MDH':
            arrowTransformProperty = 'rotate(102deg)';
            break;
        case 'HGH':
            arrowTransformProperty = 'rotate(134deg)';
            break;
        case 'VHG':
            arrowTransformProperty = 'rotate(168deg)';
            break;
    }
    var riskoSvgStr = `<div class="risko-color-code">
    <svg width="100%" height="100%" viewBox="0 0 301 140.99">
        <g id="Group_17907" transform="translate(8294 -9642)">
            <g id="Group_17940" transform="translate(-4409.685 5392.089)">
                <g id="Group_17939" transform="translate(-3855 4270)">
                    <g id="Group_17933" transform="translate(59.337 0)">
                        <g id="Path_30365" fill="#eabf23" stroke-miterlimit="10">
                            <path
                                d="M261.174 114.441V160.8a69.837 69.837 0 0 0-35.928 9.883c-.139.083-.283.172-.427.255l-21.181-34.314-3.233-5.1c.022-.011.044-.028.067-.039a115.934 115.934 0 0 1 60.702-17.044z"
                                class="cls-13" transform="translate(-200.405 -114.441)" />
                            <path
                                d="M253.174 122.735c-14.363 1.059-28.383 4.973-41.512 11.659l16.01 25.935c8.018-3.803 16.646-6.211 25.502-7.119v-30.475m8-8.294v46.36c-13.127 0-25.418 3.605-35.928 9.884-.138.083-.283.172-.427.255l-21.18-34.314-3.234-5.097c.022-.011.044-.028.067-.04 17.675-10.814 38.462-17.048 60.702-17.048z"
                                class="cls-14" transform="translate(-200.405 -114.441)" />
                        </g>
                    </g>
                    <g id="Group_17934" transform="translate(0 62.638)">
                        <g id="Path_30366" fill="#14a25b" stroke-miterlimit="10">
                            <path
                                d="M149.167 250.558a69.829 69.829 0 0 0-9.39 35.085h-46.36a115.967 115.967 0 0 1 15.6-58.263z"
                                class="cls-13" transform="translate(-93.417 -227.381)" />
                            <path
                                d="M112.193 238.454c-5.916 12.25-9.477 25.564-10.481 39.19h30.475c.85-8.285 3.014-16.377 6.405-23.953l-26.399-15.237m-3.18-11.073l40.154 23.177c-5.973 10.322-9.39 22.307-9.39 35.086h-46.36c0-21.226 5.68-41.125 15.596-58.263z"
                                class="cls-14" transform="translate(-93.417 -227.381)" />
                        </g>
                    </g>
                    <g id="Group_17935" transform="translate(16.456 18.544)">
                        <g id="Path_30367" fill="#09bc63" stroke-miterlimit="10">
                            <path
                                d="M186.375 188.136a70.651 70.651 0 0 0-23.083 24.12l-40.2-23.067a117.022 117.022 0 0 1 40.232-41.313z"
                                class="cls-13" transform="translate(-123.088 -147.876)" />
                            <path
                                d="M160.716 201.555c4.282-5.908 9.385-11.218 15.117-15.73l-15.205-26.55c-10.275 7.425-19.218 16.628-26.347 27.113l26.435 15.167m2.576 10.701l-40.204-23.067c9.745-16.904 23.61-31.136 40.232-41.313l23.055 40.26c-9.495 6.067-17.426 14.348-23.083 24.12z"
                                class="cls-14" transform="translate(-123.088 -147.876)" />
                        </g>
                    </g>
                    <g id="Group_17936" transform="translate(124.357 0)">
                        <g id="Path_30368" fill="#f0a225" stroke-miterlimit="10">
                            <path
                                d="M317.638 114.441V160.8a69.837 69.837 0 0 1 35.928 9.883c.139.083.283.172.427.255l21.181-34.314 3.233-5.1c-.022-.011-.044-.028-.067-.039a115.934 115.934 0 0 0-60.702-17.044z"
                                class="cls-13" transform="translate(-317.638 -114.441)" />
                            <path
                                d="M325.638 122.734v30.477c8.795.905 17.39 3.302 25.493 7.136l16.029-25.968c-12.89-6.62-27.008-10.579-41.522-11.645m-8-8.293c22.24 0 43.027 6.234 60.703 17.049.022.011.044.028.066.039l-3.233 5.097-21.18 34.314c-.145-.083-.29-.172-.428-.255-10.51-6.279-22.8-9.884-35.928-9.884v-46.36z"
                                class="cls-14" transform="translate(-317.638 -114.441)" />
                        </g>
                    </g>
                    <g id="Group_17937" transform="translate(188.713 62.638)">
                        <g id="Path_30369" fill="#a52836" stroke-miterlimit="10">
                            <path
                                d="M433.676 250.558a69.829 69.829 0 0 1 9.39 35.085h46.36a115.966 115.966 0 0 0-15.6-58.263z"
                                class="cls-13" transform="translate(-433.676 -227.381)" />
                            <path
                                d="M470.65 238.454L444.25 253.69c3.39 7.576 5.555 15.668 6.405 23.953h30.476c-1.005-13.626-4.566-26.94-10.482-39.19m3.18-11.073c9.917 17.138 15.596 37.037 15.596 58.263h-46.36c0-12.779-3.417-24.764-9.39-35.086l40.154-23.177z"
                                class="cls-14" transform="translate(-433.676 -227.381)" />
                        </g>
                    </g>
                    <g id="Group_17938" transform="translate(164.72 18.544)">
                        <g id="Path_30370" fill="#fd4c60" stroke-miterlimit="10">
                            <path
                                d="M390.414 188.136a70.65 70.65 0 0 1 23.083 24.12l40.2-23.067a117.022 117.022 0 0 0-40.232-41.313z"
                                class="cls-13" transform="translate(-390.414 -147.876)" />
                            <path
                                d="M416.073 201.555l26.435-15.167c-7.129-10.485-16.072-19.688-26.347-27.114l-15.205 26.551c5.733 4.512 10.836 9.822 15.117 15.73m-2.576 10.701c-5.657-9.772-13.588-18.053-23.083-24.12l23.055-40.26c16.622 10.177 30.488 24.409 40.232 41.313l-40.204 23.067z"
                                class="cls-14" transform="translate(-390.414 -147.876)" />
                        </g>
                    </g>
                </g>
            </g>
            <text id="Moderately_High" fill="#ea9d23" font-family="Lato-Regular, Lato"
                font-size="10px" transform="translate(-8122 9652)">
                <tspan x="0" y="0">Moderately High</tspan>
            </text>
            <text id="High" fill="#fd4c60" font-family="Lato-Regular, Lato" font-size="10px"
                transform="translate(-8045 9698)">
                <tspan x="0" y="0">High</tspan>
            </text>
            <text id="Low_to_Moderate" fill="#09bc63" font-family="Lato-Regular, Lato"
                font-size="10px" transform="translate(-8283 9686)">
                <tspan x="0" y="0">Low To</tspan>
                <tspan x="0" y="12">Moderate</tspan>
            </text>
            <text id="Moderate" fill="#eabf23" font-family="Lato-Regular, Lato" font-size="10px"
                transform="translate(-8205 9652)">
                <tspan x="0" y="0">Moderate</tspan>
            </text>
            <text id="Very_High" fill="#a52836" font-family="Lato-Regular, Lato"
                font-size="10px" transform="translate(-8014 9756)">
                <tspan x="0" y="0">Very</tspan>
                <tspan x="0" y="12">High</tspan>
            </text>
            <text id="Low" fill="#14a25b" font-family="Lato-Regular, Lato" font-size="10px"
                transform="translate(-8294 9768)">
                <tspan x="0" y="0">Low</tspan>
            </text>
        </g>
    </svg>
</div>
<div class="risko-arrow" style="transform: ${arrowTransformProperty}">
    <svg xmlns="http://www.w3.org/2000/svg" width="87.332" height="27.098"
        viewBox="0 0 87.332 27.098">
        <g>
            <g fill="#fff">
                <path
                    d="M12.904 75.68H1.096c-.142 0-.273-.058-.37-.163-.095-.105-.14-.241-.128-.383l5.904-64.251c.04-.432.422-.454.498-.454.076 0 .458.022.498.454l5.904 64.25c.013.143-.033.279-.129.384-.096.105-.227.163-.37.163z"
                    transform="translate(-8626.521 -9256.513) rotate(-83) translate(-8150 9692)" />
                <path fill="#ececec"
                    d="M7 10.929l-5.904 64.25h11.808L7 10.93m0-1c.47 0 .94.302.996.908L13.9 75.088c.054.586-.408 1.092-.996 1.092H1.096c-.588 0-1.05-.506-.996-1.092l5.904-64.25c.056-.607.526-.91.996-.91z"
                    transform="translate(-8626.521 -9256.513) rotate(-83) translate(-8150 9692)" />
            </g>
            <path fill="#d3d3d3" d="M-8143 9704.166v63.095h-5.858z"
                transform="translate(-8626.521 -9256.513) rotate(-83)" />
            <g transform="translate(-8626.521 -9256.513) rotate(-83) translate(-8152 9759.18)">
                <circle cx="9" cy="9" r="9" fill="#182132" stroke="#4cbafc"
                    stroke-width="1.5px" />
                <circle cx="6" cy="6" r="6" fill="#ececec" transform="translate(3 3)" />
            </g>
        </g>
    </svg>
    </div>`
    document.querySelector('.riskometer').innerHTML = riskoSvgStr;
    document.querySelector('.txt--green').innerHTML = riskometerMapper[riskoMeterValue].text;
    document.querySelector('.txt--green').style.color = riskometerMapper[riskoMeterValue].color;
});