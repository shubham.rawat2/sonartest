var LastSeenLabel, navObj = {};
var labelToBeRemoved;
var navYearElement = document.getElementById("mf-chart-legend__text");
var fund_returnsElement = document.getElementById("mf-chart-legend__count");
//contains whole rendering chart functionality
function renderChartData(data) {
  navObj = {
    "1Year": getFilteredNavData(data, 1),
    "3Year": getFilteredNavData(data, 3),
    "5Year": getFilteredNavData(data, 5),
    "10Year": getFilteredNavData(data, 10),
    max: getMaxNavData(data),
  };
  createChart(navObj["5Year"].label, navObj["5Year"].range);
}

function getMaxNavData(data) {
  var maxNavData = {
    range: [],
    label: [],
  };
  for (i = 0; i < data.length; i++) {
    maxNavData.range.push(data[i]["latest_nav"].toFixed(2));
    maxNavData.label.push(data[i]["as_of_date"]);
  }
  return maxNavData;
}

function getFilteredNavData(data, noYears) {
  var filteredNavData = {
    range: [],
    label: [],
  };
  var startDate = new Date();
  var currentDate = new Date();
  startDate = new Date(
    startDate.setFullYear(currentDate.getFullYear() - noYears)
  );
  for (var i = 0; i < data.length; i++) {
    var navDate = new Date(data[i]["as_of_date"]);
    if (navDate <= currentDate && navDate >= startDate) {
      filteredNavData.range.push(data[i]["latest_nav"]);
      filteredNavData.label.push(data[i]["as_of_date"]);
    }
  }
  return filteredNavData;
}



function createChart(labels, data) {
  var config = {
    type: "line",
    data: {
      datasets: [
        {
          data: data,
          backgroundColor: "transparent",
          borderWidth: 3,
          borderColor: "#0bbc8a",
          pointHoverPadding: "10",
          pointHoverBorderColor: "#0bbc8a",
          pointHoverBackgroundColor: "#303d4b",
          pointHoverBorderWidth: "3",
          pointHoverRadius: "7",
          pointStyle: "chartPoint",
        },
      ],
      labels: labels,
    },
    options: {
      responsive: true,
      animation: false,
      layout: {
        padding: 0,
      },
      elements: {
        point: {
          radius: "0",
        },
      },
      plugins: {
        datalabels: {
          display: false,
        },
      },
      legend: {
        display: false,
        position: "top",
        align: "start",
        labels: {
          fontColor: "#fff",
          fillStyle: "red"
        }
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              display: false,
              offsetGridLines: true,
              drawOnChartArea: true,
            },
            ticks: {
              fontColor: "#666666",
              fontSize: 12,
              autoSkip: false,
              callback: function (label, index, labels) {
                let currentLabel = label.substr(0, 4);
                if (labelToBeRemoved == undefined) {
                  labelToBeRemoved = currentLabel;
                }
                if (LastSeenLabel != currentLabel) {
                  LastSeenLabel = currentLabel;
                  if (LastSeenLabel !== labelToBeRemoved) {
                    return LastSeenLabel;
                  }
                }
              },
              maxRotation: 0,
              beginAtZero: true,
            },
          },
        ],
        yAxes: [
          {
            gridLines: {
              display: false,
              // offsetGridLines: true,
            },
            ticks: {
              display: false,
            },
          },
        ],
      },
      tooltips: {
        titleFontColor: "#0bbc8a",
        displayColors: false,
        titleAlign: 'center',
        xAlign: 'center',
        bodyAlign: 'center',
        bodyFontSize: 12,
        backgroundColor: "#182132",
        display: false,
        footerFontColor: '#9FA2A6',
        footerFontSize: 9,
        footerFontStyle: 100,
        footerFontWeight: 300,
        callbacks: {
          title: function (data) {
            return `NAV ₹  ${parseFloat(data[0].value).toFixed(2)}`;
          },
          label: function () {
            return "";
          },
          afterFooter: function (data) {
            return `On ${new Date(data[0].label).toLocaleString("en-us", {
              day: "numeric",
              month: "short",
              year: "numeric",
            })}`;
          },
          labelTextColor: function (context) {
            return "#fff";
          },
        },
        caretPadding: 10,
      },
    },
  };
  var mf_annulise_chart = document.getElementById('annualised_return_chart').getContext('2d');
  window.mf_calc_dg = new Chart(mf_annulise_chart, config);
}

for (i = 0; i < document.getElementsByClassName("annualReturnsGraphTenure").length; i++) {
  document.getElementsByClassName("annualReturnsGraphTenure")[i].addEventListener("click", function () {
    for (i = 0; i < document.getElementsByClassName("annualReturnsGraphTenure").length; i++) {
      document.getElementsByClassName("annualReturnsGraphTenure")[i].classList.remove("active-year");
    }
    this.classList.add("active-year");
    if (this.dataset.year == "max") {
      mf_calc_dg.data.labels = navObj.max.label;
      mf_calc_dg.data.datasets[0].data = navObj.max.range;
      mf_calc_dg.update();
      navYearElement.innerHTML = `Max Annualised Return`;
      fund_returnsElement.innerText = `${this.dataset.maxyrreturn || 'NA'}  %`
      checkValuePositiveOrNot();
    } else {
      mf_calc_dg.data.labels = navObj[`${this.dataset.year}Year`].label;
      mf_calc_dg.data.datasets[0].data = navObj[`${this.dataset.year}Year`].range;
      navYearElement.innerHTML = `${this.dataset.year} Yrs Annualised Return`;
      mf_calc_dg.update();
      fund_returnsElement.innerText = this.dataset[`${this.dataset.year}yrreturn`] == undefined ? "NA" : `${this.dataset[`${this.dataset.year}yrreturn`]} %`
      checkValuePositiveOrNot();
    }
  });
}