function renderPopUp() {
    var selectedFund = [];
    dataMapping.selectedFund.forEach(function (fundId) {
        dataMapping.fund.forEach(function (fundScheme) {
            if (Number(fundScheme.amcfundscheme_id) == fundId) {
                selectedFund.push(fundScheme);
            }
        })
    })
    document.querySelector('.searched-fund-cards').innerHTML = '';
    document.querySelector('.mf-comp-bottom-row').innerHTML = '';
    selectedFund.forEach(function (singleData, index) {
        document.querySelectorAll('.searched-fund-cards,.mf-comp-bottom-row').forEach(function (eachTarget) {
            eachTarget.innerHTML += '<div class="mf-fund-compare-col mf-fund-compare__datacard" cardId="' + index + '" fundId="' + singleData.amcfundscheme_id + '">' +
                ' ' + (index == 0 ? '' : '<img src="/content/dam/mutualfunds/fund-comparison/round-cancel-icon.png" alt="close-icon" class="mf-comp-bottom-card-close ">') +
                '<div class="mf-fund-compare-bottom__cardimg">' +
                '    <img src="' + singleData.amc_logo + '">' +
                '</div>' +
                '<div class="mf-comp-bottom-card-titledata">' +
                '    <h3 class="mf-comp-bottom-card-title">' + singleData.bse_scheme_name + '</h3> ' +
                '    <div class="mf-comp-bottom-card-tagrow">' +
                '        <ul>' +
                '            <li>' + singleData.fund_type + '</li>' +
                '           <li>' + singleData.fund_sub_type + '</li>' +
                '            <li>' + singleData.risk_factor + '</li>' +
                '        </ul>' +
                '    </div>' +
                '</div>' +
                '</div>';
        })
    })
    document.querySelector('.mf-fund-compare-mob__count').innerHTML = '0' + dataMapping.selectedFund.length;
    for (var i = dataMapping.selectedFund.length; i < 3; i++) {
        document.querySelector('.searched-fund-cards').innerHTML += '<div class="mf-fund-compare-col mf-fund-compare__dragcard"> <img src="/content/dam/mutualfunds/fund-comparison/add-icon.png"  alt="add-icon"> </div>'
        document.querySelector('.mf-comp-bottom-row').innerHTML += '<div class="mf-fund-compare-col mf-fund-compare__dragcard" cardId="secoundCard"> <p class="add-to-compare-txt">Add to Compare</p> </div>'
    }
    compareFundModelHideShow()
    document.querySelectorAll('.mf-comp-bottom-card-close').forEach(function (item) {
        item.addEventListener('click', function () {
            dataMapping.selectedFund.splice(dataMapping.selectedFund.indexOf(Number(item.parentElement.getAttribute('fundId'))), 1);
            updateDataMapping();
            renderPopUp();
            $('.mf-fund-card__invest--checkbox input').each(function(ind,ele){
                if($(ele).attr('fundid') == item.parentElement.getAttribute('fundId')){
                    $(ele).closest('.mf-fund-return__card').removeClass("mf-fund-return__card--active")
                    $(ele).prop('checked',false)
                }
            })
            // this.parentElement.remove();
        })
    });
}

function searchfilterListPopulation(data) {
    var htmlStr = '';
    var searchEle = document.querySelector('.mf-comp-model-inp-suggection');
    searchEle.innerHTML = '';
    data.forEach(function (singleFund) {
        var titleCap = singleFund.bse_scheme_name.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        htmlStr += '' + '<div class="mf-compare-bar-parent "searchId="' + (Number(singleFund.amcfundscheme_id)) + '">' +
            '    <div class="compare-bar-img-parent">' +
            '        <img src="https://www.bajajfinservmarkets.in/mf/static/' + singleFund.amc_logo + '" alt="Company img" class="lozad">' +
            '    </div>' +
            '    <div class="compare-bar-headline">' +
            '        <p>' + titleCap + '</p>' +
            '    </div>' +
            '    <div class="inp-search-selct-fund-btn fund-select desk-dip-block">' +
            '        <a href="javascript:void(0)">Select Fund</a>' +
            '    </div>' +
            '    <div class="comapre-bar-right mob-dip-block">' +
            '        <p class="compare-right__per--text">' + (Number(singleFund.ret_3year).toFixed(2)) + '%</p>' +
            '        <p class="compare-right__return--text">3Y Return</p>' +
            '    </div>' +
            '</div>';
    });
    searchEle.innerHTML = htmlStr;
    document.querySelectorAll('.inp-search-selct-fund-btn').forEach(function (list) {
        list.addEventListener('click', function () {
            if (dataMapping.selectedFund.length < 3) {
                dataMapping.selectedFund.includes(Number(this.parentElement.getAttribute('searchId'))) ? updateDataMapping() : dataMapping.selectedFund.push(Number(this.parentElement.getAttribute('searchId')));
                updateDataMapping();
                renderPopUp()
            }
        })
    })

    document.querySelectorAll('.mf-compare-bar-parent ').forEach(function (list) {
        list.addEventListener('click', function () {
            if (window.matchMedia("(max-width: 768px)").matches) {
                if (dataMapping.selectedFund.length < 2) {
                    dataMapping.selectedFund.includes(Number(this.getAttribute('searchId'))) ? updateDataMapping() : dataMapping.selectedFund.push(Number(this.getAttribute('searchId')));
                    updateDataMapping();
                    renderPopUp();
                    $('.mf-comp-mode-hide').hide();
                    $('body').removeClass('no-scroll');
                }
            }
        })
    })
}