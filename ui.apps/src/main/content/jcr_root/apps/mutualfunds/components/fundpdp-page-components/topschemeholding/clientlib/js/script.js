$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.topSchemesWrap__head').on('click', function () {
            $(this).toggleClass('top-schemes-arrow-btm');
            $(this).next().slideToggle();
        })
    }
    $('.topSchemeTabWrap__tabName').on("click", function () {
        var topSchemes_tab_id = $(this).attr('data-tab');
        $(this).removeClass('active-tab');
        $('.tabDataInfo').removeClass('active--tabdata');

        $(this).addClass('active-tab').siblings().removeClass('active-tab');
        $("#" + topSchemes_tab_id).addClass('active--tabdata');
    })

})

