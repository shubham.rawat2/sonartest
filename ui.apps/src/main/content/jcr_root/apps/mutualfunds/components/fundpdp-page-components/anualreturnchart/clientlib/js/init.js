document.addEventListener("DOMContentLoaded", function (event) {
  var currentAmc= Number(document.querySelector('.mf-annualised-return-chart-main').getAttribute('amcSchemeId'));
  callNavChartApi(currentAmc);
  checkValuePositiveOrNot();
});
