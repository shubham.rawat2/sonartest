$(document).ready(function () {
    $('.banner--slider').slick({
        autoplay: true,
        autoplaySpeed: 1000,
        speed: 1000,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                autoplay: true,
                autoplaySpeed: 1000,
                speed: 1000,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                dots: true,
            }
        },]
    });
});