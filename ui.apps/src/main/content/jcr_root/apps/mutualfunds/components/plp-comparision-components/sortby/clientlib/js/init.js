$(function () {
    renderSortListFn(dataMapping.fundsfilterData.Sort, sortBytabs)

    $('.mf-fund-sortby-tabs ul li').click(function () {
        if($(this).hasClass('mf-fund-sortby-active-tab')){
            $(this).removeClass('mf-fund-sortby-active-tab');
            dataFilterObj.sortArrData = [];
            if(dataFilterObj.queryValues.length > 0){
                filterApiCallFn(dataFilterObj.queryValues.join('&'));
            }else{
                filterApiCallFn('');
            }
        }else{
            $(this).addClass('mf-fund-sortby-active-tab').siblings().removeClass('mf-fund-sortby-active-tab')
            dataFilterObj.sortArrData = [];
            dataFilterObj.sortArrData.push($(this).data('sortname'));
            sortFilterDataBizFn(dataMapping.filterResponseData);
        }
       
    });
    $('.mf-fund-sortby__clear').click(function () {
        $('.mf-fund-sortby-tabs ul li').removeClass('mf-fund-sortby-active-tab');
        dataFilterObj.sortArrData = [];
        if(dataFilterObj.queryValues.length > 0){
            filterApiCallFn(dataFilterObj.queryValues.join('&'))
        }else{
            filterApiCallFn('')
        }
        
    });

    $('#mf_fund_sortby_model').click(function () {
        $('.mf-fund-sortby').css('display', 'block');
        $('body').addClass('no-scroll');
        $('.mf-fund-sortby-screen-overlay').addClass('mf-sortby-model-open');
    });

    $('#mf_fund_sortby_close').click(function () {
        $('.mf-fund-sortby').css('display', 'none');
        $('body').removeClass('no-scroll');
        $('.mf-fund-sortby-screen-overlay').removeClass('mf-sortby-model-open');
    });
});