$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.mf-compare-risko-title').on('click', function () {
            $(this).toggleClass('mf-compare-risko-section-arrow-btm');
            $(this).next().slideToggle();
        })
    }
    var riskometer_one = function(){
        return Math.round(Math.random() * 100)
    }

    var riskometerOneData = function(){
        return [
            riskometer_one(),
            riskometer_one(),
            riskometer_one(),
            riskometer_one(),

        ];
    };
    var riskometer_one_RandomValue = function (data){
        return Math.max.apply(null , data) * Math.random();
    };

    var data = riskometerOneData();
    var value = riskometer_one_RandomValue(data);

    var config = {
        type : 'gauge',
        data :{
            labels: ['Low', 'Low to Moderate', 'Moderate', 'Moderately High','High','Very High'],
            // labels: ['A', 'B', 'C', 'D','E','F'],
            datasets: [{
                data : [16, 32, 48, 64, 80, 96],
                value : value,
                backgroundColor: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                borderColor : '#182132',
                borderWidth: 4,
                
            }]
        },
        options:{
            responsive : true,
            title : {
                display : true,
            },
            layout: {
                padding: {
                  bottom: 10,
                }
              },
              needle:{
                radiusColor : '#000',
                widthPercentage: 3.2,
                lengthPercentage: 8,
                color: 'rgba(255, 255, 255, 1)'
              },
              valueLabel: {
                display: false,
              },
              plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'end',
                    offset: 0,
                    display: true,
                  formatter:  function (value, context) {
                    return context.chart.data.labels[context.dataIndex];
                  },
                  color: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                  backgroundColor: null,
                  font: {
                    size: 9,
                    weight: 'normal'
                  }
                }
              }
              
        }
    };


    var riskometerChart_one = document.getElementById('riskometer-chart-1').getContext('2d');
    window.riskometer = new Chart(riskometerChart_one , config);  

    window.onload = function(){
        config.data.datasets.forEach(function(datasets){
            datasets.data = riskometerOneData();
            datasets.value = riskometer_one_RandomValue(datasets.data);
        });
        window.riskometer.update();
    }
 
    /* riskometer chart 2 */

    var riskometer_Two = function(){
        return Math.round(Math.random() * 100)
    }

    var riskometerTwoData = function(){
        return [
            riskometer_Two(),
            riskometer_Two(),
            riskometer_Two(),
            riskometer_Two(),

        ];
    };
    var riskometer_Two_RandomValue = function (data){
        return Math.max.apply(null , data) * Math.random();
    };

    var data = riskometerTwoData();
    var value = riskometer_Two_RandomValue(data);

    var config = {
        type : 'gauge',
        data :{
            labels: ['Low', 'Low to Moderate', 'Moderate', 'Moderately High','High','Very High'],
                     datasets: [{
                data : [16, 32, 48, 64, 80, 96],
                value : value,
                backgroundColor: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                borderColor : '#182132',
                borderWidth: 4,
                
            }]
        },
        options:{
            responsive : true,
            title : {
                display : true,
            },
            layout: {
                padding: {
                  bottom: 10,
                }
              },
              needle:{
                radiusColor : '#000',
                widthPercentage: 3.2,
                lengthPercentage: 8,
                color: 'rgba(255, 255, 255, 1)'
              },
              valueLabel: {
                display: false,
              },
              plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'end',
                    offset: 0,
                    display: true,
                  formatter:  function (value, context) {
                    return context.chart.data.labels[context.dataIndex];
                  },
                  color: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                  backgroundColor: null,
                  font: {
                    size: 9,
                    weight: 'normal'
                  }
                }
              }
              
        }
    };
    var riskometerChart_Two = document.getElementById('riskometer-chart-2').getContext('2d');
    window.riskometer = new Chart(riskometerChart_Two , config);  

    window.onload = function(){
        config.data.datasets.forEach(function(datasets){
            datasets.data = riskometerTwoData();
            datasets.value = riskometer_Two_RandomValue(datasets.data);
        });
        window.riskometer.update();
    }

    var riskometer_Three = function(){
        return Math.round(Math.random() * 100)
    }

    var riskometerThreeData = function(){
        return [
            riskometer_Three(),
            riskometer_Three(),
            riskometer_Three(),
            riskometer_Three(),

        ];
    };
    var riskometer_Three_RandomValue = function (data){
        return Math.max.apply(null , data) * Math.random();
    };

    var data = riskometerThreeData();
    var value = riskometer_Three_RandomValue(data);

    var config = {
        type : 'gauge',
        data :{
            labels: ['Low', 'Low to Moderate', 'Moderate', 'Moderately High','High','Very High'],
            datasets: [{
                data : [16, 32, 48, 64, 80, 96],
                value : value,
                backgroundColor: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                borderColor : '#182132',
                borderWidth: 4,
                
            }]
        },
        options:{
            responsive : true,
            title : {
                display : true,
            },
            layout: {
                padding: {
                  bottom: 10,
                }
              },
              needle:{
                radiusColor : '#000',
                widthPercentage: 3.2,
                lengthPercentage: 8,
                color: 'rgba(255, 255, 255, 1)'
              },
              valueLabel: {
                display: false,
              },
              plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'end',
                    offset: 0,
                    display: true,
                  formatter:  function (value, context) {
                    return context.chart.data.labels[context.dataIndex];
                  },
                  color: ['#14a25b', '#09bc63', '#eabf23','#f0a225','#fd4c60','#a52836'],
                  backgroundColor: null,
                  font: {
                    size: 9,
                    weight: 'normal'
                  }
                }
              }
              
        }
    };
    var riskometerChart_Three = document.getElementById('riskometer-chart-3').getContext('2d');
    window.riskometer = new Chart(riskometerChart_Three , config);  

    window.onload = function(){
        config.data.datasets.forEach(function(datasets){
            datasets.data = riskometerThreeData();
            datasets.value = riskometer_Three_RandomValue(datasets.data);
        });
        window.riskometer.update();
    }
    
});