function populateFilterdFundList(data) {
    var htmlStr = '';
    var searchEle = document.querySelector('.inp-search-suggection-wrapper');
    searchEle.innerHTML = '';
    data.forEach(function (singleFund) {
        htmlStr += '' + '<div class="mf-compare-bar-parent "schemeId="' + (Number(singleFund.amcfundscheme_id)) + '">' +
            '    <div class="compare-bar-img-parent">' +
            '        <img src="https://www.bajajfinservmarkets.in/mf/static/' + singleFund.amc_logo + '" alt="Company img" class="lozad">' +
            '    </div>' +
            '    <div class="compare-bar-headline">' +
            '        <p>' + singleFund.bse_scheme_name + '</p>' +
            '    </div>' +
            '    <div class="inp-search-selct-fund-btn pdp-comp-sel-fund desk-dip-block">' +
            '        <a href="javascript:void(0)">Select Fund</a>' +
            '    </div>' +
            '    <div class="comapre-bar-right mob-dip-block">' +
            '        <p class="compare-right__per--text">' + (Number(singleFund.ret_3year).toFixed(2)) + '%</p>' +
            '        <p class="compare-right__return--text">3Y Return</p>' +
            '    </div>' +
            '</div>';
    });
    searchEle.innerHTML = htmlStr;

    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.inp-search-suggection-wrapper .mf-compare-bar-parent').click(function () {
            var selectedFund = this.attributes['schemeId'].value;
            setSelectedFund(selectedFund);

            $('.mf-compare-search-bar').show().css({ "height": "380px" });
            $('.inp-search-suggection-wrapper').hide();
            $('.input-search-bar').hide();
            $('.mf-compare-selected-bar').show();
            
        })
    }

    $('.pdp-comp-sel-fund').click(function () {
        $('.inp-search-suggection-wrapper, .input-search-bar').hide();
        var selectedFund = this.parentElement.attributes['schemeId'].value;
        setSelectedFund(selectedFund)
        $('#compare-section-btn').removeAttr('disabled');
        $('.mf-compare-search-bar').css({ 'padding': '0' });
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('.mf-compare-search-bar').show().css({ "height": "380px" });
            $('.mf-compare-search-bar').css({ 'padding': '15px' });
        } else {
            $('.mf-compare-search-bar').show().css({ "height": "auto" });
        }
        $('.mf-compare-selected-bar').show();
    })
}