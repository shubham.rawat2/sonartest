$(document).ready(function () {

    $('.mf-accordian__title').on('click', function () {
        var faq_accord = $(this).closest('.mf-faq-accordian-content-wrap').find('.mf-accordian-data');
        $(this).closest('.mf-faq-accordian-wrap').find('.mf-accordian-data').not(faq_accord).slideUp();
        if ($(this).hasClass('mf-faq-active-accordian')) {
            $(this).removeClass('mf-faq-active-accordian');
        } else {
            $(this).closest('.mf-faq-accordian-wrap').find('.mf-accordian__title.mf-faq-active-accordian').removeClass('mf-faq-active-accordian');
            $(this).addClass('mf-faq-active-accordian');
        }
        faq_accord.slideToggle();
    })
});