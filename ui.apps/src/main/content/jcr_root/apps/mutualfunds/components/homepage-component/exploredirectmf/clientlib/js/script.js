
$(document).ready(function () {
    if (window.matchMedia("(min-width : 768px)").matches) {
        $('.iconSlider').slick({
            lazyLoad: 'ondemand',
            infinite: false,
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: true,
        });
    }
});