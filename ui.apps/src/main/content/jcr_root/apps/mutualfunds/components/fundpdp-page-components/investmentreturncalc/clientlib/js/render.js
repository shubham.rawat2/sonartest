function renderCalcResult(result){
    var calcLabel=document.getElementsByClassName('output-value-txt');
    calcLabel[0].innerHTML="₹ " + Math.round(result.totalInvestment).toLocaleString("EN-IN");
    calcLabel[1].innerHTML="₹ " + Math.round(result.totalProfit).toLocaleString("EN-IN");
    calcLabel[2].innerHTML="₹ " + Math.round(result.result).toLocaleString("EN-IN");;

}