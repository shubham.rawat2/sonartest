var isRenderFooter=false;
function accordian() {
    try {
        $(document).mouseup(function(t) {
            var e = $(".bs-side-menu");
            e.is(t.target) || 0 !== e.has(t.target).length || ($(".menu-list.bs-accordian").find(".accordian-content").stop(!0, !0).slideUp("active"), $(".menu-list.bs-accordian").find(".accordian-heading").removeClass("active"))
        }), ($(window).width() < 767 ? $(".bs-accordian") : $(".bs-accordian:not(.secondary-link)")).find(".accordian-heading").on("click", function(t) {
            $(this).hasClass("active") ? ($(this).parents(".bs-accordian").find(".accordian-content").stop(!0, !0).slideUp("active"), $(this).parents(".bs-accordian").find(".accordian-heading").removeClass("active"), $(this).parents(".bs-accordian").find(".accordian-heading").parent().removeClass("active")) : ($(this).parents(".bs-accordian").find(".accordian-content").stop(!0, !0).slideUp("active"), $(this).parents(".bs-accordian").find(".accordian-heading").removeClass("active"), $(this).parents(".bs-accordian").find(".accordian-heading").parent().removeClass("active"), $(this).siblings(".accordian-content").stop(!0, !0).slideDown("active"), $(this).addClass("active"), $(this).parent().addClass("active")), $(this).parent().parent().find(".accordian-heading.active").length < 1 ? $(this).parent().parent().removeClass("hasactive") : $(this).parent().parent().addClass("hasactive")
        })
    } catch (t) {
        console.log(t)
    }
}
document.addEventListener('DOMContentLoaded', function (event) {
    accordian();
    document.querySelector('.wb-f-acc-l0-fn').addEventListener('click', function () {
        $(this).toggleClass('active').next().slideToggle();
        if(!isRenderFooter){
            $("#bfl-f-ourpartner .wb-f-acc-l0-content").html(ourpartnerHtml);
            isRenderFooter=true;
            document.querySelectorAll('.wb-acc-l1-head').forEach(function (eachhead) {
                eachhead.addEventListener('click', function () {
                    $(eachhead).toggleClass('selected').siblings().removeClass('selected');
                    $(eachhead).next().slideToggle().siblings('.wb-acc-l1-content').hide();
                })
            })
            if (window.matchMedia("(min-width: 992px)").matches) {
                var mediaLength = 6;
                document.querySelectorAll('.wb-f-acc-l1-wrapper').forEach(function (eachwrapper) {
                    var x = $(eachwrapper).find('.wb-acc-l1-content');
                    var y = $(eachwrapper).find('.wb-acc-l1-head');
                    var loop = Math.ceil(x.length / mediaLength);
                    for (var i = 0; i < loop; i++) {
                        x.slice(i * mediaLength, (i + 1) * mediaLength).css('order', "".concat(i + 1));
                        y.slice(i * mediaLength, (i + 1) * mediaLength).css('order', "".concat(i));
                    }
                });
            } else if (window.matchMedia("(min-width: 768px)").matches) {
                var mediaLength = 4;
                document.querySelectorAll('.wb-f-acc-l1-wrapper').forEach(function (eachwrapper) {
                    var x = $(eachwrapper).find('.wb-acc-l1-content');
                    var y = $(eachwrapper).find('.wb-acc-l1-head');
                    var loop = Math.ceil(x.length / mediaLength);
                    for (var i = 0; i < loop; i++) {
                        x.slice(i * mediaLength, (i + 1) * mediaLength).css('order', "".concat(i + 1));
                        y.slice(i * mediaLength, (i + 1) * mediaLength).css('order', "".concat(i));
                    }
                });
            }
            document.querySelectorAll('.wf-content-head-plus').forEach(function (eachhead) {
                eachhead.addEventListener('click', function () {
                    $(this).parent().toggleClass('expanded');
                    $(this).parents('.wb-f-acc-10-content-m').siblings().find('.wb-f-content-head').removeClass('expanded');
                    $(this).parent().next().slideToggle();
                    $(this).parents('.wb-f-acc-10-content-m').siblings().find('.wb-f-acc-l1-wrapper').slideUp();
                })
            })
        }
    });
})