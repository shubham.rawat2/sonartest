// biz.js 

function sortTopPerformingData(data) {
    data.sort((a, b) => b.vr_rating - a.vr_rating);
}

function topPerformingPopulation(data) {
    sortTopPerformingData(data)
    var activeTab = document.querySelector('.active-fund-tab').getAttribute('data-tab');
    var activeFund = document.querySelector('.current-tab').getAttribute('id');
    if (activeTab == activeFund) {
        renderTopPerfomingsFunds(data)
    }

}