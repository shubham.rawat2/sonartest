
$(document).ready(function () {
    $('.happyCustCardSlider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1.1,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        }, ]
    });
  });