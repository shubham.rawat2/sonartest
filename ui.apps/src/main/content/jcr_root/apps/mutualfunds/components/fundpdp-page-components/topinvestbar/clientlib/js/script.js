document.addEventListener("DOMContentLoaded", function () {
    var topinvest = document.getElementById("top-investbar-title").innerText;
    topinvest = topinvest.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
    document.getElementById("top-investbar-title").innerText = topinvest;

    $('[icon-type="share"]').click(function () {
        var currnetPageUrl = window.location.href;
        var data = {
            "title": document.title,
            "url": currnetPageUrl
        }
        navigator.share(data);
    });
});



