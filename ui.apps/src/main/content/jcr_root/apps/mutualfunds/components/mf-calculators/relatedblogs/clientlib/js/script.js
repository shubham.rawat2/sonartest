
$(document).ready(function () {
    if (window.matchMedia("(max-width: 767px)").matches) {
        $('.lumsumBlog-slick').slick({
            centerPadding: '20px',
            infinite: false,
            slidesToShow: 1.2,
            slidesToScroll: 1,
            arrows: false,
        });
    }
    else if (window.matchMedia("(min-width: 768px) and (max-width:1024px)").matches) {
        $('.lumsumBlog-slick').slick({
            centerPadding: '20px',
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
        });
    }
});
