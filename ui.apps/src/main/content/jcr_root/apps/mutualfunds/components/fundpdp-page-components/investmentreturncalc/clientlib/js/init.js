$(document).ready(function () {
    if (document.getElementById('invst-calc-sip-range-saving-amt')) {
        var invst_sip_saving_amt_sldr = document.getElementById('invst-calc-sip-range-saving-amt');
        var invst_sip_saving_amt_inp = $('#mf-investment-sip-saving-amt-inp');
        noUiSlider.create(invst_sip_saving_amt_sldr, {
            start: 100,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal',
            range: {
                'min': Number(pdpCalcObj["sipinvestmintxt"].replace(/,/g, '')),
                'max': Number(pdpCalcObj["sipinvestmaxtxt"].replace(/,/g, '')),
            },
        });

        invst_sip_saving_amt_sldr.noUiSlider.on('update', function (val) {
            invst_sip_saving_amt_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
            pdpcalculation();
        });

        invst_sip_saving_amt_inp.on('change', function () {
            var invst_sip_saving_amt = $(this).val().replace(/,/g, '');
            invst_sip_saving_amt_sldr.noUiSlider.set(invst_sip_saving_amt);
            pdpcalculation();
        });
    }
    if (document.getElementById('invst-calc-sip-range-tenure')) {
        var invst_sip_tenure_sldr = document.getElementById('invst-calc-sip-range-tenure');
        var invst_sip_tenure_inp = $('#mf-sip-tenure-inp');
        noUiSlider.create(invst_sip_tenure_sldr, {
            start: 1,
            connect: [true, false],
            step: 2,
            orientation: 'horizontal',
            range: {
                'min': Number(pdpCalcObj["sipyeartenuremintxt"].replace(/,/g, '')),
                'max': Number(pdpCalcObj["sipyeartenuremaxtxt"].replace(/,/g, '')),
            },
        });

        invst_sip_tenure_sldr.noUiSlider.on('update', function (val) {
            invst_sip_tenure_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
            pdpcalculation();
        });

        invst_sip_tenure_inp.on('change', function () {
            var invst_sip_tenure = $(this).val().replace(/,/g, '');
            invst_sip_tenure_sldr.noUiSlider.set(invst_sip_tenure);
            pdpcalculation();
        });
    }
    if (document.getElementById('invst-calc-lumpsum-range-saving-amt')) {
        var invst_lumpsum_saving_amt_sldr = document.getElementById('invst-calc-lumpsum-range-saving-amt');
        var invst_lumpsum_saving_amt_inp = $('#mf-investment-lumpsum-amt-inp');
        noUiSlider.create(invst_lumpsum_saving_amt_sldr, {
            start: 100,
            connect: [true, false],
            step: 1,
            orientation: 'horizontal',
            range: {
                'min': Number(pdpCalcObj["luminvestmintxt"].replace(/,/g, '')),
                'max': Number(pdpCalcObj["luminvestmaxtxt"].replace(/,/g, ''))
            },
        });

        invst_lumpsum_saving_amt_sldr.noUiSlider.on('update', function (val) {
            invst_lumpsum_saving_amt_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
            pdpcalculation();
        });

        invst_lumpsum_saving_amt_inp.on('change', function () {
            var invst_lumpsum_saving_amt = $(this).val().replace(/,/g, '');
            invst_lumpsum_saving_amt_sldr.noUiSlider.set(invst_lumpsum_saving_amt);
            pdpcalculation();
        });
    }
    if (document.getElementById('invst-calc-lumpsum-range-tenure')) {
        var invst_lumpsum_tenure_sldr = document.getElementById('invst-calc-lumpsum-range-tenure');
        var invst_lumpsum_tenure_inp = $('#mf-invst-lumpsum-tenure-inp');
        noUiSlider.create(invst_lumpsum_tenure_sldr, {
            start: 1,
            connect: [true, false],
            step: 2,
            orientation: 'horizontal',
            range: {
                'min': Number(pdpCalcObj["lumyeartenmintxt"].replace(/,/g, '')),
                'max': Number(pdpCalcObj["lumyeartenmaxtxt"].replace(/,/g, ''))
            },
        });

        invst_lumpsum_tenure_sldr.noUiSlider.on('update', function (val) {
            invst_lumpsum_tenure_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
            pdpcalculation();
        });

        invst_lumpsum_tenure_inp.on('change', function () {
            var invst_lumpsump_tenure = $(this).val().replace(/,/g, '');
            invst_lumpsum_tenure_sldr.noUiSlider.set(invst_lumpsump_tenure);
            pdpcalculation();
        });
    }

});
document.addEventListener("DOMContentLoaded", function (event) {

    $('.investmentCalcTabWrap__tabName').on("click", function () {
        var invst_calc_tab_id = $(this).attr('data-tab');
        $(this).removeClass('active--investmentcalctab');
        $('.investmentcalculator').removeClass('active-invetment-calculator-tab');

        $(this).addClass('active--investmentcalctab').siblings().removeClass('active--investmentcalctab');
        $("#" + invst_calc_tab_id).addClass('active-invetment-calculator-tab');
        pdpcalculation();
    })
});

function isNumber(e, t) {
    if (((e = e || window.event).type = "input"))
        return (
            (e.target.value = e.target.value.replace(/[^0-9]/g, "")),
            e.target.value.length > t &&
            (e.target.value = e.target.value.slice(0, t)),
            /[0-9]/g.test(e.data)
        );
}