$(document).ready(function(){
    $('.advInvestingInMFAccordianWrap').on('click',function(){
        $(this).toggleClass('currently-active-investment-accordian').siblings().removeClass('currently-active-investment-accordian');
         $(this).children('.investment-mf-accordian-content').slideToggle();
        $(this).siblings().find('.investment-mf-accordian-content').slideUp();
        if ($(this).children('.investment-mf-accordian__title').hasClass('investment-mf-active-accordian')) {
            $(this).children('.investment-mf-accordian__title').removeClass('investment-mf-active-accordian'); 
        } else {
            $(this).closest('.investment-mf-accordian-pr').find('.investment-mf-accordian__title.investment-mf-active-accordian').removeClass('investment-mf-active-accordian');
            $(this).children('.investment-mf-accordian__title').addClass('investment-mf-active-accordian');
        }
        var accord_id = $(this).attr('data-tab');
        $('.investmentvideocardWrapper').removeClass('current');
        $("#"+accord_id).addClass('current');
    });
});
