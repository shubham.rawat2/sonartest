function fundReturnTabFn(objData, eleId) {
    var htmlStr = '';
    Object.values(objData).forEach(function (ele) {
        htmlStr += '<li data-fundreturnval="' + ele.value + '" data-labelname="' + ele.labelName + '"><a href="javascript:void(0)">' + ele.name + '</a></li>';
    })
    $(eleId).html('<ul>' + htmlStr + '</ul>');
}

function fundSchemeCardsRenderFn(objData, eleId) {
    // $('#loader').show();
    var htmlStr = '';
    $(eleId).html('');
    $('#pagination-container').html('');
    var activetab = $('.mf-fund-return--filter .active-filter').data('fundreturnval');
    if (objData.length > 0) {
        objData.forEach(function (ele,ind) {
            htmlStr += '<div class="mf-fund-return__card">' +
                '                            <div class="mf-fund-card-col mf-fund-card-title-col">' +
                '                                <div class="mf-fund-return__card--img">' +
                '                                    <img class="lozad" src="https://bfsd.qa.bfsgodirect.com/' + ele.amc_logo + '"' +
                '                                        alt="Bank logo">' +
                '                                </div>' +
                '                                <div class="mf-fund-return__titlehead">' +
                '                                    <h2 class="mf-fund-return__card--title">' + ele.name + '</h2>' +
                '                                    <div class="mf-fund-return__card-tagrow">' +
                '                                        <ul>' +
                '                                            <li>' + (ele.type == null || '' ? '-' : ele.type) + '</li>' +
                '                                            <li>' + (ele.fund_type == null || '' ? '-' : ele.fund_type) + '</li>' +
                '                                            <li>' + (ele.AMCFund__risk_factor == null || '' ? '-' : fundRiskMapper[ele.AMCFund__risk_factor]) + '</li>' +
                '                                        </ul>' +
                '                                    </div>' +
                '                                 </div>' +
                '                                <div class="mf-fund-return__headarrow">' +
                '                                    <img src="/content/dam/mutualfunds/fund-comparison/arrow.png"' +
                '                                        alt="Arrow icon">' +
                '                                </div>' +
                '                            </div>' +
                '                            <div class="mf-fund-card-col mf-fund-card-rating-col">' +
                '                                <div class="mf-fund-return__card--ratingcol">' +
                '                                    <p class="mf-fund-return__card--lable">Min. Invest.</p>' +
                '                                    <h3 class="mf-fund-return__card--conut">₹ ' + (ele.minimum_investment_amount == null || '' ? '-' : (ele.minimum_investment_amount)) + '</h3>' +
                '                                </div>' +
                '                                <div class="mf-fund-return__card--ratingcol">' +
                '                                    <p class="mf-fund-return__card--lable">' + $('.mf-fund-return--filter .active-filter').data('labelname') + '</p>' +
                '                                    <h3 class="mf-fund-return__card--conut">' + (ele[activetab] == null || '' ? '-' : (ele[activetab]).toFixed(2)) + ' %</h3>' +
                '                                </div>' +
                '                                <div class="mf-fund-return__card--ratingcol mf-fund-ds-flex-2">' +
                '                                    <p class="mf-fund-return__card--lable">Value Research Rating</p>' +
                '                                    <div class="mf-fund-return__card--stingstar">' +
                '                                        <div class="cmpc-cd-ratings">' +
                '                                            <div class="bs-rating rating-' + (ele.vr_rating == null || '' ? '-' : ele.vr_rating) + ' clear-fix">' +
                '                                                <span class="star"><i class="icon icon-rating-star"></i></span>' +
                '                                                <span class="star"><i class="icon icon-rating-star"></i></span>' +
                '                                                <span class="star"><i class="icon icon-rating-star"></i></span>' +
                '                                                <span class="star"><i class="icon icon-rating-star"></i></span>' +
                '                                                <span class="star"><i class="icon icon-rating-star"></i></span>' +
                '                                            </div>' +
                '                                        </div>' +
                '                                    </div>' +
                '                                </div>' +
                '                            </div>' +
                '                            <div class="mf-fund-card-col mf-fund-card-invite-col">' +
                '                                <div class="mf-fund-card__invest--checkbox">' +
                '                                    <input type="checkbox" id="compare'+(ind+1)+'" fundId=' + ele.amcfundscheme_id + '>' +
                '                                    <label for="compare'+(ind+1)+'">Add to Compare</label>' +
                '                                </div>' +
                '                                <a href="javascript:void(0)" class="mf-fund-card__invest--btn">Invest Now</a>' +
                '                            </div>' +
                '                        </div>';

        })
        $(eleId).html(htmlStr);
        paginationFn(objData.length);
        addCompareEventReInitialize();
    } else {
        htmlStr = '<div class="mf-fund-return__card"><div class="mf-fund-card-col mf-fund-card-title-col"><h2 class="no-response-mf-fund-return__card--title">No Funds Found</h2></div></div>'
        $(eleId).html(htmlStr);
    }
    // $('#loader').hide();
}