$(document).ready(function(){
    $('.mf-fund-return--filter ul li').click(function () {
        $(this).addClass('active-filter').siblings().removeClass('active-filter');
        //render card FN Call
        if (dataFilterObj.sortArrData.length > 0) {
            sortFilterDataBizFn(dataMapping.filterResponseData);
        } else {
            fundSchemeCardsRenderFn(dataMapping.filterResponseData, renderFilterCards);
        }
    
    });
    
    $('.disabled img').attr('src', '/content/dam/mutualfunds/fund-comparison/Group 22126.svg');
});