
$(document).ready(function () {
    var setTimerslickfirst= Number(slideTimer.logotimerfirst) > 500 ? Number(slideTimer.logotimerfirst) : 3000;
    var setTimerslicksecond= Number(slideTimer.logotimersecond) > 500 ? Number(slideTimer.logotimersecond) : 3000;
    var setTimerslickthird= Number(slideTimer.logotimerthird) > 500 ? Number(slideTimer.logotimerthird) : 3000;
    $('.trustedPartners-slide-1').slick({
        autoplaySpeed: setTimerslickfirst,
        speed : 2000,
        autoplay: true,
        infinite: true,
        slidesToShow: 3,
        centerMode: true,
        variableWidth: true,
        arrows: false,
     });
    $('.trustedPartners-slide-2').slick({
        autoplaySpeed: setTimerslicksecond,
        speed : 2000,
        autoplay: true,
        infinite: true,
        slidesToShow: 2,
        arrows: false,
        dots: false,
        centerMode: true,
        variableWidth: true,
    }); 
    $('.trustedPartners-slide-3').slick({
        autoplaySpeed: setTimerslickthird,
        speed : 2000,
        autoplay: true,
        infinite: true,
        slidesToShow: 3,
        centerMode: true,
        variableWidth: true,
        arrows: false,
     });
});
