function filterdfunds(filter) {
    var filterdData = [];
    var compareFunds = dataMapping.fund;
    for (var i = 0; i < compareFunds.length; i++) {
        if ((compareFunds[i]['bse_scheme_name'].toUpperCase()).indexOf(filter.toUpperCase()) > -1 ) {
            filterdData.push(compareFunds[i]);
        }
    }
    populateFilterdFundList(filterdData)
}
document.querySelector('#compare-search-inp').addEventListener('keyup', function () {
    if (this.value.trim().length >= 3) {
        filterdfunds(this.value.trim());
    }else{
        document.querySelector('.inp-search-suggection-wrapper').innerHTML = '';
    }
})



function setSelectedFund(id) {
    (dataMapping.fund).forEach(function (data) {
        if (Number(data.amcfundscheme_id) == Number(id)) {
            document.querySelector('.mf-compare-selected-bar').innerHTML = '' + '<div class="mf-compare-bar-parent "schemeId="'
                + (Number(data.amcfundscheme_id)) + '" onclick="selectFund("open") ">' +
                '    <div class="compare-bar-img-parent">' +
                '        <img src="https://www.bajajfinservmarkets.in/mf/static/' + data.amc_logo + '" alt="Company img" class="lozad">' +
                '    </div>' +
                '    <div class="compare-bar-headline">' +
                '        <p>' + data.bse_scheme_name + '</p>' +
                '    </div>' +
                '    <div class="comapre-bar-right">' +
                '        <p class="compare-right__per--text">' + (Number(data.ret_3year).toFixed(2)) + '%</p>' +
                '        <p class="compare-right__retur n--text">3Y Return</p>' +
                '    </div>' +
                '    <div class="mf-compare-selected-bar__cancel" onclick="selectFund("close")">' +
                '        <i class="cancel-icon"></i>' +
                '    </div>' +
                '</div>';
        }
    })
    $('.mf-compare-selected-bar__cancel').click(function () {
        $('.input-search-bar , .inp-search-suggection-wrapper').show();
        $('#compare-section-btn').attr("disabled", true);
        $('.mf-compare-search-bar').css({ 'padding': '15px' });
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('.mf-compare-search-bar').show().css({ "height": "650px" });
            $('.mf-compare-mob-search-bar').show();
            $('.mob-compare-bottom-btn').show();
        }
        else {
            $('.mf-compare-search-bar').show().css({ "height": "auto" });
        }
        $('.mf-compare-selected-bar').hide();
    })
}

document.querySelector('#compare-section-btn').addEventListener('click',function(){
    dataMapping.selectedFundToCompare.fund1 = document.querySelector('.mf-compare-bar-parent').attributes['schemeid'].value;
    dataMapping.selectedFundToCompare.fund2 = document.querySelector('.mf-compare-selected-bar').childNodes[0].attributes['schemeid'].value;
    updateDataMapping();
})