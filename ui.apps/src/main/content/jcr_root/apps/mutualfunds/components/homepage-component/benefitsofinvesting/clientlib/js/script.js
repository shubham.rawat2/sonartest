$(document).ready(function () {
    if (window.matchMedia('(min-width: 768px)')) {
        var cfg = { rootMargin: '0px 0px -100% 0px', threshold: 0 }
        var compsec = document.querySelectorAll('.benefit_inner');
        var cb = function (ents) {
            ents.forEach(function (ent) {
                if (ent.isIntersecting) {
                    var entI = $(ent.target).index();
                    $('.benefitsTabBox').eq(entI).addClass('active--benifittab').siblings().removeClass('active--benifittab');
                    $('.benefitsTabsDeskCardWrap').eq(entI).removeClass('hide-div').siblings().addClass('hide-div');
                }
            });
        };
        var sc = new IntersectionObserver(cb, cfg);
        compsec.forEach(function (ele) {
            sc.observe(ele);
        })
    }
});