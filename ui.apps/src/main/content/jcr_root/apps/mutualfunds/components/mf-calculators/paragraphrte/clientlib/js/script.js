$(document).ready(function () {
    function paraAddViewMore() {

        var viewMoreTxt = "View More";
        var viewLessTxt = "View Less";

        $('.addViewMore').each(function () {
            if ($(this).find(".firstSection").length)
                return;

            let allString = $(this).text();

            if (window.matchMedia('(max-width: 767px)').matches) {
                charlmt = 120;
            }else{
                var charlmt = 310;
            }
            let firstSet = allString.substring(0, charlmt);
            let secondHalf = allString.substring(charlmt, allString.length);
            let strToAdd = firstSet + "<span class='SecSec'>" + secondHalf + "</span><span class='viewMore' title='Click to Show More'>" + viewMoreTxt + "</span><span class='viewLess' title='click to show Less Content'>" + viewLessTxt + "</span>";
            $(this).html(strToAdd);
        });
        $(document).on("click", ".viewMore,.viewLess", function () {
            $(this).closest(".addViewMore").toggleClass("showlesscontent showmorecontent");
        });
        $(document).on("click", ".viewMore", function () {
            $(this).closest(".addViewMore").removeClass("less-height");
        });
        $(document).on("click", ".viewLess", function () {
            $(this).closest(".addViewMore").addClass("less-height");
        });

    }
    paraAddViewMore();
});
