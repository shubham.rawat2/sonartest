function renderCalculatorData(data) {
    mf_calc_dg.data.datasets[0].data = [Number(data.firsttotalInvestment.toFixed()), Number(data.firsttotalGain.toFixed())];
    mf_calc_dg.update();
    $(".doughnut-tot-amm").text("₹" + Number(data.firstresult.toFixed()).toLocaleString('EN-IN'));
    $(".gain-amnt").text("₹" + Number(data.firsttotalGain.toFixed()).toLocaleString('EN-IN'));
    $(".invest-amt").text("₹" + Number(data.firsttotalInvestment.toFixed()).toLocaleString('EN-IN'));
    $(".gain-amnt-chart").text("₹" + Number(data.secondtotalGain.toFixed()).toLocaleString('EN-IN'));
    $(".invest-amt-chart").text("₹" + Number(data.secondtotalInvestment.toFixed()).toLocaleString('EN-IN'));
    $(".investment-amt").text("₹" + Number(data.amount.toFixed()).toLocaleString('EN-IN'));
    $(".investment-tenure").text(data.tenure * 2 + " Years");
    $(".investment-roi").text(data.returnRate + "%");
}