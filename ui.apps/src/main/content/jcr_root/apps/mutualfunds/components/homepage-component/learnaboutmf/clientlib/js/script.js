$(document).ready(function () {
    $('.learnAboutMfvideoCards--videoCardSlider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        },
        {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1.2,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        },]
    });
});


