$(document).ready(function () {
    $('.mf-invst-obj__title').click(function () {
        $(this).toggleClass('arrow-bottom');
        $('.mf-invst-obj__main').slideToggle();
    })
    var investmentmobobjhead = document.querySelector('.mf-invst-obj__main-head').innerText;
    investmentmobobjhead = investmentmobobjhead.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
    document.querySelector('.mf-invst-obj__main-head').innerText = investmentmobobjhead;
})