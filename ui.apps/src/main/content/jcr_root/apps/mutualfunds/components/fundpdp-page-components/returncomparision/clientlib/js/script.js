$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.mf-fund-title').on('click', function () {
            $(this).toggleClass('arrow-bottom');
            $(this).next().slideToggle();
        })
    }
});