$(document).ready(function () {
    $('.whats-trending-card-slider').slick({
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 3.4,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        },
            {
            breakpoint: 1024,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        }, ]
    });
  });