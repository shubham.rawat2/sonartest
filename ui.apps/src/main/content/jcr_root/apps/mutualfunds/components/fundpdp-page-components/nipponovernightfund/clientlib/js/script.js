$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.nipponOvernightFundWrap__head').on('click', function () {
            $(this).toggleClass('nippon-fund-para-arrow-btm');
            $(this).next().slideToggle();
        })
    }

    var nipponFund = document.getElementsByClassName("head_capitalized");
    for (var j = 0; j < nipponFund.length; j++) {
        nipponFundCapt = nipponFund[j].innerHTML.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        document.getElementsByClassName("head_capitalized")[j].innerText = nipponFundCapt;
    }
});