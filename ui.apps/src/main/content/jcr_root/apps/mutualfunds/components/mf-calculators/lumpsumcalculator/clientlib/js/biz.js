function getSIPInputs() {
    return {
        "calc": "sip",
        "amount": Number($("#mf-sip-saving-amt-inp").val().split(",").join("")),
        "tenure": Number($("#mf-sip-for-year-inp").val()),
        "returnRate": Number($("#mf-sip-roi-inp").val())
    }
}

function getLumpsumInputs() {
    return {
        "calc": "lumpsum",
        "amount": Number($("#mf-lmsm-saving-amt-inp").val().split(",").join("")),
        "tenure": Number($("#mf-lmsm-for-year-inp").val()),
        "returnRate": Number($("#mf-lmsm-roi-inp").val())
    }
}

function getSIPChartData(investment, tenure, roi) {
    var date = new Date();
    var year = date.getFullYear() + 1;
    var sipData = {};
    var monthlyObj = {
        "investment": 0,
        "interest": 0,
        "totalInvestment": 0,
        "year": year
    };
    var yearlyData = [];
    for (var index = 1; index <= (tenure * 2) * 12; index++) {
        var monthlyInvestment = monthlyObj["investment"] + investment;
        var totalInterest = (monthlyObj["totalInvestment"] + investment) * roi;
        var totalInvestment = (monthlyObj["totalInvestment"] + investment) + totalInterest
        monthlyObj = {
            "investment": monthlyInvestment,
            "interest": totalInterest,
            "totalInvestment": totalInvestment,
            "year": year
        };
        yearlyData.push(monthlyObj);
        if (index % 12 == 0) {
            sipData[year] = yearlyData;
            yearlyData = [];
            year++;
        }
    }
    return sipData;
};



function getLumpsumChartData(investment, tenure, roi) {
    var date = new Date();
    var year = date.getFullYear() + 1;
    var lumpsumData = {};
    var monthlyObj = {
        "investment": 0,
        "interest": 0,
        "totalInvestment": 0,
        "year": year
    };
    var yearlyData = [];
    for (var index = 1; index <= (tenure * 2) * 12; index++) {
        var monthlyInvestment = investment;
        var totalInterest = (monthlyObj["totalInvestment"] + monthlyInvestment) * roi;
        var totalInvestment = (monthlyObj["totalInvestment"] + monthlyInvestment) + totalInterest
        monthlyObj = {
            "investment": monthlyInvestment,
            "interest": totalInterest,
            "totalInvestment": totalInvestment,
            "year": year
        };
        yearlyData.push(monthlyObj);
        if (index % 12 == 0) {
            lumpsumData[year] = yearlyData;
            yearlyData = [];
            year++;
        }
    }
    return lumpsumData;
};

function calculatesip() {
    var data = {};
    if ($(".mf-calc-active-tab").data().tab == "sip-calc") {
        data = getSIPInputs();
    } else {
        data = getLumpsumInputs();
    }
    if (data.calc == "sip") {
        var monthlyroi = (data.returnRate / 12) / 100;
        data.firstresult = (data.amount) * ((Math.pow(1 + monthlyroi, (data.tenure * 12)) - 1) / monthlyroi) * (1 + monthlyroi);
        data.firsttotalInvestment = (data.amount * 12) * data.tenure;
        data.firsttotalGain = data.firstresult - data.firsttotalInvestment;
        data.secondresult = (data.amount) * ((Math.pow(1 + monthlyroi, ((data.tenure * 2) * 12)) - 1) / monthlyroi) * (1 + monthlyroi);
        data.secondtotalInvestment = (data.amount * 12) * (data.tenure * 2);
        data.secondtotalGain = data.secondresult - data.secondtotalInvestment;

        var sipData = getSIPChartData(data.amount, data.tenure, monthlyroi);
        var flatSipData = Object.values(sipData).flat();
        var graphData = flatSipData.filter(function (obj, index) {
            return (((index + 1) % (flatSipData.length / 8)) == 0) ? 1 : 0;
        })
        mfLineChart.data.datasets[1]["data"] = [0].concat(graphData.map(function (obj) {
            return Math.round(obj["totalInvestment"]);
        }));
        mfLineChart.data.datasets[0]["data"] = [0].concat(graphData.map(function (obj) {
            return Math.round(obj["investment"]);
        }));
        mfLineChart.data.labels = [new Date().getFullYear()].concat(graphData.map(function (obj) {
            return Math.round(obj["year"]);
        }));
        mfLineChart.update();
    } else {
        var monthlyroi = (data.returnRate / 12) / 100;
        data.firstresult = data.amount * Math.pow(1 + (data.returnRate / 100), data.tenure);
        data.firsttotalInvestment = data.amount;
        data.firsttotalGain = data.firstresult - data.firsttotalInvestment;

        data.secondresult = data.amount * Math.pow(1 + (data.returnRate / 100), (data.tenure * 2));
        data.secondtotalInvestment = data.amount;
        data.secondtotalGain = data.secondresult - data.secondtotalInvestment;

        var lumpsum = getLumpsumChartData(data.amount, data.tenure, monthlyroi);
        var flatSipData = Object.values(lumpsum).flat();
        var graphData = flatSipData.filter(function (obj, index) {
            return (((index + 1) % (flatSipData.length / 8)) == 0) ? 1 : 0;
        })
        mfLineChart.data.datasets[1]["data"] = [0].concat(graphData.map(function (obj) {
            return Math.round(obj["totalInvestment"]);
        }));
        mfLineChart.data.datasets[0]["data"] = [0].concat(graphData.map(function (obj) {
            return Math.round(obj["investment"]);
        }));
        mfLineChart.data.labels = [new Date().getFullYear()].concat(graphData.map(function (obj) {
            return Math.round(obj["year"]);
        }));
        mfLineChart.update();

    }
    renderCalculatorData(data);
}

function getMonthlyData(totalInvestment, roi, tenure) {
    var monthlyInvestment = totalInvestment;
    var date = new Date();
    var year = date.getFullYear();
    var result = [{
        "investment": 0,
        "amount": 0,
        "interest": 0,
        "year": year
    }];
    var rate = (roi / (12 * 100));
    var intRate = rate + 1;
    intRate = Math.pow(intRate, tenure * 12);
    var month = date.getMonth() + 1;
    for (var i = 0; i < tenure * 12; i++) {
        var obj = {};
        obj["investment"] = result[i].amount + monthlyInvestment;
        obj["interest"] = obj["investment"] * (roi / 12 / 100);
        obj["amount"] = obj["investment"] + obj["interest"];
        obj["year"] = year;
        month++;
        if (month == 13) {
            month = 1;
            year++;
        }
        result.push(obj);
    }
    return result;
}

$(document).ready(function () {
    $('.calc__name').on("click", function () {
        var calc_tab_id = $(this).attr('data-tab');
        $(this).removeClass('mf-calc-active-tab');
        $('.mfLumsumCalcBox').removeClass('active-calculator-tab');
        $(this).addClass('mf-calc-active-tab').siblings().removeClass('mf-calc-active-tab');
        $("#" + calc_tab_id).addClass('active-calculator-tab');
        calculatesip();
        if ($(".mf-calc-active-tab").data().tab == "sip-calc") {
            initCommTenureSlider(Number(siplumsumObj["sipyeartenslidmintxt"].replace(/,/g, '')), Number(siplumsumObj["sipyeartenslidmaxtxt"].replace(/,/g, '')));
        } else {
            initCommTenureSlider(Number(siplumsumObj["lumyeartenslidmintxt"].replace(/,/g, '')), Number(siplumsumObj["lumyeartenslidmaxtxt"].replace(/,/g, '')));
        }
    })
});