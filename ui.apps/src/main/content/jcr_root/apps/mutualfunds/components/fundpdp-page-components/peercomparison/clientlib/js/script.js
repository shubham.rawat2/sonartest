$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.mf-peer-title').on('click', function () {
            $(this).toggleClass('arrow-bottom');
            $(this).next().slideToggle();
        })
    }

    // var peer_year_count = 1;
    // $('.peer_return_text').text("1");

    // $('.mf-peer-up-arrow').on("click", function(){
    //     if(peer_year_count <= 5){
    //         peer_year_count+2;
    //         $('.peer_return_text').text(peer_year_count);
    //     }
    // })
    // $('.mf-peer-up-down').on("click", function(){
    //     if(peer_year_count > 1){
    //         peer_year_count-2;
    //         $('.peer_return_text').text(peer_year_count);
    //     }
    // })

    // $('.mf-peer-up-arrow , .mf-peer-up-down').click(function () {
    //     var anualizedCount = document.querySelector('.peer_return_text').innerHTML
    //     if ($(this).hasClass('mf-peer-up-arrow') && Number(anualizedCount) < 5) {
    //         document.querySelector('.peer_return_text').innerHTML = Number(document.querySelector('.peer_return_text').innerHTML) + 2;
    //         // if(anualizedCount == )
    //     } else if ($(this).hasClass('mf-peer-up-down') && Number(anualizedCount) > 1) {
    //         document.querySelector('.peer_return_text').innerHTML = Number(document.querySelector('.peer_return_text').innerHTML) - 2;
    //     }
    // })
    // $('#peer-return-count').text(1);
    // var tab = document.querySelectorAll('.tab');
    // for (i = 0; i < tab.length; i++) {
    //     if (tab[i].dataset.tab != 1) {
    //         tab[i].style.display = 'none';
    //     }
    // }
    var count = 1;
    var tabList = document.querySelectorAll('.tab');
    for (i = 0; i < tabList.length; i++) {
        if (tabList[i].dataset.tab == 1) {
            tabList[i].style.display = 'block';
        } else {
            tabList[i].style.display = 'none';
        }
    }
    $('.mf-peer-up-arrow, .mf-peer-up-down').click(function () {
        if ($(this).hasClass('mf-peer-up-arrow')) {
            if (count < 3) {
                count = 3;
            }
            else if (count < 5) {
                count = 5;
            }
            else if (count < 10) {
                count = 10;
            }
            $('#peer_return_text').text(count);
        } else {
            if (count > 5) {
                count = 5;
            }
            else if (count > 3) {
                count = 3;
            }
            else if (count > 1) {
                count = 1;
            }
            $('#peer_return_text').text(count);
        }
        for (i = 0; i < tabList.length; i++) {
            if (tabList[i].dataset.tab != count) {
                tabList[i].style.display = 'none';
            }
            else if (tabList[i].dataset.tab == count) {
                tabList[i].style.display = 'block';
            }
        }
    });

    var peerTitle = document.querySelectorAll('.mf-peer-fund-title');
    peerTitle.forEach(function (element) {
        var fundTitle = element.innerHTML;
        fundTitle = fundTitle.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        element.innerHTML=fundTitle;
    })
});