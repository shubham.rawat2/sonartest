
$(document).ready(function () {

    if (window.matchMedia('(max-width: 767px)').matches) {
        if ($('.view-fund-slider .viewFundCards').length > 1) {
            $('.view-fund-slider').slick({
                infinite: false,
                arrows: false,
                slidesToShow: 1.2,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            });
        }
    } else if (window.matchMedia('(min-width: 768px)').matches) {
        if ($('.view-fund-slider .viewFundCards').length >= 4) {
            $('.view-fund-slider').slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                float: screenLeft,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        slidesToShow: 1.2,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false,
                    }
                },]
            });
        }
    }

    if ($('.view-fund-slider .viewFundCards').length == 0) {
        $('.recentlyviewdfunds').hide();
    }
});

