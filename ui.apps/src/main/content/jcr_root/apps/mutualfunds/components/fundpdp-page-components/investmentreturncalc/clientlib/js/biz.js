function getSIPInputs() {
    return {
        "calc": "sip",
        "amount": Number($("#mf-investment-sip-saving-amt-inp").val().split(",").join("")),
        "tenure": Number($("#mf-sip-tenure-inp").val()),
        // "roi" : callExploreApi(),
    }
}

function getLumpsumInputs() {
    return {
        "calc": "lumpsum",
        "amount": Number($("#mf-investment-lumpsum-amt-inp").val().split(",").join("")),
        "tenure": Number($("#mf-invst-lumpsum-tenure-inp").val())
    }
}
var apiData, data = {};
function pdpcalculation() {
    if (apiData == undefined) {
        apiData = JSON.parse(localStorage.getItem("ExpData"));
    }
    if ($(".active--investmentcalctab").data().tab == "investment-monthly-sip") {
        data = getSIPInputs();
    } else {
        data = getLumpsumInputs();
    }
    if (data.calc == 'sip') {
        var monthlyroi = (14 / 12) / 100;
        data.result = (data.amount) * ((Math.pow(1 + monthlyroi, (data.tenure * 12)) - 1) / monthlyroi) * (1 + monthlyroi);
        data.totalInvestment = (data.amount * 12) * data.tenure;
        data.totalProfit = data.result - data.totalInvestment;
        renderCalcResult(data)
    } else {
        var monthlyroi = (7 / 12) / 100;
        data.result = data.amount * Math.pow(1 + (7 / 100), data.tenure);
        data.totalInvestment = data.amount;
        data.totalProfit = data.result - data.totalInvestment;
        renderCalcResult(data)
    }
}

$('.annualise-arrow-up , .annualise-arrow-down').click(function () {
    var anualizedCount = document.querySelector('#annualise-value').innerHTML
    if ($(this).hasClass('annualise-arrow-up') && Number(anualizedCount) < 5) {
        document.querySelector('#annualise-value').innerHTML = Number(document.querySelector('#annualise-value').innerHTML) + 2;
    } else if ($(this).hasClass('annualise-arrow-down') && Number(anualizedCount) > 1) {
        document.querySelector('#annualise-value').innerHTML = Number(document.querySelector('#annualise-value').innerHTML) - 2;
    }
})
