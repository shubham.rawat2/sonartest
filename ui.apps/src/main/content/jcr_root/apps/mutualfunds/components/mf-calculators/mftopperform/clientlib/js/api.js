function callTopPerformingFundApi(){
    var fundType = document.querySelector('.active-fund-tab').getAttribute('fund-type');
    dataMapping.topPerformingFunds.getTopPerformingFundsByType(fundType).then(function (data) {
        topPerformingPopulation(data)
    })
}