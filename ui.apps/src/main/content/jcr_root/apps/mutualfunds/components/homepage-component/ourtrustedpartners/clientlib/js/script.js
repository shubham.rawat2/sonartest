
$(document).ready(function () {
    $('.OurPartnerSlider').slick({
        autoplay: true,
        speed: 2000,
        infinite: true,
        slidesToShow: 5.7,
        slidesToScroll: 1,
        arrows:false,
        autoplaySpeed: 0,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 3.4,
                dots: false,
            }
        }, ]
    });
  });