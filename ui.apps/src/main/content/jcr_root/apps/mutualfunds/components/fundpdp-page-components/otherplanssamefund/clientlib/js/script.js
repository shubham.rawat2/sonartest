$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.mf-fund-other-plan__title').on('click', function () {
            $(this).toggleClass('arrow-bottom');
            $(this).next().slideToggle();
        })
    }
});