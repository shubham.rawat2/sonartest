dataFilterObj = {};
dataFilterObj.arrSeggSubCat = [];
dataFilterObj.queryValues = [];
dataFilterObj.sortArrData = [];

function removeSubCatParam(catType){
    Object.values(dataMapping.fundsfilterData.SubCategory).forEach(function (ele) {
        if (ele.type == catType) {
            if(dataFilterObj.queryValues.indexOf(ele.value) > -1){
                dataFilterObj.queryValues.splice(dataFilterObj.queryValues.indexOf(ele.value),1)
            }
        }
    })
    
}

function queryParamSetFn(inputDataVal){
    if(dataFilterObj.queryValues.indexOf(inputDataVal) > -1){
        dataFilterObj.queryValues.splice(dataFilterObj.queryValues.indexOf(inputDataVal),1)
    }else{
        dataFilterObj.queryValues.push(inputDataVal)
    }
}
function subCatBizFn(data) {
    var renderSubCatArr = [];
    data.forEach(function (ele) {
        Object.values(dataMapping.fundsfilterData.SubCategory).forEach(function (ele1) {
            if (ele1.type == ele) {
                renderSubCatArr.push(ele1);
            }
        })
    })
    renderSubCatList(renderSubCatArr,subCatFil)
}


function subTypeChangeEvent(){
    $('[data-sortSubCatFilter]').click(function () {
        var thisData = $(this);
        queryParamSetFn(thisData.data('sortsubcatfilter'));
        Object.values(dataMapping.fundsfilterData.SubCategory).forEach(function (ele) {
            if (thisData.attr('id') == ele.name) {
                if (ele.isChecked == false) {
                    ele.isChecked = true;
                    console.log('checked')
                } else {
                    ele.isChecked = false;
                    console.log('un-checked')
                }
            }
        })
        filterApiCallFn(dataFilterObj.queryValues.join('&'))
    })
}

function removeCheckedFlag(catType){
    Object.values(dataMapping.fundsfilterData.SubCategory).forEach(function (ele) {
        if (ele.type == catType) {
            ele.isChecked = false
        }
    })
}

function onloadCallFn(){
    renderFilterListFn(dataMapping.fundsfilterData.Category,fundtype)
    renderFilterListFn(dataMapping.fundsfilterData.RiskFactor,risktype)
    renderRatingList(dataMapping.fundsfilterData.Ratings,ratingSort);
    fundReturnTabFn(dataMapping.fundsfilterData.Returns, returntabs)
    $('#fundtype li:first').addClass('fund-category-active-tab');
    $('#returntabs li:first').addClass('active-filter');
    // dataFilterObj.sortArrData.push($('#returntabs .active-filter').data('fundreturnval'))
    dataFilterObj.queryValues.push($('#fundtype .fund-category-active-tab').data('sortfilter'))
    dataFilterObj.arrSeggSubCat.push($('#fundtype .fund-category-active-tab').data('sortfilterval'));
    subCatBizFn(dataFilterObj.arrSeggSubCat)
    filterApiCallFn('type=equity');
}