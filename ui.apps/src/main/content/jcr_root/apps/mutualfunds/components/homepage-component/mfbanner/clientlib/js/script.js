
$(document).ready(function () {
    var setTimerslickfirst = Number(slideTimer.logotimerfirst) > 500 ? Number(slideTimer.logotimerfirst) : 3000;
    var setTimerslicksecond = Number(slideTimer.logotimersecond) > 500 ? Number(slideTimer.logotimersecond) : 3000;
    var setTimerslickthird = Number(slideTimer.logotimerthird) > 500 ? Number(slideTimer.logotimerthird) : 3000;
    $('.banner-slider-1').slick({
        autoplay: true,
        autoplaySpeed: setTimerslickfirst,
        speed: 2000,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        variableWidth: true,

    });
    $('.banner-slider-2').slick({
        autoplay: true,
        autoplaySpeed: setTimerslicksecond,
        speed: 2000,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        variableWidth: true,

    });

    $('.banner-slider-2').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        $(slick.$slides).removeClass('slick-highlight');
        $(slick.$slides.get(nextSlide)).addClass('slick-highlight')
        $(slick.$slides.get(currentSlide)).addClass('slick-highlight')
    });
    $('.banner-slider-3').slick({
        autoplay: true,
        autoplaySpeed: setTimerslickthird,
        speed: 2000,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        variableWidth: true,
    });
});