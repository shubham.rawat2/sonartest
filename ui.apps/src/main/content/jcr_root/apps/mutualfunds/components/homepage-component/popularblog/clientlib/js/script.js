
$(document).ready(function () {
    $('.popular-blog-card-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        }, {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1.2,
                slidesToScroll: 1,
                infinite: false,
                dots: false,
            }
        },]
    });
  });