document.addEventListener('DOMContentLoaded', function (event) {
    onloadCallFn();
    $('[data-sortFilter]').click(function(){
        queryParamSetFn($(this).data('sortfilter'));
        if(Object.keys(dataMapping.fundsfilterData.Category).indexOf($(this).data('sortfilterval')) > -1){
            if(dataFilterObj.arrSeggSubCat.indexOf($(this).data('sortfilterval')) > -1){
                dataFilterObj.arrSeggSubCat.splice(dataFilterObj.arrSeggSubCat.indexOf($(this).data('sortfilterval')),1);
                removeSubCatParam($(this).data('sortfilterval'));
                removeCheckedFlag($(this).data('sortfilterval'));
                subCatBizFn(dataFilterObj.arrSeggSubCat);
             }else{
                 dataFilterObj.arrSeggSubCat.push($(this).data('sortfilterval'));
                 subCatBizFn(dataFilterObj.arrSeggSubCat);
             }
        }
        if(dataFilterObj.queryValues.length > 0){
            filterApiCallFn(dataFilterObj.queryValues.join('&'))
        }else{
            filterApiCallFn('')
        }
        
        
    })
    $('.mf-fund-category-tabs ul li').click(function () {
        if($(this).hasClass('fund-category-active-tab')){
            $(this).removeClass('fund-category-active-tab')
        }else{
            $(this).addClass('fund-category-active-tab')
        }
    });
    $('.mf-fund-types-tabs ul li').click(function () {
        $(this).addClass('fund-type-active-tab').siblings().removeClass('fund-type-active-tab');
    });
    $('.mf-fund-filter-risk-tabs ul li').click(function () {
        $(this).addClass('fund-risk-active-tab').siblings().removeClass('fund-risk-active-tab');
    });
    $('.mf-fund-filter-banchmark-tabs ul li').click(function () {
        $(this).addClass('fund-banchmark-active-tab').siblings().removeClass('fund-banchmark-active-tab');
    });

    $('.mf-fund-filter__clear').click(function () {
        $('.mf-fund-category-tabs ul li').removeClass('fund-category-active-tab');
        $('.mf-fund-types-tabs ul li').removeClass('fund-type-active-tab');
        $('.mf-fund-filter-risk-tabs ul li').removeClass('fund-risk-active-tab');
        $('.mf-fund-filter-banchmark-tabs ul li').removeClass('fund-banchmark-active-tab');
        $('#fund-filter-main [data-sortfilter]').each(function(ind,ele){
            if(dataFilterObj.queryValues.indexOf($(ele).data('sortfilter')) > -1){
                    dataFilterObj.queryValues.splice(dataFilterObj.queryValues.indexOf($(ele).data('sortfilter')),1)
            }
        })
        $('#fund-filter-main [data-sortsubcatfilter]').each(function(ind,ele){
            if(dataFilterObj.queryValues.indexOf($(ele).data('sortsubcatfilter')) > -1){
                    dataFilterObj.queryValues.splice(dataFilterObj.queryValues.indexOf($(ele).data('sortsubcatfilter')),1)
            }
        })
        Object.values(dataMapping.fundsfilterData.SubCategory).forEach(function (ele) {
                ele.isChecked = false;
        })
        dataFilterObj.arrSeggSubCat = [];
        $('[data-roundcheckbox] input').prop('checked',false);
        subCatBizFn(dataFilterObj.arrSeggSubCat)
        filterApiCallFn('');
    });

    $('#mf_fund_filter_model').click(function () {
        $('.mf-fund-filter').css('display','block');
        $('body').addClass('no-scroll');
        $('.mf-fund-filter-screen-overlay').addClass('mf-filter-model-open');
    });

    $('#mf_fund_filter_close').click(function () {
        if(dataFilterObj.queryValues.length > 0){$('.active-dot').show()}else{$('.active-dot').hide()}
        $('.mf-fund-filter').css('display','none');
        $('body').removeClass('no-scroll');
        $('.mf-fund-filter-screen-overlay').removeClass('mf-filter-model-open');
    });
    $('.mf-fund-filter__advanced--title').click(function(){
        $('.mf-fund-filter__advanced--list').slideToggle();
        $('.mf-fund-filter__advanced--title h3').toggleClass('mf-fund-advanced-active');
    });
});

