$(function () {
    compareFundModelHideShow();
    getFundListData()
});

$('.compare-fund-checkbox input[type="checkbox"]').click(function () {
    if ($(this).is(':checked')) {
        debugger
        $('.mf-fund-compare-popup').show();
        currentFund();
    } else {
        $('.mf-fund-compare-popup').hide();
        renderPopUp();
    }
});

$('.mf-compare-cancel-des-icon').click(function(){
    $('.compare-fund-checkbox .compare-checkbox').prop( "checked", false );
    $('.mf-fund-card__invest--checkbox input[type="checkbox"]').prop('checked',false)
    $('.mf-fund-compare-popup').hide();
})

$('.mob-small-close-icon').click(function(){
    $('.mf-fund-compare-popup').hide();
    $('body').removeClass('no-scroll');
    $('.compare-fund-checkbox input[type="checkbox"]').prop('checked',false); 
    $('.mf-fund-card__invest--checkbox input[type="checkbox"]').prop('checked',false);
})

function compareFundModelHideShow() {
    try {

        $('.mf-fund-compare__dragcard').click(function () {
            $('.mf-comp-mode-hide').show();
            $('body').addClass('no-scroll');
        });

        $('.mf-comp-model-close').click(function () {
            $('.mf-comp-mode-hide').hide();
            $('body').removeClass('no-scroll');
        });

    } catch (e) {
        console.log(e)
    }
}

$('.md-fund-mob-model-arrow').click(function () {
    $('.md-fund-mob-model-arrow').toggleClass('down');
    if ($(this).hasClass('down')) {
        $('.mf-compare-mob-section').show();
        $('body').addClass('no-scroll');
    } else {
        $('.mf-compare-mob-section').hide();
        $('body').removeClass('no-scroll');
    }
});





