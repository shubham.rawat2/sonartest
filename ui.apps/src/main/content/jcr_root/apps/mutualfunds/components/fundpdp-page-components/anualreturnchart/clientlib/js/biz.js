
function checkValuePositiveOrNot() {
    var check_return_year = Number(document.getElementById('mf-chart-legend__count').innerText.replace('%', ''));
    if (Math.sign(check_return_year) == 1) {
      $('.arrow-positive').css('display', 'block');
      $('.arrow-nagative').css('display', 'none');
    } else {
      console.log(check_return_year)
      $('.arrow-nagative').css('display', 'block');
      $('.arrow-positive').css('display', 'none');
    }
  }