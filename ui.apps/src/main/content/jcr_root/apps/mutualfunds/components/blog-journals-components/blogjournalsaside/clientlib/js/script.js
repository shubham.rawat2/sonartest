

$(document).ready(function () {
    $('.categories-blog__tab-link').click(function () {
        $('.categories-blog__tab-item a').removeClass('active-tab');
        $(this).addClass('active-tab');
    })
    var src_iframe;
    $('#popup_close').on('click',function(){
        $('.categories-blog__popup').hide();
        $('body').removeClass('no-scroll');
        $('.categories-blog__iframe iframe').remove()
    })

    $('.popup_play_icon').on('click',function(){
        videoid=$(this).attr('ytsrc-data')
        src_iframe= `<iframe width="100%" height="100%" src="https://www.youtube.com/embed/${videoid}" 
        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; 
        encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
        $('.categories-blog__iframe').append(src_iframe)
        $('.categories-blog__popup').show();
        $('body').addClass('no-scroll');
    })

});