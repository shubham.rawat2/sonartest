function renderrecentfunds(collectedFunds, thisfund) {
    collectedFunds.reverse()
    collectedFunds.forEach(element => {
        if (element.amcfund !== thisfund.amcfund) {
            document.querySelector('.viewdFunds').innerHTML += `<div class="viewFundCards">
        <div class="viewFundsCardsbg">
            <div class="viewFundCard__topContent">
                <div class="viewfundinfo">
                    <div class="viewFundCardimgpr">
                        <img src="${element.amclogo}"
                            alt="image">
                    </div>
                    <span class="viewFundCardHead">${element.fundname}</span>
                </div>
                <div class="viewFundCardarrowimgPr">
                    <a href="${element.pagePath}.html"><img class="viewfundCdArrowImg" src="/content/dam/mutualfunds/mf-fund-pdp/arrow.png" alt="arrow"></a>
                </div>
            </div>
            <div class="viewFundCard__bottomContent">
                <div class="fundCardData_col">
                    <p class="fundCardlbl">Min. Invest.</p>
                    <span class="fundCardValue">₹ ${element.mininvest}</span>
                </div>
                <div class="fundCardData_col">
                    <p class="fundCardlbl">5 yrs Returns</p>
                    <span class="fundCardValue">${element.fiveyrsretrun} %</span>
                </div>
                <div class="fundCardData_col">
                    <p class="fundCardlbl">Value Research Rating</p>
                    <div class="cmpc-cd-ratings">
                        <div class="bs-rating rating-${element.ratings} clear-fix ">
                            <span class="star">
                                <i class="icon icon-rating-star"></i></span>
                            <span class="star"><i class="icon icon-rating-star"></i></span>
                            <span class="star"><i class="icon icon-rating-star"></i></span>
                            <span class="star"><i class="icon icon-rating-star"></i></span>
                            <span class="star star-gray">
                                <i class="icon icon-rating-star"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;
        }
    });
}