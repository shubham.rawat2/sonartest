var fundRiskMapper = {
    "VHG": "Very High",
    "HGH": "High",
    "MDH": "Moderate High",
    "MOD": "Moderate",
    "MLW": "Moderate Low",
    "LOW": "Low"
};
var selectedPage = 1;
function paginationFn(data) {
    var items = $(".mf-fund-return__card--wrapper .mf-fund-return__card");
    var numItems = jsHelper.isDef(dataMapping.fundNextUrl) ? 4 * Math.floor(data / 4) : data;
    var perPage = 4;
    var edge;
    if (window.matchMedia('(max-width: 767px)').matches) {
        edge = 0;
    } else {
        edge = 1;
    }
    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        edges: edge,
        prevText: "<img class='green-prev' src='/content/dam/mutualfunds/fund-comparison/Group 21992.svg'>",
        nextText: "<img class='' src='/content/dam/mutualfunds/fund-comparison/Group 21992.svg'>",
        onPageClick: function (pageNumber) {
            selectedPage = pageNumber;
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
            $('.disabled img').attr('src', '/content/dam/mutualfunds/fund-comparison/Group 22126.svg');
            var responseData = dataMapping.filterResponseData;
            if (pageNumber == (Math.floor(data / 4)) && jsHelper.isDef(dataMapping.fundNextUrl)) {
                dataMapping.getFilteredFundApiFn(dataMapping.fundNextUrl.split('?')[1]).then(function (dataRes) {
                    dataMapping.filterResponseData = (dataRes).concat(responseData);
                    if (dataFilterObj.sortArrData.length > 0) {
                        sortFilterDataBizFn(dataMapping.filterResponseData);
                    } else {
                        fundSchemeCardsRenderFn(dataMapping.filterResponseData, renderFilterCards);
                    }
                });
            }
        }
    });
    if (Math.floor(data / 4) < selectedPage) {
        selectedPage = 1;
    }

    $('#pagination-container').pagination('selectPage', selectedPage);

}
function addCompareEventReInitialize() {
    $('.mf-fund-card__invest--checkbox input[type="checkbox"]').click(function () {
        if ($(this).is(':checked')) {
            if (dataMapping.selectedFund.length < 2 && window.matchMedia('(max-width: 767px)').matches) {
                $(this).closest('.mf-fund-return__card').addClass("mf-fund-return__card--active");
                dataMapping.selectedFund.push($(this).attr('fundid'))
                $('.mf-fund-compare-popup').show();
                renderPopUp();
            } else if (dataMapping.selectedFund.length < 3 && window.matchMedia('(min-width: 767px)').matches) {
                $(this).closest('.mf-fund-return__card').addClass("mf-fund-return__card--active");
                dataMapping.selectedFund.push($(this).attr('fundid'))
                $('.mf-fund-compare-popup').show();
                renderPopUp()
            } else {
                $(this).prop('checked', false);
            }

        } else {
            $(this).closest('.mf-fund-return__card').removeClass("mf-fund-return__card--active");
            dataMapping.selectedFund.splice(dataMapping.selectedFund.indexOf($(this).attr('fundid')), 1);
            if (dataMapping.selectedFund.length <= 0) {
                $('.mf-fund-compare-popup').hide();
            } else {
                $('.mf-fund-compare-popup').show();
                renderPopUp()
            }

        }
        updateDataMapping();
    });
}
