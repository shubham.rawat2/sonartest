function renderTopPerfomingsFunds(data) {
    document.querySelector('.current-tab .active-fund').innerHTML = "";
    data.forEach(function (singleFund,ind) {
        if (ind < 3) {
            document.querySelector('.current-tab .active-fund').innerHTML +=
                '<div class="fundchartCardbrdr">' +
                '    <div class="fundcard__colWrap">' +
                '        <div class="fundcard__companydata">' +
                '            <div class="companyData__info">' +
                '                <div class="card__imgPr">' +
                '                    <img src="https://bfsd.uat.bfsgodirect.com' + singleFund.amc_logo + '" class="card__img" alt="">' +
                '                </div>' +
                '                <div class="cardInfo__tab__wrap">' +
                '                    <div class="company__name">' + singleFund.name + '' +
                '                        Growth</div>' +
                '                    <div class="fundcard__tabCat">' +
                '                        <div class="fundCard__tab">' + singleFund.type + '</div>' +
                '                        <div class="fundCard__tab">' + singleFund.fund_type + '</div>' +
                '                        <div class="fundCard__tab">' + fundRiskMapper[singleFund.AMCFund__risk_factor] + '</div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '            <div class="cardArrowWrap">' +
                '                <a href="#"><img class="card__arrow" src="/content/dam/mutualfunds/mf-calculators/arrow.png"' +
                '                        alt="arrow-img"></a>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '    <div class="investment__data">' +
                '        <div class="investment__data__col">' +
                '            <div class="card__col_lbl">Min. Invest.</div>' +
                '            <div class="card__value__txt">' + (singleFund.minimum_investment_amount == null ? +'-' : '₹' + singleFund.minimum_investment_amount) + '</div>' +
                '        </div>' +
                '        <div class="investment__data__col">' +
                '            <div class="card__col_lbl">' + yearCounter + ' yrs Returns</div>' +
                '            <div class="card__value__txt">' + (singleFund[`returns_${yearCounter}_yr`] == null ? '-' : singleFund[`returns_${yearCounter}_yr`].toFixed(2) + "%") + '</div>' +
                '        </div>' +
                '        <div class="investment__data__col">' +
                '            <div class="card__col_lbl">Value Research Rating</div>' +
                '            <div class="cmpc-cd-ratings">' +
                '                <div class="bs-rating rating-' + singleFund.vr_rating + ' clear-fix ">' +
                '                    <span class="star"><i class="icon icon-rating-star"></i></span><span class="star"><i' +
                '                            class="icon icon-rating-star"></i></span><span class="star"><i' +
                '                            class="icon icon-rating-star"></i></span><span class="star"><i' +
                '                            class="icon icon-rating-star"></i></span><span class="star star-gray"><i' +
                '                            class="icon icon-rating-star"></i></span>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '        <div class="fundcard__btnwrap">' +
                '            <a href="#" class="fundcard__btn">Invest Now</a>' +
                '        </div>' +
                '    </div>' +
                '</div>';
        }
    })
}