function initDoughnutChart() {
    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [100, 50],
                backgroundColor: [
                    '#eabf23', '#059df8'
                ],
                borderWidth: 0,
                weight: 1,
            }],
            labels: ["Invested Amount", "Wealth Gain"]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.labels[tooltipItem.index] + " : ₹" + data.datasets[0].data[tooltipItem.index].toLocaleString('EN-IN') || '';
                        return label;
                    }
                }
            },
            responsive: true,
            legend: false,
            animation: {
                animateScale: false,
                animateRotate: true,
            }
        }
    };
    var mf_calc_chart = document.getElementById('mf-calc-doughnut-chart').getContext('2d');
    window.mf_calc_dg = new Chart(mf_calc_chart, config);
}

function initCapitalLineChart() {
    var mfCalcLineChart = document.getElementById('mf-calc-line-chart-1');
    window.mfLineChart = new Chart(mfCalcLineChart, {
        type: 'line',
        data: {
            borderWidth: 4,
            borderCapStyle: 'round',
            labels: [],
            datasets: [
                {
                    data: [],
                    fill: false,
                    label: 'Total Investment',
                    borderColor: 'rgba(234, 191, 35, 1)',
                    tension: 0,
                    borderWidth: 4,
                    yAxisID: 'y-axis-1',
                    pointBorderWidth: 0,
                    pointBackgroundColor: "rgba(234, 191, 35, 1)",
                },
                {
                    data: [],
                    fill: false,
                    label: 'Capital Gain',
                    borderColor: 'rgba(5, 157, 248, 1)',
                    tension: 0.5,
                    borderWidth: 4,
                    yAxisID: 'y-axis-1',
                    pointBorderWidth: 0,
                    pointBackgroundColor: "rgba(5, 157, 248, 1)",
                },
            ]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label + " : ₹" + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('EN-IN') || '';
                        return label;
                    }
                },
                enabled: true,
                mode: 'index'
            },
            responsive: true,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: true,
                        drawOnChartArea: false,
                        color: 'rgba(245, 237, 237, 0.2)',
                        lineWidth: 2,
                        tickMarkLength: 0
                    },
                    ticks: {
                        display: false,
                        autoSkip: true
                    },
                }],
                yAxes: [
                    {
                        position: 'right',
                        id: 'y-axis-1',
                        ticks: {
                            display: false,
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: true,
                            drawOnChartArea: false,
                            color: 'rgba(245, 237, 237, 0.2)',
                            tickMarkLength: 0
                        },
                    },
                ]
            }
        }

    })
}

function initCommTenureSlider(min, max) {
    window.line_chart_range_slider = document.getElementById('input-slider');
    window.line_chart_range_input = $('#slider');
    try {
        line_chart_range_slider.noUiSlider.destroy()
    } catch (error) {

    }
    noUiSlider.create(line_chart_range_slider, {
        start: min,
        connect: [true, false],
        step: 1,
        orientation: 'horizontal',
        range: {
            'min': min,
            'max': max
        },
    });
    $(".siplumpcomm-minten").text(min + "Y");
    $(".siplumpcomm-maxten").text(max + "Y");
    line_chart_range_slider.noUiSlider.off('update');
    line_chart_range_slider.noUiSlider.on('update', function (val) {
        line_chart_range_input.val(parseInt(val[0]).toLocaleString('EN-IN'));
        calculatesip();
    });
    line_chart_range_input.off('change');
    line_chart_range_input.on('change', function () {
        var line_chart_for_years = $(this).val().replace(/,/g, '');
        line_chart_range_slider.noUiSlider.set(line_chart_for_years);
        calculatesip();
    });
}

$(document).ready(function () {
   

    initDoughnutChart();
    initCapitalLineChart();
    var sip_saving_amt_sldr = document.getElementById('sip-calc-range-saving-amt');
    var sip_saving_amt_inp = $('#mf-sip-saving-amt-inp');
    noUiSlider.create(sip_saving_amt_sldr, {
        start: Number(siplumsumObj["sipinvestslidemintxt"].replace(/,/g, '')),
        connect: [true, false],
        step: 100,
        orientation: 'horizontal',
        range: {
            'min': Number(siplumsumObj["sipinvestslidemintxt"].replace(/,/g, '')),
            'max': Number(siplumsumObj["sipinvestslidemaxtxt"].replace(/,/g, ''))
        },
    });
    sip_saving_amt_sldr.noUiSlider.on('update', function (val) {
        sip_saving_amt_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
        calculatesip();
    });

    sip_saving_amt_inp.on('change', function () {
        var sip_saving_amt = $(this).val().replace(/,/g, '');
        sip_saving_amt_sldr.noUiSlider.set(sip_saving_amt);
        calculatesip();
    });
    var sip_for_year_sldr = document.getElementById('sip-calc-range-for-year');
    var sip_for_year_inp = $('#mf-sip-for-year-inp');
    noUiSlider.create(sip_for_year_sldr, {
        start: Number(siplumsumObj["sipyeartenslidmintxt"].replace(/,/g, '')),
        connect: [true, false],
        step: 1,
        orientation: 'horizontal',
        range: {
            'min': Number(siplumsumObj["sipyeartenslidmintxt"].replace(/,/g, '')),
            'max': Number(siplumsumObj["sipyeartenslidmaxtxt"].replace(/,/g, ''))
        },
    });
    sip_for_year_sldr.noUiSlider.on('update', function (val) {
        sip_for_year_inp.val(parseInt(val[0]).toLocaleString('EN-IN'));
        calculatesip();
        try {
            line_chart_range_slider.noUiSlider.set(val);
        } catch (error) { }
    });
    sip_for_year_inp.on('change', function () {
        var for_years_sip = $(this).val().replace(/,/g, '');
        sip_for_year_sldr.noUiSlider.set(for_years_sip);
        calculatesip();
        try {
            line_chart_range_slider.noUiSlider.set(for_years_sip);
        } catch (error) { }
    });
    var sip_expected_roi_sldr = document.getElementById('sip-calc-range-roi');
    var sip_expected_return_inp = $('#mf-sip-roi-inp');
    noUiSlider.create(sip_expected_roi_sldr, {
        start: Number(siplumsumObj["siprangeroislidmintxt"].replace(/,/g, '')),
        connect: [true, false],
        step: 0.5,
        orientation: 'horizontal',
        range: {
            'min': Number(siplumsumObj["siprangeroislidmintxt"].replace(/,/g, '')),
            'max': Number(siplumsumObj["siprangeroislidmaxtxt"].replace(/,/g, ''))
        },
    });
    sip_expected_roi_sldr.noUiSlider.on('update', function (val) {
        sip_expected_return_inp.val(parseFloat(val[0]).toFixed(2));
        calculatesip();
    });
    sip_expected_return_inp.on('change', function () {
        var sip_expected_returns = $(this).val().replace(/,/g, '');
        sip_expected_roi_sldr.noUiSlider.set(sip_expected_returns);
        calculatesip();
    });
    initCommTenureSlider(Number(siplumsumObj["sipyeartenslidmintxt"].replace(/,/g, '')), Number(siplumsumObj["sipyeartenslidmaxtxt"].replace(/,/g, '')));
});