function callNavChartApi(id) {
    apiUtility
      .NavGetApi(id)
      .then(function (data) {
        renderChartData(JSON.parse(data).results);
      })
      .catch(function (err) {
        console.log(err);
      });
  }
  