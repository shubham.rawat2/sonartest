"use strict";
use(function () {
   var date = new Date();
   var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
   return date.getDate() + " "+ months[date.getMonth()] + " "+date.getFullYear();
});