$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.additionaldtl__Head').on('click', function () {
            $(this).toggleClass('additional-details-arrow-btm');
            $(this).next().slideToggle();
        })
    }
});