$(document).ready(function(){
    $('.disabled img').attr('src','/content/dam/mutualfunds/mf-amc/Group 22126.svg');
});

var items = $(".pagination-wrapper .amc-fund-list-card");
var numItems = items.length;
var perPage = 3;

items.slice(perPage).hide();

$('#pagination-container').pagination({
    items: numItems,
    itemsOnPage: perPage,
    prevText: "<img class='green-prev' alt='page-arrow' src='/content/dam/mutualfunds/mf-amc/Group 21992.svg'>",
    nextText: "<img class='' alt='pre-arrow' src='/content/dam/mutualfunds/mf-amc/Group 21992.svg'>",
    onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        items.hide().slice(showFrom, showTo).show();
        $('.disabled img').attr('src','/content/dam/mutualfunds/mf-amc/Group 22126.svg');
    }

});
