var yearCounter = Number(document.querySelector('.current-tab .return_text').innerHTML);
var fundRiskMapper = {
    "VHG": "Very High",
    "HGH": "High",
    "MDH": "Moderate High",
    "MOD": "Moderate",
    "MLW": "Moderate Low",
    "LOW": "Low"
}
$(document).ready(function () {
    $('.topfund__roundTab').on("click", function () {
        var fundTab_id = $(this).attr('data-tab');
        $(this).removeClass('active-fund-tab');
        $('.topfundDatawrap').removeClass('current-tab');

        $(this).addClass('active-fund-tab').siblings().removeClass('active-fund-tab');
        $("#" + fundTab_id).addClass('current-tab');
        yearCounter = 1;
        document.querySelector('.current-tab .return_text').innerHTML = 1;
        callTopPerformingFundApi()
    });
    $('.returns-arrow-up, .returns-arrow-down').click(function () {
        if ($(this).hasClass('returns-arrow-up')) {
            yearCounter < 5 ? yearCounter = yearCounter + 2 : yearCounter < 10 ? yearCounter = yearCounter + 5 : yearCounter = 10;
            document.querySelector('.current-tab .return_text').innerHTML = yearCounter;
        }
        if ($(this).hasClass('returns-arrow-down')) {
            yearCounter > 5 ? yearCounter = yearCounter - 5 : 1 < yearCounter ? yearCounter = yearCounter - 2 : yearCounter = 1;
            document.querySelector('.current-tab .return_text').innerHTML = yearCounter;
        }
        callTopPerformingFundApi()
    });
    callTopPerformingFundApi()
});