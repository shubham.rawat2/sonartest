$(document).ready(function () {
    $('.mf-fund-popular-com').slick({
      slidesToShow:3,
      slidesToScroll: 1,
      arrows: true,
      infinite:false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1.2,
            slidesToScroll: 1,
            arrows: false,
          }
        }
      ]
      
    });
  });