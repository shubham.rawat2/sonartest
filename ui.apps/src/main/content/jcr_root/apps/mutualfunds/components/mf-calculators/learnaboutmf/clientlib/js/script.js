$(document).ready(function () {
    $('.mf-video-slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
            breakpoint: 767,
            settings: "unslick"
        },]
    });
});
