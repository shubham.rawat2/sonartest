$(document).ready(function () {
    $('#search-inp').on("input", function () {
        if ($(this).val() == '') {
            $('.inp-search-suggection-wrapper').hide();
        } else {
            $('.inp-search-suggection-wrapper').show();
        }

    });
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.compare-title').click(function () {
            $(this).toggleClass('arrow-bottom');
            $('.mf-compare-wrapper').slideToggle();
        })
    }
    comparefundslider=function(type) {
        if (type == 'open') {
            $('.mf-compare-search-bar').show().css({ "height": "650px" });
            $('body').addClass('no-scroll');
            $('.screen-overlay').addClass('open');
        }
        else {
            $('.mf-compare-search-bar').hide();
            $('body').removeClass('no-scroll');
            $('.screen-overlay').removeClass('open');
        }
    }
    getFundListData()
});
var compareFund = document.querySelector('.compare-bar-headline').innerText;
compareFund = compareFund.toLowerCase().replace(/\b[a-z]/g, function (letter) {
    return letter.toUpperCase();
});
document.querySelector('.compare-bar-headline p').innerText = compareFund;









