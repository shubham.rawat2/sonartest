var riskometerMapper = {
    LOW: {
        text: "Low",
        color: "#14a25b",
    },
    MLW: {
        text: "Low to Moderate",
        color: "#09bc63",
    },
    MOD: {
        text: "Moderate",
        color: "#eabf23",
    },
    MDH: {
        text: "Moderately High",
        color: "#f0a225",
    },
    HGH: {
        text: "High",
        color: "#fd4c60",
    },
    VHG: {
        text: "Very High",
        color: "#a52836",
    },
}
$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.nipponAboutSectionWrap__head').on('click', function () {
            $(this).toggleClass('nippon-about-section-arrow-btm');
            $(this).next().slideToggle();
        })
    }
});