$(document).ready(function () {
    $('.mf-calc-trending-comp-slider').slick({
        infinite: false,
        slidesToShow: 3.4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 768,
            settings:"unslick"
        }, ]
    });
  });