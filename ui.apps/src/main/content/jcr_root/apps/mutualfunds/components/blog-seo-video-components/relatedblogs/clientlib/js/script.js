$(function () {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $('.lumsumBlog-slick').slick({
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1.2,
                    },
                },
            ],
        });
    }
});