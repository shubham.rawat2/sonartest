const DEFAULT_YEAR = ['2017', '2018', '2019', '2020', '2021'];

$(document).ready(function () {

    initlineChart();
    function initlineChart() {
        var config = {
            type: 'line',
            data: {
                labels: DEFAULT_YEAR,
                datasets: [{
                    data: [1, 10, 2, 9, 3],
                    backgroundColor: "transparent",
                    borderColor: "#a2688d",
                    borderWidth: 3,
                    pointHoverPadding: "10",
                    pointHoverBorderColor: "#a2688d",
                    pointHoverBackgroundColor: "#303d4b",
                    pointHoverBorderWidth: "3",
                    pointHoverRadius: "7",
                    pointStyle: "chartPoint",
                },
                {
                    data: [4, 10, 2, 9, 8],
                    backgroundColor: "transparent",
                    borderColor: "#059df8",
                    borderWidth: 3,
                    pointHoverPadding: "10",
                    pointHoverBorderColor: "#059df8",
                    pointHoverBackgroundColor: "#303d4b",
                    pointHoverBorderWidth: "3",
                    pointHoverRadius: "7",
                    pointStyle: "chartPoint",
                },

                {
                    data: [5, 12, 3, 11, 1],
                    backgroundColor: "transparent",
                    borderColor: "#eabf23",
                    borderWidth: 3,
                    pointHoverPadding: "10",
                    pointHoverBorderColor: "#eabf23",
                    pointHoverBackgroundColor: "#303d4b",
                    pointHoverBorderWidth: "3",
                    pointHoverRadius: "7",
                    pointStyle: "chartPoint",
                }]
            },

            options: {
                responsive: true,
                elements: {
                    point: {
                        radius: "0"
                    }
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                },
                tooltips: {
                    titleAlign: "center",
                    bodyAlign: "center",
                    backgroundColor: "#182132",
                    display: false,
                },
                legend: {
                    display: false,
                    position: "top",
                    align: "start",
                    labels: {
                        fontColor: "#fff",
                        fillStyle: "red"
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                        },
                        ticks: {
                            display: false
                        }
                    }]
                }
            },
        };
        var mf_annulise_chart = document.getElementById('annualised_return_chart').getContext('2d');
        window.mf_calc_dg = new Chart(mf_annulise_chart, config);
    }
    $('.mf-graph__tab ul li a').on('click', function () {
        $('.mf-graph__tab ul li a').removeClass('active-year');
        $(this).addClass('active-year');
    })
    $('#one_year').on('click', function () {
        one_year = DEFAULT_YEAR.slice(0, 1);
        mf_calc_dg.data.labels = one_year;
        mf_calc_dg.update();
    })

    $('#three_year').on('click', function () {
        three_year = DEFAULT_YEAR.slice(0, 3);
        mf_calc_dg.data.labels = three_year;
        mf_calc_dg.update();
    })
    $('#five_year').on('click', function () {
        five_year = DEFAULT_YEAR.slice(0, 5);
        mf_calc_dg.data.labels = five_year;
        mf_calc_dg.update();
    })
    $('#max_year').on('click', function () {
        max_year = DEFAULT_YEAR.slice(0, 10);
        mf_calc_dg.data.labels = max_year;
        mf_calc_dg.update();
    })
});








