$(document).ready(function () {
    if (window.matchMedia("(max-width: 768px)").matches) {
        $('.fundmngheadWrapper').on('click', function () {
            $('.fundmngheadWrapper__head').toggleClass('fundmngWrap-arrow-btm');
            $(this).next().slideToggle();
        })
    }
});

$('.fundUserDp').each(function(){
    var names = $(this).text().trim().split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
   $(this).text(initials);
})


// function renderManagerData() {
//     fundManager.data.forEach(function (element) {
//         var userDP = '';
//         var initails = element.fund_manager.split(' ');
//         initails.forEach(function (element) {
//             userDP += element.substring(0, 1);
//         })
//         document.querySelector('.fundprofilecardContainer').innerHTML += `<div class="fundProfileCardwrap mf-dark-black__bg">
//         <div class="fundUserDp" style="background-color: #2f78ac;">${userDP}</div>
//         <div class="fundCardUserData">
//             <div class="fundCarduserInfoWrap">
//                 <span class="fundCardUserName">${element.fund_manager}</span>
//                 <span class="fundCardUdserHistory">${element.since}</span>
//             </div>
            
//         </div>
//         <div class="fundCardRedirectArrowWrap">
//             <a href="#">
//                 <img src="/content/dam/mutualfunds/mf-fund-pdp/arrow.png" alt="Arrow">
//             </a>
//         </div>
        
//     </div>`
//     })
// }
// renderManagerData()
