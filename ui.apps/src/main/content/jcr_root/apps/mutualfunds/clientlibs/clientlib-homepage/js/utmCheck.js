function glUTMCheck(ele) {
    var utm_params;
    var urlParams = window.urlObj.search.includes("src_30") || window.urlObj.search.includes("src_60") || window.urlObj.search.includes("src_90") || window.urlObj.search.includes("pid") || window.urlObj.search.includes("utm_source");
    var confParams = window.urlObj.search.includes("prodCode") || window.urlObj.search.includes(" ");
    if (urlParams) {
        utm_params = window.urlObj.search;
        if ($(this).attr("target")) {
            if ($(this).attr("href").includes('?')) {
                if (utm_params.includes('&nav_src')) {
                    if (typeof $(ele).attr("href") !== typeof undefined && $(ele).attr("href") !== false) {
                        window.open($(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src')), $(ele).attr("target"));
                        return false;
                    }
                } else {
                    if (typeof $(ele).attr("href") !== typeof undefined && $(ele).attr("href") !== false) {
                        window.open($(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length), $(ele).attr("target"));
                        return false;
                    }
                }
            } else {
                if (typeof $(ele).attr("href") !== typeof undefined && $(ele).attr("href") !== false) {
                    window.open($(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src')), $(ele).attr("target"));
                    return false;
                }
            }
        } else {
            if (typeof $(ele).attr("href") !== typeof undefined && $(ele).attr("href") !== false) {
                if ($(ele).attr("href").includes('?')) {
                    if (utm_params.includes('&nav_src')) {
                        window.location.href = $(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src'));
                        return false;
                    } else {
                        window.location.href = $(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length);
                        return false;
                    }
                } else {
                    if (utm_params.includes('&nav_src')) {
                        window.location.href = $(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src'));
                        return false;
                    } else {
                        window.location.href = $(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length);
                        return false;
                    }
                }
            }
        }
    } else {
        if (typeof $(ele).attr("href") !== typeof undefined && $(ele).attr("href") !== false) {
            utm_params = utm;
            if ($(ele).attr("target")) {
                if ($(ele).attr("href").includes('?')) {
                    if (utm_params.includes('&nav_src')) {
                        window.open($(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src')), $(ele).attr("target"));
                        return false;
                    } else {
                        window.open($(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length), $(ele).attr("target"));
                        return false;
                    }
                } else {
                    window.open($(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src')), $(ele).attr("target"));
                    return false;
                }
            } else {
                if ($(ele).attr("href").includes('?')) {
                    if (utm_params.includes('&nav_src')) {
                        window.location.href = $(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src'));
                        return false;
                    } else {
                        if (utm_params.length > 0) {
                            window.location.href = $(ele).attr("href") + '&' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length);
                            return false;
                        } else {
                            window.location.href = $(ele).attr("href");
                            return false;
                        }
                    }
                } else {
                    if (utm_params.includes('&nav_src')) {
                        window.location.href = $(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.indexOf('&nav_src'));
                        return false;
                    } else {
                        if (utm_params.length > 0) {
                            window.location.href = $(ele).attr("href") + '?' + utm_params.substring(utm_params.indexOf('?') + 1, utm_params.length);
                            return false;
                        } else {
                            window.location.href = $(ele).attr("href");
                            return false;
                        }
                    }
                }
            }
        }
    }
}