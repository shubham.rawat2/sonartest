/*******************************************API Config Module - Start******************************************************/
(function (_global) {
    var _apiConfig = (function (jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "API Config");
        }
        /**
         * API Constant Values
         */
        var NAV_API = "NAV_API";
        var EXPLORE_FUND_API = "EXPLORE_FUND_API";
        var SEARCH_COMPARE_FUND_API ="SEARCH_COMPARE_FUND_API";
        /**
         * API Constants 
         */
        var apiConstants = {};
        apiConstants[NAV_API] = NAV_API;
        apiConstants[EXPLORE_FUND_API] = EXPLORE_FUND_API;
        apiConstants[SEARCH_COMPARE_FUND_API] = SEARCH_COMPARE_FUND_API;
        /**
         * API Selectors
         */
        var apiSelectors = {};
        apiSelectors[NAV_API] = "nav_api";
        apiSelectors[EXPLORE_FUND_API] = "explore_fund_api";
        apiSelectors[SEARCH_COMPARE_FUND_API] = "search_compare_fund_api";
        /**
         * API Endpoints
        */
        var apiEndpoints = {};
        apiEndpoints[NAV_API] = "/mf/app/api/amcfundschemes/";
        apiEndpoints[EXPLORE_FUND_API] = "/mf/app/api/v2/mobile/explore/";
        apiEndpoints[SEARCH_COMPARE_FUND_API] = "/api/assets/mutualfunds/funds-data/fund-list.json";

        var  apiRoot={};
        apiRoot[SEARCH_COMPARE_FUND_API]="";


        /**
         * API Config Object to expose
         */
        var apiConfig = {};
        Object.keys(apiConstants).forEach(function (eachApiConstant) {
            apiConfig[eachApiConstant] = jsHelper.freezeObj({
                "name": apiConstants[eachApiConstant],
                "selector": apiSelectors[eachApiConstant],
                "endpoint": apiEndpoints[eachApiConstant],
                "root": apiRoot[eachApiConstant]
            });
        });
        return jsHelper.freezeObj(apiConfig);
    })(_global.jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'apiConfig', _apiConfig);
})(this);