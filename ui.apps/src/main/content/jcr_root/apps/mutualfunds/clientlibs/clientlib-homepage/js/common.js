function mfLoadVideo(ele) {
    a = $(ele).hide().next().hide();
    $(ele).hide().next().hide();
    $(ele).attr('data-youtube-video', 'loaded');
}
var PlayerObj = {};
var Player;
function onYouTubeIframeAPIReady() {
    var elems1 = document.getElementsByClassName('card--video');
    for (var i = 0; i < elems1.length; i++) {
        Player = new YT.Player(elems1[i], {
            height: "100%", width: "100%",
            videoId: elems1[i].getAttribute("data-youtube-video"),
            playerVars: {
                'autoplay': 0,
                'rel': 0,
            },
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
        PlayerObj[elems1[i].getAttribute("data-youtube-video")] = Player;
    }
}
function onPlayerStateChange(event) {
    handleVideo(event.data);
}

$(function () {
    $('.videoplay--btn').on("click", function () {
        var status = $(this).attr('data-youtube-video');
        if (status == 'loaded') {
            $('.investment-playButton').hide();
            $('.investmentvideo-card-img').hide();
            PlayerObj[$(this).prev().data().videoid].playVideo();
        } else {
            if (!$('body').hasClass('ytube')) {
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                $('body').addClass('ytube')
            }
            mfLoadVideo($(this));
        }
    })
});