var renderHeader=false;
function innerDivHeight() {
    var bfsTopBandHeight = $(".bfs-mob-top-band-wrap").outerHeight();
    var bfsSideBarFooterHeight = $(".side-bar-footer-wrap").outerHeight();
    var bfsInnerNavList = $(".bfs-nav-list-main").outerHeight();
    var innerMenuTotalHeightTotal = bfsTopBandHeight + bfsSideBarFooterHeight + bfsInnerNavList;
    var innerMenuTotalHeightSingle = $(".bfs-nav-list-wrap").outerHeight();
    if (innerMenuTotalHeightSingle > innerMenuTotalHeightTotal) {
        return innerMenuTotalHeightSingle;
    } else {
        return innerMenuTotalHeightTotal;
    }
}

function secLevelNavHeight(_this) {
    var backForBtnHeight = _this.find(".side-bar-back-tab-main").outerHeight();
    var dealOptionsHeight = _this.find(".deal-options-main").outerHeight();
    var subMenuInnerHeight = _this.find(".inner-wrap").outerHeight();
    var totalSecLevelNavHeight = backForBtnHeight + dealOptionsHeight + subMenuInnerHeight;
    var defaultMenuHeight = innerDivHeight();

    if (defaultMenuHeight > totalSecLevelNavHeight) {
        return defaultMenuHeight;
    } else {
        return totalSecLevelNavHeight;

    }
}

function checkArrow() {
    $(".inner-submenus-acc-head").each(function () {
        if ($(this).find(".inner-submenus-acc-ul").length == 0) {
            $(this).find(".inner-submenus-acc-cl").addClass("arrow-none");
        }
    });
}

function getProfileInitials() {
    var profileText = $(".prof-name-in").text().split(' ');
    var profileInitials = "";

    for (var i = 0; i < profileText.length; i++) {
        profileInitials += profileText[i].substring(0, 1);
    }

    $(".profile-initial").text(profileInitials);
}

var currentPageLink = window.location.pathname;



function hamburgerRedirectionParent() {
    var currentPageLink = window.location.pathname.split("/");
    if (currentPageLink[1].includes(".html")) {
        currentPageLink[1].replace(".html", "");
    }
    var pathnameText = currentPageLink[1].toLowerCase().replace("-", "");

    $(".bfs-nav-list-parent").each(function () {
        var hamburgerLinkText = $(".bfs-nav-list-parent-link span", this).text().toLowerCase().replace(/\s+/g, '');
        console.log(hamburgerLinkText);
        if (hamburgerLinkText == pathnameText) {
            $(this).find(".inner-menu-submenu-wrap").addClass("side-bar-back-open-tab");
        }

    });
}

function hamburgerRedirectionChild() {
    var currentPageLink = window.location.pathname.split("/");
    var pathnameText;

    if (currentPageLink[2] != undefined) {
        if (currentPageLink[2].includes(".html")) {
            currentPageLink[2].replace(".html", "");
        }
        pathnameText = currentPageLink[2].toLowerCase().replace("-", "");
    }
    $(".inner-child-list").each(function () {
        var hamburgerLinkText = $(this).text().toLowerCase().replace(/\s+/g, '');
        if (hamburgerLinkText == pathnameText) {
            $(this).parent().find(".inner-submenu").addClass("opem-inner-submenu");
            $(this).parents(".inner-menu-submenu-wrap").addClass("side-bar-back-open-tab");
        }
    })
}

var j = 0;
var animateText = "What can we help you find?";
var appendText = "";
var textSpeed = 85;

function typeAnimate() {

    if (j < animateText.length) {
        appendText += animateText.charAt(j);
        $(".animatepholder").text(appendText);
        j++;
        setTimeout(typeAnimate, textSpeed);
    }

}

function fadeOutSearchText() {
    $(".animatepholder").fadeOut();
    $(".bfs-menu-search-main .form-control").attr("placeholder", "What can we help you find?");
}


function langSplit() {
    var langText = $(".details-lang").text();
    var dispLang = langText.substr(0, 3);
    $(".details-lang").text(dispLang);
}

var pageCode;

function changeLangCode(pageCode, langCode) {
    if (location.pathname.includes("/content/bajajfinserv")) {
        if (pageCode != langCode && !location.pathname.includes("/" + langCode + "/")) {
            location.pathname = location.pathname.replace("/" + pageCode, "/" + langCode);
        }
    } else {
        if (pageCode != langCode && langCode == "en") {
            location.pathname = location.pathname.replace("/" + pageCode, "");
        } else if (pageCode != langCode && !location.pathname.includes("/" + langCode + "/") && location.pathname.includes("/" + pageCode + "/")) {
            location.pathname = location.pathname.replace("/" + pageCode, "/" + langCode);
        } else {
            location.pathname = "/" + langCode + location.pathname;
        }
    }
}

function onLoadLangSelection() {
    var pathName = location.pathname;
    if (pathName.includes("/content/bajajfinserv")) {
        pathName = pathName.replace("/content/bajajfinserv", "");
    }
    var splitPath = pathName.split('/');
    splitPath.shift();
    var langCd = splitPath[0];
    var foundLangCode = true;
    $(".language-sel-options").each(function () {
        var redirectLangCode = $(this).data("lang");
        if (langCd == redirectLangCode) {
            $(this).addClass("lang-sel-active").siblings().removeClass("lang-sel-active");
            var selectLang = $(".lang-opt", this).text().trim();
            var dispLang = selectLang.substr(0, 2);
            $(".details-lang").text(dispLang);
            pageCode = redirectLangCode;
            foundLangCode = false;
        }
    });
    if (foundLangCode) {
        $('.language-sel-options[data-lang="en"]').addClass('lang-sel-active');
        pageCode = "en";
    }
}

function mobLngSelectionLoad() {
    var mobpathName = location.pathname;
    var mobsplitPath = mobpathName.split('/');
    mobsplitPath.shift();
    var moblangCd = mobsplitPath[0];
    var foundLangCode = true;
    $(".lang-selection-mob-li").each(function () {
        var mobRedirectLangCode = $(this).data("lang");
        if (moblangCd == mobRedirectLangCode) {
            $(this).addClass("mob-lang-active").siblings().removeClass("mob-lang-active");
            pageCode = mobRedirectLangCode;
            foundLangCode = false;
        }
    });
    if (foundLangCode) {
        $('.lang-selection-mob-li[data-lang="en"]').addClass('mob-lang-active');
        pageCode = "en";
    }
}

function mobLangSelectionC() {
    $(".lang-selection-mob-li").each(function () {
        if ($(this).hasClass("mob-lang-active")) {
            var activeLang = $(this).find(".lang-opt").text();
            $(".footer-lang").find(".footer-lang-sel").text(activeLang);
        }
    })
}

$(document).ready(function () {

    onLoadLangSelection();

    $(".details-lang-hd").on("click", function () {
        $('.language-select-dropd').toggleClass("lang-select-cli");
        $(".details-lang").toggleClass("arrow-org");
    });

    $('.language-select-dropd li.language-sel-options').on("click", function (e) {
        e.stopPropagation();
        var selectLang = $(".lang-opt", this).text().trim();
        console.log(selectLang);
        $(this).addClass("lang-sel-active").siblings().removeClass("lang-sel-active");
        var langID = $(this).data("lang");
        var dispLang = selectLang.substr(0, 2);
        $(".details-lang").text(dispLang);
        $(this).parent(".language-select-dropd").removeClass("lang-select-cli");
        $(".details-lang").removeClass("arrow-org");
        changeLangCode(pageCode, langID);
    });

    $(".lang-selection-mob-li").on("click", function (e) {
        e.stopPropagation();
        var mobLangId = $(this).data("lang");
        changeLangCode(pageCode, mobLangId);
    });

    getProfileInitials();
    checkArrow();
    typeAnimate();
    setTimeout(fadeOutSearchText, textSpeed * animateText.length);

    $(".bfs-nav-popup-wh-wrap").on("click", function (e) {
        if ($(e.target).hasClass("open-popup-wh")) {
            $(".bfs-nav-popup-wh-wrap").removeClass("open-popup-wh");
            $(".bfs-nav-popup-overlay-main").removeClass("active-log-popup");
            $(".bfs-nav-popup-wh-main").addClass("login-popup-sh");
            $(".bfs-nav-popup-success").addClass("login-popup-sh");
        }
    });


    $(".bfs-menu-search-main .form-control").focus(function () {
        $(".animatepholder").hide();
        $(".bfs-menu-search-main .form-control").attr("placeholder", "");
    });

    $(".bfs-menu-search-main .form-control").focusout(function () {
        $(".bfs-menu-search-main .form-control").attr("placeholder", "What can we help you find?");

    })

    $(".bfs-menu-search-main .v1_HomeserchtHead1").on('click', function () {
        $(".animatepholder").css('display', 'none');
        $(".bfs-menu-search-main .form-control").focus();

    });

    $(".profile-menu-list-wrap").click(function (e) {
        e.stopPropagation();
    });

    if ($(window).innerWidth() > 991) {
        langSplit();
    }

    if ($(window).innerWidth() < 992) {
        mobLngSelectionLoad();

        hamburgerRedirectionParent();

        hamburgerRedirectionChild();

        $(".side-bar-footer-list").on("click", function () {
            $(this).find(".lang-selection-mob").addClass("openlangmenu");
        });

        $(window).scroll(function () {

            if ($(this).scrollTop() > 0) {
                $('.bfs-nav-details-wrap').hide();
            } else {
                $('.bfs-nav-details-wrap').show();
            }
        });

        $(document).on("click",".accor-link-head", function (e) {
            e.preventDefault();
            if ($(this).next().hasClass('option-show')) {
                $(this).next().removeClass('option-show');
                $(this).next().slideUp(350);
                $(this).removeClass("arroww")
            } else {
                $(this).toggleClass('arroww');
                $(this).parent().parent().find('li .inner-acc').removeClass('option-show');
                $(this).parent().parent().find('li .inner-acc').slideUp(350);
                $(this).next().toggleClass('option-show');
                $(this).next().slideToggle(350);
                $(this).parent(".lns-submenu").siblings().find(".accor-link-head").removeClass("arroww");
            }
        });

        $(document).on("click",".inner-submenus-acc-cl", function (e) {
            e.preventDefault();
            if ($(this).next().hasClass('option-show')) {
                $(this).next().removeClass('option-show');
                $(this).next().slideUp(350);
                $(this).removeClass("arroww")
            } else {
                $(this).toggleClass('arroww');
                $(this).parent().parent().find('.inner-submenus-acc-head .inner-submenus-acc-ul').removeClass('option-show');
                $(this).parent().parent().find('.inner-submenus-acc-head .inner-submenus-acc-ul').slideUp(350);
                $(this).next().toggleClass('option-show');
                $(this).next().slideToggle(350);
                $(this).parent(".inner-submenus-acc-head").siblings().find(".inner-submenus-acc-cl").removeClass("arroww");

            }
        });

        $(".inner-menu-submenu-wrap").css("height", innerDivHeight());

        $(".lang-selection-mob").css("height", innerDivHeight());

        $(document).on("click",".bfs-hamburger-icon", function () {
            $(".bfs-nav-list-wrap").addClass("open-bfs-side-menu");
            $(".bfs-overlay").addClass("openn");
            $("body").addClass("no-overflow");
        });

        $(document).on("click",".bfs-overlay", function () {
            $(this).removeClass("openn");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $("body").removeClass("no-overflow");
        });

        $(document).on("click",".side-menu-cross-wrap", function () {
            $(".bfs-overlay").removeClass("openn");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $("body").removeClass("no-overflow");

        });

        $(document).on("click",".inner-sub-menu-bk-one .back-arrow", function (e) {
            e.stopPropagation();
            $(this).parents(".inner-menu-submenu-wrap").removeClass("side-bar-back-open-tab");
            $(this).parents(".lang-selection-mob").removeClass("openlangmenu");
        });

        $(document).on("click",".inner-sub-menu-bk-one .back-close", function (e) {
            e.stopPropagation();
            $(".bfs-overlay").removeClass("openn");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $(this).parents(".inner-menu-submenu-wrap").removeClass("side-bar-back-open-tab");
            $("body").removeClass("no-overflow");
            $(this).parents(".lang-selection-mob").removeClass("openlangmenu");
        });

        $(document).on("click",".inner-subs-main-cl .back-arrow", function (e) {
            e.stopPropagation();
            $(this).parents(".inner-submenu").removeClass("opem-inner-submenu");
            $(this).parents(".inner-submenus-adpt-wrap").removeClass("adpt-wrap-open");
        });
        $(document).on("click", ".inner-subs-main-cl .back-close", function (e) {
            e.stopPropagation();
            $(".bfs-overlay").removeClass("openn");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $(this).parents(".inner-submenu").removeClass("opem-inner-submenu");
            $(".inner-menu-submenu-wrap").removeClass("side-bar-back-open-tab");
            $(".profile-popup-wrap").removeClass("show-prof-popup");
            $("body").removeClass("no-overflow");
        });

        $(document).on("click",".inner-child", function () {
            if ($(this).find(".inner-submenu").hasClass("emi-menus")) {
                var submneuAttr = $(this).find(".inner-child-list").data("type");
                console.log(submneuAttr);
                var menuToShow = $("#" + submneuAttr);
                console.log(menuToShow);
                $(menuToShow).addClass("adpt-wrap-open").siblings(".inner-submenus-adpt-wrap").removeClass("adpt-wrap-open");
            } else {
                $(this).find(".inner-submenu").addClass("opem-inner-submenu");
            }
        });

        $('.profile-popup-wrap').click(function (e) {
            e.stopPropagation();
        });

        $(document).on("click",".profile-menu-view", function () {
            $(".profile-popup-wrap").addClass("show-prof-popup");
        });

        $("#signInProfile").on('click', function () {
            $("#signInDropdown").addClass("show-prof-list");
        });

        $('#postLogin').on('click', function () {
            $("#signInDropdown").addClass("show-prof-list");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $(".bfs-overlay").removeClass("openn");
            $("body").removeClass("no-overflow");
        });

        $('#postSignIn').on('click', function () {
            $("#signInDropdown").addClass("show-prof-list");
            $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
            $(".bfs-overlay").removeClass("openn");
            $("body").removeClass("no-overflow");
        });

        $(document).on("click","#signInDropdown .back-close", function () {
            $("#signInDropdown").removeClass("show-prof-list");
        });

        $(".bfs-nav-list-parent .bfs-nav-list-parent-link").removeAttr("href nav-src");

        $(".inner-child .inner-child-list").removeAttr("href nav-src");

        $(".bfs-side-bar-footer-mob .bfs-side-bar-footer-link").each(function () {
            if (!$(this).hasClass("bfs-no-redirect")) {
                $(this).removeAttr("href nav-src");
            }
        })
    }

    $(document).on("click", ".bfs-nav-list-parent", function () {
        if ($(window).innerWidth() < 992){
            $(this).find(".inner-menu-submenu-wrap").addClass("side-bar-back-open-tab");
            $(".inner-submenu").css("height", secLevelNavHeight($(this)));
            $(".inner-submenus-adpt-wrap").css("height", secLevelNavHeight($(this)));
        }else{
           if($(this).hasClass("active")){
                $(this).removeClass("active");
           }else{
              $(this).addClass("active").siblings().removeClass("active");
           }
        }
    });

    $(document).on("click", function(event){
        if(!$(event.target).closest('.bfs-nav-list-parent').length){
            $('.bfs-nav-list-parent').removeClass('active');
        }
    });

    if (matchMedia("(max-width: 991px)").matches) {
        $('.bfs-chatlinkmob').attr('id', 'chat-link-light');
    }
    else {
        $('.bfs-chatlinkdesk').attr('id','chat-link-light');
    }

    $(document).on("click",".cancel-btn-wrap", function () {
        $(".bfs-nav-popup-overlay-main").removeClass("active-log-popup");
        $(".bfs-nav-popup-wh-wrap").removeClass("open-popup-wh");
        $(".bfs-nav-popup-wh-main").addClass("login-popup-sh");
    })

    function inputLabel() {
        if ($(".cell-no").val() == "") {
            $(".cell-no").parent().removeClass("focus-anim");
        } else {
            $(".cell-no").parent().addClass("focus-anim");
        }
        $(".cell-no").focus(function () {
            $(this).parent().addClass("focus-anim");
        });
        $(".cell-no").blur(function () {
            if ($(this).val() == "") {
                $(this).parent().removeClass("focus-anim");
            }
        });

    }

    inputLabel();

    $(".login-mob-no").on("click", function () {
        $(this).prev(".cell-no").focus();
    });

    $(".re-sign").on("click", function () {
        var attrView = $(this).data("view");
        var signPopupToShow;
        if (attrView == "loginPopUp") {
            signPopupToShow = $("#" + attrView);

            $(signPopupToShow).addClass("active-log-popup");
            $(signPopupToShow).find(".bfs-nav-popup-wh-wrap").addClass("open-popup-wh");
            if ($(signPopupToShow).find(".bfs-nav-popup-wh-wrap .login-mob").hasClass("login-popup-sh")) {
                $(signPopupToShow).find(".bfs-nav-popup-wh-wrap .login-mob").removeClass("login-popup-sh");
            }

            $("#viewsignuppop").removeClass("active-log-popup");
            $("#viewsignuppop").find(".bfs-nav-popup-wh-wrap").removeClass("open-popup-wh");
            $("#viewsignuppop .bfs-nav-popup-wh-main").addClass("login-popup-sh");
            $("#login-mob-popup-form input").val("");
            $("#otpGenerate").addClass("diabled-sub-btn");
            $("#otpGenerate").attr("disabled", true);
            $("#login-mob-popup-form .pop-input-item-name").removeClass("focus-anim");
        }

        if (attrView == "viewsignuppop") {
            signPopupToShow = $("#" + attrView);
            $(signPopupToShow).addClass("active-log-popup");
            $(signPopupToShow).find(".bfs-nav-popup-wh-wrap").addClass("open-popup-wh");
            $("#loginPopUp").removeClass("active-log-popup");
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap").removeClass("open-popup-wh");
            if ($(signPopupToShow).find(".bfs-nav-popup-wh-wrap .signup-reg").hasClass("login-popup-sh")) {
                $(signPopupToShow).find(".bfs-nav-popup-wh-wrap .signup-reg").removeClass("login-popup-sh");
            }

            $("#loginPopUp .bfs-nav-popup-wh-main").addClass("login-popup-sh");
            $("#signup-popup-form input").val("");
            $("#regitserGenerateOTP").addClass("diabled-sub-btn");
            $("#regitserGenerateOTP").attr("disabled", true);
        }

        if ($('#cust-fnameErrorMsg').css('display') == "block") {
            $('#cust-fnameErrorMsg').css('display', 'none');
        }

        if ($('#register-mob-noErrorMsg').css('display') == "block") {
            $('#register-mob-noErrorMsg').css('display', 'none');
        }

        if ($('#dobErrorMsg').css('display') == "block") {
            $('#dobErrorMsg').css('display', 'none');
        }
    });

    $("#loginOTPBack").on("click", function () {
        document.getElementById("loginPassword").value = "";
        document.getElementById("loginPasswordErrorMsg").style.display = "none";
        var backAttr = $("#loginOTPBack").data("back");
        if (backAttr == "login-mob-back") {
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").removeClass("login-popup-sh");
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-otp").addClass("login-popup-sh");
        }
        else if (backAttr == "login-dob-back") {
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-dob").removeClass("login-popup-sh");
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-otp").addClass("login-popup-sh");
        }
    });

    $("#registerOTPBack").on("click", function () {
        document.getElementById("register-otp").value = "";
        document.getElementById("register-otpErrorMsg").style.display = "none";
        if ($("#viewsignuppop .signup-reg").hasClass("login-popup-sh")) {
            $("#viewsignuppop .signup-reg").removeClass("login-popup-sh");
            $("#viewsignuppop .signUpOTP").addClass("login-popup-sh");
        }
    });

    $(".bfs-nav-list-parent-link").on("click",function(){
        if(!renderHeader){
            Object.keys(headerData).forEach(function(nav){
                $("#bfl-dy-"+nav).append(headerData[nav]);
            })
            renderHeader=true;
        }
    })

    $("#signIn-mob-hamburger").on("click", function () {
        $(".bfs-overlay").removeClass("openn");
        $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
        $("#loginPopUp").addClass("active-log-popup");
        $("#loginPopUp").find(".bfs-nav-popup-wh-wrap").addClass("open-popup-wh");

        if ($("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").hasClass("login-popup-sh")) {
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").removeClass("login-popup-sh");
        }
        if(!renderHeader){
            Object.keys(headerData).forEach(function(nav){
                $("#bfl-dy-"+nav).append(headerData[nav]);
            })
            renderHeader=true;
        }
    });

    $("#preLogin").on("click", function () {
        $(".bfs-overlay").removeClass("openn");
        $(".bfs-nav-list-wrap").removeClass("open-bfs-side-menu");
        $("#loginPopUp").addClass("active-log-popup");
        $("#loginPopUp").find(".bfs-nav-popup-wh-wrap").addClass("open-popup-wh");

        if ($("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").hasClass("login-popup-sh")) {
            $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").removeClass("login-popup-sh");
        }
    });
});

function openCloseLoginPopUp() {
    $("#loginPopUp").addClass("active-log-popup");
    $("#loginPopUp").find(".bfs-nav-popup-wh-wrap").addClass("open-popup-wh");
    $("#loginPopUp input").val("");
    $("#otpGenerate").addClass("diabled-sub-btn");
    $("#otpGenerate").attr("disabled", true);
    if ($("#login-mob-popup-form .pop-input-item-name").hasClass("focus-anim")) {
        $("#login-mob-popup-form .pop-input-item-name").removeClass("focus-anim");
    }

    if ($("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").hasClass("login-popup-sh")) {
        $("#loginPopUp").find(".bfs-nav-popup-wh-wrap .login-mob").removeClass("login-popup-sh");
    }

    if ($("#login-mob-noErrorMsg").css("display") == "block") {
        $("#login-mob-noErrorMsg").css("display", "none");
    }
}
