/*******************************************API Utility Module - Start******************************************************/
(function (_global) {
    var _apiUtility = (function fnApiUtility(jsHelper, appConfig, apiConfig, ajaxUtility) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper, appConfig, apiConfig, ajaxUtility], "API Utility");
        }
        var apiUtilityObj = {};
        var call = function callApi(eachApiConfig, data) {
            return new Promise(function (resolve, reject) {
                var apiUrl = appConfig.apiRoot + "." + eachApiConfig.selector + appConfig.apiExtension;
                ajaxUtility
                    .postJson(apiUrl, data)
                    .then(function (responseText) {
                        resolve(responseText);
                    }).catch(function (error) {
                        reject(error);
                    });
            });
        }
        var getcall = function callApi(eachApiConfig, data) {
            return new Promise(function (resolve, reject) {
                var apiUrl =
                    (eachApiConfig.root != undefined ? eachApiConfig.root : appConfig.apiRoot) + eachApiConfig.endpoint + appConfig.apiExtension;
                ajaxUtility
                    .getJson(apiUrl, data)
                    .then(function (responseText) {
                        resolve(responseText);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };
        /***********GET NAV API**********/
        var NavGetApi = function (data) {
            var apiData = {};
            apiData.name = apiConfig.NAV_API.name;
            apiData.selector = apiConfig.NAV_API.selector;
            apiData.endpoint = apiConfig.NAV_API.endpoint + data + "/navlog";
            return new Promise(function (resolve, reject) {
                getcall(apiData,{}).then(function (response) {
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                })
            });
        }
        apiUtilityObj.NavGetApi = NavGetApi;
        /***********GET NAV API**********/
        /***********GET EXPLORE API**********/
        var ExploreGetApi = function (data) {
            var apiData = {};
            apiData.name = apiConfig.EXPLORE_FUND_API.name;
            apiData.selector = apiConfig.EXPLORE_FUND_API.selector;
            apiData.endpoint = apiConfig.EXPLORE_FUND_API.endpoint + "?" + data;
            return new Promise(function (resolve, reject) {
                getcall(apiData, {})
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        }
        apiUtilityObj.ExploreGetApi = ExploreGetApi;
        /***********GET EXPLORE API**********/

        /***********GET Search Compare API**********/

        var getSearchCompareApi = function (data) {
            var apiData = {};
            apiData.name = apiConfig.SEARCH_COMPARE_FUND_API.name;
            apiData.selector = apiConfig.SEARCH_COMPARE_FUND_API.selector;
            apiData.endpoint = apiConfig.SEARCH_COMPARE_FUND_API.endpoint;
            return new Promise(function (resolve, reject) {
                getcall(apiData, {})
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        }
        apiUtilityObj.getSearchCompareApi = getSearchCompareApi;

        /***********GET Search Compare API**********/



        return jsHelper.freezeObj(apiUtilityObj);
    })(_global.jsHelper, _global.appConfig, _global.apiConfig, _global.ajaxUtility);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'apiUtility', _apiUtility);
})(this);
/*******************************************API Utility Module - End******************************************************/