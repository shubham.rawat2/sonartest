/*******************************************App Config - Start******************************************************/

(function(_global){
    var _appConfig = (function (jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "App Config");
        }

        var appName = "bajaj-finserv-mutual-fund";
        // var contentRoot = "/content/" + appName;
        var contentRoot = "";
        // var apiRoot = contentRoot + "/api";
        var apiRoot = "https://bfsd.qa.bfsgodirect.com";
        // var apiExtension = ".json";
        var apiExtension = "";
        return jsHelper.freezeObj({
            appName: appName,
            contentRoot: contentRoot,
            apiRoot: apiRoot,
            apiExtension: apiExtension
        });
    })(_global.jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global,'appConfig',_appConfig);
})(this);

/*******************************************App Config - Start******************************************************/