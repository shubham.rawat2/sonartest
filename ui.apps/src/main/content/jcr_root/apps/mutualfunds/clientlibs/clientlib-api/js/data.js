var dataMapping = {};
dataMapping.topPerformingFunds = {};
dataMapping.selectedFundToCompare = {};
dataMapping.selectedFund = [];
dataMapping.topPerformingFunds.equity = [];
dataMapping.topPerformingFunds.debt = [];
dataMapping.topPerformingFunds.hybrid = [];
dataMapping.topPerformingFunds.elss = [];
dataMapping.fundNextUrl = '';
dataMapping.filterResponseData = [];
dataMapping.fundsfilterData = {
    Category: {
        Equity: {
            name: "Equity",
            value: "type=equity",
        },
        Debt: {
            name: "Debt",
            value: "type=debt",
        },
        Hybrid: {
            name: "Hybrid",
            value: "type=hybrid",
        },
    },
    SubCategory: {
        ELSS: {
            name: "ELSS",
            value: "sub_type=ELSS",
            type: "Equity",
            isChecked: false,
        },
        "Large Cap": {
            name: "Large Cap",
            value: "sub_type=Large Cap",
            type: "Equity",
            isChecked: false,
        },
        "Mid-cap": {
            name: "Mid-cap",
            value: "sub_type=Mid Cap",
            type: "Equity",
            isChecked: false,
        },
        "Small Cap": {
            name: "Small Cap",
            value: "sub_type=Small Cap",
            type: "Equity",
            isChecked: false,
        },
        "Multi-cap": {
            name: "Multi-cap",
            value: "sub_type=Multi Cap",
            type: "Equity",
            isChecked: false,
        },
        "Large & Mid-cap": {
            name: "Large & Mid-cap",
            value: "sub_type=Large & MidCap",
            type: "Equity",
            isChecked: false,
        },
        value: {
            name: "value",
            value: "sub_type=value Oriented",
            type: "Equity",
            isChecked: false,
        },
        Contra: {
            name: "Contra",
            value: "sub_type=",
            type: "Equity",
            isChecked: false,
        },
        "Sectoral/thematic": {
            name: "Sectoral/thematic",
            value:
                "sub_type=Sectoral-Banking OR  Sectoral-FMCG OR Sectoral-Infrastructure OR Sectoral-Pharma OR Sectoral-Technology OR Thematic OR Thematic-Dividend Yield OR Thematic-MNC OR Thematic-Energy OR Thematic-PSU OR Thematic-Consumption OR Thematic-ESG",
            type: "Equity",
            isChecked: false,
        },
        Focussed: {
            name: "Focussed",
            value: "sub_type=",
            type: "Equity",
            isChecked: false,
        },
        "Dividend yield": {
            name: "Dividend yield",
            value: "sub_type=Thematic-Dividend Yield",
            type: "Equity",
            isChecked: false,
        },
        Liquid: {
            name: "Liquid",
            value: "sub_type=Liquid",
            type: "Debt",
            isChecked: false,
        },
        "Ultra Short Duration": {
            name: "Ultra Short Duration",
            value: "sub_type=Ultra Short Duration",
            type: "Debt",
            isChecked: false,
        },
        "Low duration": {
            name: "Low duration",
            value: "sub_type=Low duration",
            type: "Debt",
            isChecked: false,
        },
        Overnight: {
            name: "Overnight",
            value: "sub_type=Overnight",
            type: "Debt",
            isChecked: false,
        },
        "Money Market": {
            name: "Money Market",
            value: "sub_type=Money Market",
            type: "Debt",
            isChecked: false,
        },
        "Corporate bond": {
            name: "Corporate bond",
            value: "sub_type=Corporate Bond",
            type: "Debt",
            isChecked: false,
        },
        "Credit risk": {
            name: "Credit risk",
            value: "sub_type=Credit Risk",
            type: "Debt",
            isChecked: false,
        },
        "Dynamic Bond": {
            name: "Dynamic Bond",
            value: "sub_type=Dynamic Bond",
            type: "Debt",
            isChecked: false,
        },
        "Short Duration": {
            name: "Short Duration",
            value: "sub_typeShort Duration",
            type: "Debt",
            isChecked: false,
        },
        "Medium duration": {
            name: "Medium duration",
            value: "sub_type=Medium Duration",
            type: "Debt",
            isChecked: false,
        },
        "Medium to long duration": {
            name: "Medium to long duration",
            value: "sub_type=Medium to long duration",
            type: "Debt",
            isChecked: false,
        },
        Gilt: {
            name: "Gilt",
            value: "sub_type=Gilt",
            type: "Debt",
            isChecked: false,
        },
        "Gilt with 10yr duration": {
            name: "Gilt with 10yr duration",
            value: "sub_type=Gilt with 10 year Constant Duration",
            type: "Debt",
            isChecked: false,
        },
        "Banking & PSU": {
            name: "Banking & PSU",
            value: "sub_type=Banking and PSU",
            type: "Debt",
            isChecked: false,
        },
        Floater: {
            name: "Floater",
            value: "sub_type=Floater",
            type: "Debt",
            isChecked: false,
        },
        "Small Cap": {
            name: "Small Cap",
            value: "sub_type=Small Cap",
            type: "Debt",
            isChecked: false,
        },
        "Dynamic asset allocation/Balanced Advantage": {
            name: "Dynamic asset allocation/Balanced Advantage",
            value: "sub_type=Dynamic Asset Allocation OR Balanced Advantage",
            type: "Hybrid",
            isChecked: false,
        },
        Arbitrage: {
            name: "Arbitrage",
            value: "sub_type=Arbitrage",
            type: "Hybrid",
            isChecked: false,
        },
        "Multi Asset allocation": {
            name: "Multi Asset allocation",
            value: "sub_type=Multi Asset Allocation",
            type: "Hybrid",
            isChecked: false,
        },
        "Conservative hybrid": {
            name: "Conservative hybrid",
            value: "sub_type=Conservative Hybrid",
            type: "Hybrid",
            isChecked: false,
        },
        "Balance hybrid": {
            name: "Balance hybrid",
            value: "sub_type=Balanced Hybrid",
            type: "Hybrid",
            isChecked: false,
        },
        "Aggressive hybrid": {
            name: "Aggressive hybrid",
            value: "sub_type=Aggressive Hybrid",
            type: "Hybrid",
            isChecked: false,
        },
        "Equity Savings": {
            name: "Equity Savings",
            value: "sub_type=Equity Savings",
            type: "Hybrid",
            isChecked: false,
        },
    },
    RiskFactor: {
        Low: {
            name: "Low",
            value: "risk_factor=LOW",
        },
        "Low to moderate": {
            name: "Low to moderate",
            value: "risk_factor=MLW",
        },
        Moderate: {
            name: "Moderate",
            value: "risk_factor=MOD",
        },
        "Moderately High": {
            name: "Moderately High",
            value: "risk_factor=MDH",
        },
        High: {
            name: "High",
            value: "risk_factor=HGH",
        },
        "Very High": {
            name: "Very High",
            value: "risk_factor=VHG",
        },
    },
    Ratings: {
        "1 Star": {
            name: "1 Star",
            value: "ratings=1",
        },
        "2 Star": {
            name: "2 Star",
            value: "ratings=2",
        },
        "3 Star": {
            name: "3 Star",
            value: "ratings=3",
        },
        "4 Star": {
            name: "4 Star",
            value: "ratings=4",
        },
        "5 Star": {
            name: "5 Star",
            value: "ratings=5",
        },
    },
    Returns: {
        "1 Months": {
            name: "1M Returns",
            value: "returns_1_mth",
            labelName: "1 mth Returns",
        },
        "3 Months": {
            name: "3M Returns",
            value: "returns_3_mth",
            labelName: "3 mth Returns",
        },
        "1 Year": {
            name: "1Y Returns",
            value: "returns_1_yr",
            labelName: "1 yr Returns",
        },
        "3 Year": {
            name: "3Y Returns",
            value: "returns_3_yr",
            labelName: "3 yrs Returns",
        },
        "5 Year": {
            name: "5Y Returns",
            value: "returns_5_yr",
            labelName: "5 yrs Returns",
        },
        "10 Year": {
            name: "10Y Returns",
            value: "returns_10_yr",
            labelName: "10 yrs Returns",
        },
    },
    Sort: {
        "Returns": {
            name: "Returns",
            value: "returns",
        },
        "Ratings": {
            name: "Ratings",
            value: "vr_rating",
        },
        "Min.investment": {
            name: "Min.investment",
            value: "minimum_investment_amount",
        },
        "AUM": {
            name: "AUM",
            value: "assets_under_management",
        },
    },
};
if (sessionStorage.hasOwnProperty("dataMapping")) {
  dataMapping = JSON.parse(sessionStorage.getItem("dataMapping"));
}
function updateDataMapping() {
  sessionStorage.setItem("dataMapping", JSON.stringify(dataMapping));
}
function getTopPerformingFundsByType(fundType) {
  var _this = this;
  var param = fundType.split('=')[1];
  return new Promise(function (resolve, reject) {
    if (!jsHelper.isEmpArr(_this[param])) {
      resolve(_this[param]);
    } else {
      apiUtility.ExploreGetApi(fundType).then(function (response) {
        response =
          typeof response == "string" ? JSON.parse(response) : response;
        var fundsData = jsHelper.isDef(response["results"]["results"])
          ? response["results"]["results"]
          : [];
        if (!jsHelper.isEmpArr(fundsData)) {
          _this[param] = fundsData;
          updateDataMapping();
          resolve(fundsData);
        } else {
          reject(response);
        }
      });
    }
  });
}
dataMapping.topPerformingFunds.getTopPerformingFundsByType = getTopPerformingFundsByType;

function getSearchCompareData(data) {
  return new Promise(function (resolve, reject) {
      apiUtility.getSearchCompareApi(data).then(function (response) {
        response = typeof response == "string" ? JSON.parse(response) : response;
        var compareData = jsHelper.isDef(response)
          ? response
          : [];
        if (!jsHelper.isEmpArr(compareData)) {
          updateDataMapping();
          resolve(compareData);
        } else {
          reject(response);
        }
      })
  });
}
dataMapping.getSearchCompareData = getSearchCompareData;

function getFilteredFundApiFn(qrParams) {
    return new Promise(function (resolve, reject) {
        apiUtility.ExploreGetApi(qrParams).then(function (response) {
            response = typeof response == "string" ? JSON.parse(response) : response;
            var fundsData = jsHelper.isDef(response["results"]["results"]) ? response["results"]["results"] : [];
            var fundnextUrl = jsHelper.isDef(response["results"]["next"]) && jsHelper.isNonNull(response["results"]["next"]) ? response["results"]["next"] : "";
            dataMapping.fundNextUrl = fundnextUrl;
            dataMapping.filterResponseData = fundsData;
            resolve(fundsData);
        });

    });
}
dataMapping.getFilteredFundApiFn = getFilteredFundApiFn;
